﻿using UnityEngine;
using System.Collections;

// Target creature 'breaks combat' and may move up to 4 squares including through your opponent's creatures but they cannot attack or interact with POI this turn.

public class CardPhaseToShadows : Card 
{
	TurnPhaseManager mPhaseManager;
	
	// Use this for initialization
	void Start () 
	{
		cardName = "Phase To Shadows";
		mPhaseManager = FindObjectOfType(typeof(TurnPhaseManager)) as TurnPhaseManager;
	}
	
	public override void CardEffect()
	{
		mTargetedCreature = handClickController.selectedCreature;
		HighlightSquaresThroughEnemiesFromCreature(mTargetedCreature, 4);
		handManager.mCurrentCard = this;
		//handManager.mTarget = mTargetedCreature;
		handManager.mActivatingActionCard = true;
		handManager.playingActionCard = false;
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}
	
	
	public override void ActivateOnTarget()
	{
		bool wasAlreadyElusive = mTargetedCreature.mElusive;
		if (mTargetedCreature.lockedInCombat && mPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
		{
			mTargetedCreature.canMove = true;
		}
		//mTargetedCreature.lockedInCombat = false;
		mTargetedCreature.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All,false);
		mTargetedCreature.mElusive = true;
		mTargetedCreature.mPathSquares.Clear();
		handClickController.SetCreatureMovePath(handClickController.mClickedSquare, mTargetedCreature);
		mTargetedCreature.MoveToSpace();
		mTargetedCreature.mNumberOfAttacksLeft = -50;
		mTargetedCreature.mElusive = wasAlreadyElusive;
		mTargetedCreature.canAttack = false;
		handManager.mActivatingActionCard = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;
	}
	
}