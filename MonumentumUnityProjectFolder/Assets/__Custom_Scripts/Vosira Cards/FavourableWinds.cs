﻿using UnityEngine;
using System.Collections;

//All friendly characters gain +2S until the end of this turn.
public class FavourableWinds : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 2;

    // Use this for initialization
    void Start()
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "FavourableWinds";
    }

    // Update is called once per frame
    void Update()
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
    }

    public override void CardEffect()
    {
        Creature[] creatures = FindObjectsOfType<Creature>();
        string boostName = cardName + "boost";

        foreach (Creature creature in creatures)
        {
            if ((creature.controllingPlayer == this.playerNumber) && creature.character)
            {

                if (!creature.mBoosts.Contains(boostName))
                {

                    creature.UpdateStrength(mBoostAmount);
                    creature.mBoosts.Add(boostName);
                    creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
                }
            }
        }



        //finish up the action and send this card to the Cooldown pile
        handManager.playingActionCard = false;
        handManager.mTargetting = false;
        handManager.actionCardToPlay = null;

    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        CardEffect();
    }

    public void OnConclusionPhase()
    {
        Debug.Log("Tidal Force has reached it's Conclusion.");
        Creature[] creatures = FindObjectsOfType<Creature>();
        string boostName = cardName + "boost";

        foreach (Creature creature in creatures)
        {
            if ((creature.controllingPlayer == this.playerNumber) && creature.character)
            {
                Debug.Log("Tidal Force has found " + creature.name);
                if (creature.mBoosts.Contains(boostName))
                {
                    Debug.Log("Tidal Force is removing the boost from " + creature.name);
                    creature.UpdateStrength(-mBoostAmount);
                    creature.mBoosts.Remove(boostName);
                    creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
                }
            }
        }
        cardState = CardState.Cooldown;
    }
}
