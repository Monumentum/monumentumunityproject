﻿using UnityEngine;
using System.Collections;

//Target character gains +4S when they are attacking a damaged character until the end of this turn.
public class FinishThem : Card
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 4;
    public bool mAttackedProperTarget = false;
	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "FinishThem!";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        mTargetedCreature = handClickController.selectedCreature;
        mTargetedCreature.mCombatCards.Add(this);
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public void OnConclusionPhase()
    {
        mTargetedCreature.mCombatCards.Remove(this);
        cardState = CardState.Cooldown;
    }

    public override void BattleEffect(Creature targetedCreature)
    {
        string boostName = cardName + "boost";
        if (targetedCreature.hitPoints < targetedCreature.startingHitPoints)
        {
            mAttackedProperTarget = true;
            mTargetedCreature.UpdateStrength(mBoostAmount);
            mTargetedCreature.mBoosts.Add(boostName);
            mTargetedCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCreature.strength, mTargetedCreature.fortitude);
        }
    }

    public override void AfterBattleEffect()
    {
        string boostName = cardName + "boost";
        if (mAttackedProperTarget)
        {
            mTargetedCreature.UpdateStrength(-mBoostAmount);
            mTargetedCreature.mBoosts.Remove(boostName);
            mTargetedCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCreature.strength, mTargetedCreature.fortitude);
        }
    }
}
