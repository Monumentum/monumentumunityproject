﻿using UnityEngine;
using System.Collections;

public class VosiraSetTrap : Card 
{
    public int mLOS = 3;
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void CardEffect()
    {
        Debug.Log("Doing effect of " + cardName);
        mTargetedCreature = handClickController.selectedCreature;
		HighlightSquaresOnlyinLOSofCreature(mTargetedCreature, mLOS);
        handManager.mTargetting = true;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public override void TargetAcquired()
    {
        PlaceTrap();
        handManager.mTargetting = false;
    }
}
