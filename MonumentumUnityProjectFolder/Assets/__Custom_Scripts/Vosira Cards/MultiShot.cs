﻿using UnityEngine;
using System.Collections;

//Target character gains an additional attack this turn.
public class MultiShot : Card 
{
    public int mExtraAttacksGiven = 1;
	// Use this for initialization
	void Start () 
    {
        cardName = "Multi-Shot";
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void CardEffect()
    {
        mTargetedCreature = handClickController.selectedCreature;
        mTargetedCreature.mNumberOfAttacksLeft = mTargetedCreature.mNumberOfAttacksLeft + mExtraAttacksGiven;
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
        cardState = CardState.Cooldown;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }
}
