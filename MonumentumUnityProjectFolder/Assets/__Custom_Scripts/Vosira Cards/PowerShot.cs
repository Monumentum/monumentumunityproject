﻿using UnityEngine;
using System.Collections;

//Target friendly character gains +4S and +Range(5) until the end of this turn 
//but suffers -2F and 'crippled' until the end of your next turn.
public class PowerShot : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 4;
    public int mRangeBoostAmount = 5;
    public int mFortitudeLossAmount = -2;
    public int mTurnsCreatureWillBeWeakened = 1;
	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "PowerShot";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        string boostName = cardName + "boost";
        mTargetedCreature = handClickController.selectedCreature;
        mTargetedCreature.UpdateStrength(mBoostAmount);
        mTargetedCreature.attackRange = mTargetedCreature.attackRange + mRangeBoostAmount;
        mTargetedCreature.mBoosts.Add(boostName);
        mTargetedCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCreature.strength, mTargetedCreature.fortitude);
        mTargetedCreature.GetComponent<NetworkView>().RPC("BeWeakened", RPCMode.All, boostName, mFortitudeLossAmount, mTurnsCreatureWillBeWeakened);
        handManager.mCurrentCard = this;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public void OnConclusionPhase()
    {
        string boostName = cardName + "boost";
        if (mTargetedCreature.mBoosts.Contains(boostName))
        {
            mTargetedCreature.UpdateStrength(-mBoostAmount);
            mTargetedCreature.attackRange = mTargetedCreature.attackRange - mRangeBoostAmount;
            mTargetedCreature.mBoosts.Remove(boostName);
            mTargetedCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCreature.strength, mTargetedCreature.fortitude);
        }
        cardState = CardState.Cooldown;
    }
}
