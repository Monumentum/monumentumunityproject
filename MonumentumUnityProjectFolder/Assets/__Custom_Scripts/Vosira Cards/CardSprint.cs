﻿using UnityEngine;
using System.Collections;

//Target creature 'breaks combat' and may move up to 2 squares.

public class CardSprint : Card 
{
	TurnPhaseManager mPhaseManager;

	// Use this for initialization
	void Start () 
	{
		cardName = "Sprint";
		mPhaseManager = FindObjectOfType(typeof(TurnPhaseManager)) as TurnPhaseManager;
	}
	
	public override void CardEffect()
	{
		mTargetedCreature = handClickController.selectedCreature;
		HighlightSquaresOnlyinRangeofCreature(mTargetedCreature, 2);
		handManager.mCurrentCard = this;
		//handManager.mTarget = mTargetedCreature;
		handManager.mActivatingActionCard = true;
		handManager.playingActionCard = false;
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}


	public override void ActivateOnTarget()
	{
		//mMovingCreature.transform.position = handClickController.mClickedSquare.transform.position + new Vector3(0, 0.5f, 0);
		// mMovingCreature.networkView.RPC("UpdatePosition", RPCMode.All, mMovingCreature.transform.position);
		if (mTargetedCreature.lockedInCombat && mPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
		{
			mTargetedCreature.canMove = true;
		}
		//mTargetedCreature.lockedInCombat = false;
		mTargetedCreature.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
		mTargetedCreature.mPathSquares.Clear();
		handClickController.SetCreatureMovePath(handClickController.mClickedSquare, mTargetedCreature);
		mTargetedCreature.MoveToSpace();
		handManager.mActivatingActionCard = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;
	}

}
