﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Used for showing the Strength/Fortitude of creatures when combat occurs, the attack roll that was made, and a Hit/Miss message ~Adam

public class CombatInfoDisplay : MonoBehaviour 
{
	TurnPhaseManager mTurnManager;
	[HideInInspector] public PlayerClickController mAttackingPlayerController; 
//	public Creature mAttacker;
//	public Creature mDefender;
	public int mAttackerStrength;
	public int mDefenderFortitude;
	public Sprite mAttackerCard;
	public Sprite mDefenderCard;
	int mAttackRoll = -1;
	float mResultDisplayTime = 4f;
	float mResultDisplayStartTime = -2f;
	bool mAttackOccured = false;

	[SerializeField] private GUIStyle mCombatDisplayStyle;


	//For dealing with the new Canvas UI ~Adam
	[SerializeField] private GameObject mInfoPanel;
	[SerializeField] private Image mAttackerImage;
	[SerializeField] private Text mAttackerBase;
	[SerializeField] private Text mAttackerRoll;
	[SerializeField] private Image mDefenderImage;
	[SerializeField] private Text mDefenderBase;
	[SerializeField] private Text mHitResult;
	[SerializeField] private Scrollbar mScroller;

	[SerializeField] private Sprite mDefaultCardImage;



	// Use this for initialization
	void Start () 
	{
		mTurnManager = GetComponent<TurnPhaseManager>();
		mScroller.value = 0;
	}//END of Start()
	
	// Update is called once per frame
	void Update () 
	{
		if(mTurnManager != null)
		{
			if(mTurnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
			{
				//Make the panel visible during the Combat Phase ~Adam
				//mInfoPanel.SetActive(true);

				//Slide the panel on and off screen ~Adam
				if( (mAttackerCard != null) || (mDefenderCard != null) )
				{
					mScroller.value = Mathf.Lerp (mScroller.value, 1f,0.1f);
				}
				else
				{
					mScroller.value = Mathf.Lerp (mScroller.value, 0f,0.1f);
				}


				//Show the attacker ~Adam
				if(mAttackerCard != null)
				{
					mAttackerImage.sprite = mAttackerCard;
					mAttackerBase.text = mAttackerStrength.ToString();
				}
				else
				{
					mAttackerImage.sprite = mDefaultCardImage;
					mAttackerBase.text = "";
				}
				//Show the Defender ~Adam
				if(mDefenderCard != null)
				{
					mDefenderImage.sprite = mDefenderCard;
					mDefenderBase.text = mDefenderFortitude.ToString();;
				}
				else
				{
					mDefenderImage.sprite = mDefaultCardImage;
					mDefenderBase.text = "";
				}



				//Show Attack Results ~Adam
				if(Time.time < mResultDisplayStartTime + mResultDisplayTime)
				{
					//Display attack roll ~Adam
					
					//Display whether the attack hit or missed
					mAttackerRoll.text = mAttackRoll.ToString();

					if(mAttackerStrength+mAttackRoll > mDefenderFortitude)
					{
						mHitResult.text = "Hit!";
						
					}
					
					else
					{
						mHitResult.text =  "Miss!";
						
					}
					
				}
				else
				{
					mHitResult.text = "";
					mAttackerRoll.text = "";
				}
			}
			else
			{
				//mInfoPanel.SetActive(false);
				mScroller.value = Mathf.Lerp (mScroller.value, 0f,0.1f);

			}
		}
		else
		{
			mTurnManager = FindObjectOfType<TurnPhaseManager>();
		}




		if(Time.time >= mResultDisplayStartTime + mResultDisplayTime && mAttackOccured)
		{
			GetComponent<NetworkView>().RPC("ClearCombatants", RPCMode.All);
		}

	}//END of Update()

	void OnGUI()
	{
		//Only show up during the Combat phase ~Adam
		if(mTurnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
		{
//			//Make the background box ~Adam
//			GUI.Box(new Rect(Screen.width *0.7f, Screen.height*0.6f, Screen.width*0.3f,Screen.height*0.3f), "");
//
//			//Show the attacker's Image and Strength ~Adam
//			if(mAttackerCard != null)
//			{
//				//Display Strength
//				GUI.Label(new Rect(Screen.width*0.75f, Screen.height*0.62f, Screen.height*0.05f, Screen.width*0.05f), mAttackerStrength.ToString());
//				//Display Card Image
//				GUI.Label(new Rect(Screen.width*0.72f, Screen.height*0.7f, Screen.width*0.1f, Screen.height*0.2f), mAttackerCard.texture);
//			}
//
//			//Show the Defender's Image and Fortitude ~Adam
//			if (mDefenderCard != null)
//			{
//				//Display Fortitude
//				GUI.Label(new Rect(Screen.width*0.85f, Screen.height*0.62f, Screen.height*0.05f, Screen.width*0.05f), mDefenderFortitude.ToString());
//				//Display Card Image
//				GUI.Label(new Rect(Screen.width*0.87f, Screen.height*0.7f, Screen.width*0.1f, Screen.height*0.2f), mDefenderCard.texture);
//			}
//
//			//Show Attack Results ~Adam
//			if(Time.time < mResultDisplayStartTime + mResultDisplayTime)
//			{
//				//Display attack roll ~Adam
//
//				//Display whether the attack hit or missed
//				GUI.Label(new Rect(Screen.width*0.755f, Screen.height*0.62f, Screen.height*0.05f, Screen.width*0.05f),"+"+ mAttackRoll.ToString());
//				GUI.Label(new Rect(Screen.width*0.8f, Screen.height*0.62f, Screen.height*0.05f, Screen.width*0.05f),"VS.");
//				if(mAttackerStrength+mAttackRoll > mDefenderFortitude)
//				{
//					GUI.Box(new Rect(Screen.width *0.7f, Screen.height*0.5f, Screen.width*0.3f,Screen.height*0.1f), "Attack Hit!");
//
//				}
//
//				else
//				{
//					GUI.Box(new Rect(Screen.width *0.7f, Screen.height*0.5f, Screen.width*0.3f,Screen.height*0.1f), "Attack Missed!");
//
//				}
//
//			}

		}
	}//END of OnGUI()


	[RPC] public void SetPlayerController(NetworkViewID controllerID)
	{
		mAttackingPlayerController = NetworkView.Find(controllerID).GetComponent<PlayerClickController>();
	}//END of SetAttacker()

	[RPC] public void SetAttacker(NetworkViewID attackerID)
	{
		Creature attacker = NetworkView.Find(attackerID).GetComponent<Creature>();
		mAttackerStrength = attacker.strength;
		mAttackerCard = attacker.handManager.deck[attacker.handCardNumber].mCardArt;
	}//END of SetAttacker()

	[RPC] public void SetDefender(NetworkViewID defenderID)
	{
		Creature defender = NetworkView.Find(defenderID).GetComponent<Creature>();
		mDefenderFortitude = defender.fortitude;
		mDefenderCard = defender.handManager.deck[defender.handCardNumber].mCardArt;
	}//END of SetDefender()

	[RPC] public void ClearCombatants()
	{
		mAttackerCard = null;
		mDefenderCard = null;
		mAttackerStrength = 0;
		mDefenderFortitude = 0;
		mAttackRoll = 0;
		mAttackOccured = false;
		mAttackingPlayerController.mAnimationPlaying = false;
	}//End of ClearCombatants

	[RPC] public void DeclareAttackResult(int attackRoll)
	{
		mAttackRoll = attackRoll;
		mAttackingPlayerController.mAnimationPlaying = true;
		mResultDisplayStartTime = Time.time;
		mAttackOccured = true;
	}
}
