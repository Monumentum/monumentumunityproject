﻿using UnityEngine;
using System.Collections;

public class RendandRive : Creature 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 4;
        }

        if (strength == 0)
        {
            strength = 4;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        creatureDominion = CreatureDominion.Vosira;
	}
	

    public override bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1)
    {
        bool didIHit = base.AttackTarget(targetedCreature);

        if ((targetedCreature != null) && targetedCreature.fortitude > 0)
        {
            targetedCreature.GetComponent<NetworkView>().RPC("ModifyFortitude", RPCMode.All, -1);
        }

        return didIHit;
    }
}
