﻿using UnityEngine;
using System.Collections;

public class FuryandFrenzy : Creature 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

        if (attackRange == 1)
        {
            attackRange = 5;
        }

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 6;
        }

        if (strength == 0)
        {
            strength = 4;
        }

        if (fortitude == 0)
        {
            fortitude = 10;
        }

        creatureDominion = CreatureDominion.Vosira;
	}

    public override bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1)
    {
        bool didIHit = base.AttackTarget(targetedCreature, 3);

        return didIHit;
    }
}
