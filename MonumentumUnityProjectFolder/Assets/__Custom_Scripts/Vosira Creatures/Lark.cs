﻿using UnityEngine;
using System.Collections;

public class Lark : Creature 
{
    // Use this for initialization
    public override void Start()
    {
        base.Start();

        if (attackRange == 1)
        {
            attackRange = 10;
        }

        if (strength == 0)
        {
            strength = 6;
        }

        creatureDominion = CreatureDominion.Vosira;
    }

	public override void BeginCombatPhase()
	{
		base.BeginCombatPhase();
		canAttack = true;
	}
}
