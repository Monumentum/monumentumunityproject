﻿using UnityEngine;
using System.Collections;

public class GloomandBleak : Creature 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

        if (attackRange == 1)
        {
            attackRange = 5;
        }
        
        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 6;
        }
        
        if (strength == 0)
        {
            strength = 6;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        creatureDominion = CreatureDominion.Vosira;
        mElusive = true;
	}

}
