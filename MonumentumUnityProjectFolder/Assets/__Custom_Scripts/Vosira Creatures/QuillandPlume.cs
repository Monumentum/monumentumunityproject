﻿using UnityEngine;
using System.Collections;

//Creature info: Range(5), Barbed Shot: This creature's attacks cause 'crippled'.

public class QuillandPlume : Creature 
{

	// Use this for initialization
    public override void Start()
    {
        base.Start();

		if (attackRange == 1)
		{
			attackRange = 5;
		}
		if (sourceStoneCost == 0)
		{
			sourceStoneCost = 6;
		}
		
		if (strength == 0)
		{
			strength = 6;
		}
		
		if (fortitude == 0)
		{
			fortitude = 10;
		}
		
		creatureDominion = CreatureDominion.Vosira;
	}


	//Override the AttackTarget function to cripple the target on a hit
	public override bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1)
	{

		bool didIHit = base.AttackTarget(targetedCreature);

		if(didIHit && targetedCreature.hitPoints > 0)
		{
			targetedCreature.GetComponent<NetworkView>().RPC("GetCrippled", RPCMode.All);
		}

		return didIHit;
	}
}
