﻿using UnityEngine;
using System.Collections;

public class PlayerTurnInfoUI : MonoBehaviour 
{
	[SerializeField] private PlayerClickController mPlayerController;
	[SerializeField] private PlayerHandManager mPlayerHandManager;
	[SerializeField] private TurnPhaseManager mTurnManager;
	//SourceStone Icons
	[SerializeField] private Sprite mSourceStoneIconFull;
	[SerializeField] private Sprite mSourceStoneIconDepleted;
	//Overlay for when it's not our turn
	[SerializeField] private Sprite mIconOverlay;


	[SerializeField] private GUIStyle mTurnUISTyle;

	// Use this for initialization
	void Start () 
	{
		mPlayerController = GetComponent<PlayerClickController>();
		mPlayerHandManager = GetComponent<PlayerHandManager>();
		mTurnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnGUI()
	{
		if(GetComponent<NetworkView>().isMine)
		{
			//Find icon for the current player's turn
			Sprite voidBreacherIcon = null;
	//		Card[] cardsInScene = FindObjectsOfType<Card>() as Card[];
	//		foreach(Card voidBreacherCard in cardsInScene)
	//		{
	//			if (voidBreacherCard.cardType == Card.CardType.VoidBreacher && voidBreacherCard.playerNumber == playerTurn)
	//			{
	//				if (voidBreacherCard.mVoidBreacherIcon != null)
	//				{
						voidBreacherIcon = mPlayerHandManager.deck[12].mVoidBreacherIcon[mPlayerController.playerNumber-1];
	//				}
	//			}
	//		}


			//The X positioning for the SourceStone icons
			float sourceStoneXPos = 0.051f;
			
			//Box displaying current turn/phase info
			//GUI.Box(new Rect(0,0, Screen.width * 0.2f, Screen.height*0.1f), "Player " + playerTurn + "'s turn.\n" + currentPhase + "Phase.\n" + playerControllers[playerTurn-1].sourceStoneCount + " Sourcestones Remaining.");
			//Display the VoidBreacher icon
			GUI.DrawTexture(new Rect(0,0, Screen.width*0.35f, Screen.width*0.05f), voidBreacherIcon.texture);
			//Display Source Stones
			for(int i = 0; i < 12; i++)
			{
				if(mPlayerController.sourceStoneCount >= i+1)
				{
					GUI.Label(new Rect(Screen.width*sourceStoneXPos, Screen.width*0.003f, Screen.width*0.025f,Screen.width*0.02f), mSourceStoneIconFull.texture, mTurnUISTyle);
				}
				else
				{
					GUI.Label(new Rect(Screen.width*sourceStoneXPos, Screen.width*0.003f, Screen.width*0.025f,Screen.width*0.02f), mSourceStoneIconDepleted.texture, mTurnUISTyle);
				}
				sourceStoneXPos+=0.024f;
			}
			//Fade over the VoidBreacher icon if it's not our turn
			if(mTurnManager.playerTurn != mPlayerController.playerNumber)
			{
				GUI.DrawTexture(new Rect(0,0, Screen.width*0.35f, Screen.width*0.05f), mIconOverlay.texture);
			}

			mTurnUISTyle.fontSize = Mathf.RoundToInt(Screen.width*0.01f);
			//Say what turn/phase it is
			GUI.Label(new Rect(Screen.width*0.051f,Screen.width*0.03f, Screen.width*0.35f, Screen.width*0.1f),"Player " + mTurnManager.playerTurn + " " + mTurnManager.currentPhase.ToString() + " Phase", mTurnUISTyle);
		}
	}//END of OnGUI()
}
