﻿using UnityEngine;
using System.Collections;

public class Minotaur : Creature
{
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 8;
        }

        if (strength == 0)
        {
            strength = 8;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        creatureDominion = CreatureDominion.Rhavik;
    }
	//Function for attacking a target
    public override bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1)
	{
		bool didIHit = base.AttackTarget(targetedCreature);


		//if you took the target down to 0 HP, kill it
		if (targetedCreature.hitPoints == 0)
		{
			MinotaurVictoryRush();
		}

        return didIHit;
	}//End of AttackTarget()


	void MinotaurVictoryRush()
	{
		canMove = true;
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && canMove && !movementSquaresAreHighlighted)
		{
			//highlight adjacent squares
			//Debug.Log("Highlighting the first set of squares");
			foreach(BoardSquare potentialMoveTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
			{
				if (potentialMoveTarget.occupier == null || potentialMoveTarget.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer)
				{
					potentialMoveTarget.inMovementRange = true;
				}
			}
					
			//unhighlight occupied squares in range
			//Debug.Log("Done Highlighting squares");
			movementSquaresAreHighlighted = true;
			BoardSquare[] moveTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
			foreach (BoardSquare invalidMoveTarget in moveTargetToUnHighlight)//Unhighlight any squares that are Highlighted
			{
				if (invalidMoveTarget.occupier != null)
				{
					invalidMoveTarget.inMovementRange = false;
				}
			}
		}
			
	}//End of MinotaurVictoryRush()


}
