﻿using UnityEngine;
using System.Collections;

public class FlintDross : Creature 
{
    public int mBoostAmount = 1;
	//Needs code for buffing any famliars with Runes on them    
    public override void Start()
    {
        base.Start();

        creatureDominion = CreatureDominion.Rhavik;
    }

//    public override void BeginCombatPhase()
//    {
//		ApplyFlintRuneBoost();
//        base.BeginCombatPhase();
//    }


	public void ApplyFlintRuneBoost ()
	{
		Creature[] creatures = FindObjectsOfType<Creature>();
		string boostName = "FlintRuneBoost";
		
		foreach (Creature creature in creatures)
		{
			boostName = "FlintRuneBoost";
			if ((creature.controllingPlayer == this.controllingPlayer) && creature.character && GetComponent<NetworkView>().isMine)
			{
				//Make it so the boost name is based on the number of runes, so that each new rune is a new boost
				boostName += creature.mNumberOfRunes.ToString();

				if (!creature.mBoosts.Contains(boostName) && creature.mNumberOfRunes > 0)
				{
					creature.UpdateStrength(mBoostAmount);
					creature.mBoosts.Add(boostName);
					creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
				}
				//I don't think this part will ever get called because Creature's can't lose Runes ~Adam
				else if (creature.mBoosts.Contains(boostName) && creature.mNumberOfRunes <= 0)
				{
					creature.UpdateStrength(-mBoostAmount);
					creature.mBoosts.Remove(boostName);
					creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
				}
			}
		}

	}
}
