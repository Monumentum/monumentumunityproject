﻿using UnityEngine;
using System.Collections;

public class Thorlem : Creature 
{
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 6;
        }

        if (strength == 0)
        {
            strength = 6;
        }

        if (fortitude == 0)
        {
            fortitude = 12;
        }

        creatureDominion = CreatureDominion.Rhavik;
    }
	//Function for attacking a target
    public override bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1) //Overrides to get stronger with each attack
	{
        GetComponent<NetworkView>().RPC("ModifyStrength", RPCMode.All, 1); //Add 1Strength each time it attacks
		bool didIHit = base.AttackTarget(targetedCreature);

        return didIHit;
	}//End of AttackTarget()

}
