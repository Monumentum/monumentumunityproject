﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Card : MonoBehaviour 
{
	public enum CardState{Hand, Table, Cooldown, Recharge}; //the positions a card can be in
	public enum CardType{Familiar, Minion, Action, Trap, VoidBreacher}; //what type of card it is
	public enum CardDominion{Irotar,Vosira,Rhavik}; //what dominion the card is

	public CardState cardState;
	public CardType cardType;
	public GameObject summonedGameObject;
	public int sourceStoneCost;
	public string cardName;
	public CardDominion cardDominion;
	public int playerNumber;
	public int handNumber;
    public Creature mTargetedCreature;
	public PlayerHandManager handManager;
	public PlayerClickController handClickController;

	public string mCardDescription = "";

	public Sprite mCardArt;

	public string mCardIDCode = "";

	//Icon that appears in the top-left of the screen for showing whose turn it is.  
	//Assign in the prefab, but only if this card is for a VoidBreacher, otherwise leave it null.
	public Sprite[] mVoidBreacherIcon;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(cardState != CardState.Table)
		{
			mTargetedCreature = null;
		}

	}

	public virtual void CardEffect()
	{
		Debug.Log("Doing effect of " + cardName);
		cardState = CardState.Cooldown;
		handManager.playingActionCard = false;
		handManager.actionCardToPlay = null;
	}

	public virtual void CardTarget()
	{
		
	}

	public void TargetAllFriendlyCharacters()
	{
		Creature[] creatures = FindObjectsOfType<Creature>();

		foreach (Creature creature in creatures)
		{
			if ((creature.controllingPlayer == this.playerNumber) && creature.character)
			{
				creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
			}
		}
	}

    public void TargetAllEnemyCharacters()
    {
        Creature[] creatures = FindObjectsOfType<Creature>();

        foreach (Creature creature in creatures)
        {
            if ((creature.controllingPlayer != this.playerNumber) && creature.character)
            {
                creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
            }
        }
    }

    public void TargetAllCharactersInLOS(int range)
    {
        //Creature[] creatures = FindObjectsOfType<Creature>();

        //for (int i = 1; i <= range; i++)
        //{
        //    foreach (Creature creature in creatures)
        //    {
        //        if ((creature.controllingPlayer != this.playerNumber) && creature.character)
        //        {
        //            if (Vector3.Distance(creature.currentSpace.gameObject.transform.position, mTargetedCreature.currentSpace.gameObject.transform.position) == i)
        //            {
        //                creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
        //            }
        //        }
        //    }
        //}

        for (int i = 1; i <= range; i++)
        {
            //highlight adjacent squares
            if (i == 1)
            {
                //Debug.Log("Highlighting the first set of squares");
                foreach (BoardSquare potentialTarget in mTargetedCreature.currentSpace.GetComponent<BoardSquare>().neighborSquares)
                {
                    potentialTarget.hasAttackableTarget = true;
                }
            }
            //Highlight other squares in range
            else
            {
                //Debug.Log("Highlighting another set of squares");
                BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
                foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
                {
                    if (newCombatTarget.hasAttackableTarget == true)
                    {
                        highlightedSquareList.Add(newCombatTarget);
                    }
                }
                foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
                {
                    foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
                    {
                        if (listedSqaure.occupier == null ||
                            (listedSqaure.occupier.GetComponent<Creature>() != null && listedSqaure.occupier.GetComponent<Creature>().controllingPlayer == mTargetedCreature.controllingPlayer))
                        {
                            if (Vector3.Distance(potentialTarget.gameObject.transform.position, mTargetedCreature.currentSpace.gameObject.transform.position) == i)
                            {
                                potentialTarget.hasAttackableTarget = true;
                            }
                        }
                    }
                }

            }
            //unhighlight unoccupied/friendly squares in range
            if (i == range)
            {
                BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
                {
                    if (invalidTarget.occupier == null || (invalidTarget.occupier.GetComponent<Creature>() != null && invalidTarget.occupier.GetComponent<Creature>().tag == "VoidBreacher"))
                    {
                        invalidTarget.hasAttackableTarget = false;
                    }
                }

            }
        }
            
    }

	public void UnTargetAllSpaces()
	{
		Creature[] creatures = FindObjectsOfType<Creature>();
		
		foreach (Creature creature in creatures)
		{
			creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
			creature.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget = false;
			creature.currentSpace.GetComponent<BoardSquare>().validSummonSpot = false;
		}

        BoardSquare[] squares = FindObjectsOfType<BoardSquare>();

        foreach (BoardSquare square in squares)
        {
            square.GetComponent<BoardSquare>().inMovementRange = false;
            square.GetComponent<BoardSquare>().hasAttackableTarget = false;
            square.GetComponent<BoardSquare>().validSummonSpot = false;
        }
	}

	public void SummonRune()
	{
		GameObject summonedRune = (GameObject)Network.Instantiate(summonedGameObject, Vector3.zero, Quaternion.identity,0);
		
		summonedRune.GetComponent<Rune>().EquipRune(handClickController.selectedCreature.GetComponent<NetworkView>().viewID);
		summonedRune.GetComponent<Rune>().mAssociatedCard = this;
		handClickController.selectedCreature.GetComponent<NetworkView>().RPC("AddRuneCardImage", RPCMode.All, gameObject.name);

		handManager.playingActionCard = false;
		handManager.actionCardToPlay = null;
	}

    public void PlaceTrap()
    {
        Vector3 trapPosition = handClickController.mClickedSquare.transform.position + new Vector3(0, 0.5f, 0);
        GameObject setTrap = (GameObject)Network.Instantiate(summonedGameObject, trapPosition, Quaternion.identity, 0);
        Trap trap = setTrap.GetComponent<Trap>();
        //trap.mOccupiedSquare = handClickController.mClickedSquare;
        trap.mAssociatedCard = this;
        //trap.mControllingPlayer = this.playerNumber;
		trap.GetComponent<NetworkView>().RPC("SetTrap", RPCMode.All, this.playerNumber, cardName, mCardDescription);
        handManager.playingActionCard = false;
        handManager.mTargetting = false;
        handManager.mActivatingActionCard = false;
        handManager.actionCardToPlay = null;
    }

    public virtual void TargetAcquired()
    {

    }

    [RPC] public virtual void ActivateOnTarget()
    {

    }

	public void HighlightSquaresOnlyinRangeofCreature(Creature newNeighbor, int range)
	{
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTarget in newNeighbor.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTarget.inMovementRange = true;
					potentialTarget.mDistanceFromMover = i;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.inMovementRange == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Creature>() != null && listedSqaure.occupier.GetComponent<Creature>().controllingPlayer == mTargetedCreature.controllingPlayer))
						{
							potentialTarget.inMovementRange = true;
							if (potentialTarget.mDistanceFromMover == 99)
							{
								potentialTarget.mDistanceFromMover = i;
							}
						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (invalidTarget.occupier != null)
					{
						invalidTarget.inMovementRange = false;
					}
				}
				
			}
		}
	}//END of HighlightSquaresOnlyinRangeofCreature()


    public void HighlightSquaresOnlyinLOSofCreature(Creature newNeighbor, int range)
    {
        for (int i = 1; i <= range; i++)
        {
            //highlight adjacent squares
            if (i == 1)
            {
                //Debug.Log("Highlighting the first set of squares");
                foreach (BoardSquare potentialTarget in newNeighbor.currentSpace.GetComponent<BoardSquare>().neighborSquares)
                {
                    potentialTarget.hasAttackableTarget = true;
                }
            }
            //Highlight other squares in range
            else
            {
                //Debug.Log("Highlighting another set of squares");
                BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
                foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
                {
                    if (newCombatTarget.hasAttackableTarget == true)
                    {
                        highlightedSquareList.Add(newCombatTarget);
                    }
                }
                foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
                {
                    foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
                    {
                        if (listedSqaure.occupier == null ||
                            (listedSqaure.occupier.GetComponent<Creature>() != null && listedSqaure.occupier.GetComponent<Creature>().controllingPlayer == mTargetedCreature.controllingPlayer))
                        {
                            if (Vector3.Distance(potentialTarget.gameObject.transform.position, mTargetedCreature.currentSpace.gameObject.transform.position) == i)
                            {
                                potentialTarget.hasAttackableTarget = true;
                            }
                        }
                    }
                }

            }
            //unhighlight occupied squares in range
            if (i == range)
            {
                BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
                {
                    if (invalidTarget.occupier != null)
                    {
                        invalidTarget.hasAttackableTarget = false;
                    }
                }

            }
        }
	}//END of HighlightSquaresOnlyinLOSofCreature()


	public void HighlightSquaresThroughEnemiesFromCreature(Creature newNeighbor, int range)
	{
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTarget in newNeighbor.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTarget.inMovementRange = true;
					potentialTarget.mDistanceFromMover = i;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.inMovementRange == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Creature>() != null))
						{
							potentialTarget.inMovementRange = true;
							if (potentialTarget.mDistanceFromMover == 99)
							{
								potentialTarget.mDistanceFromMover = i;
							}

						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (invalidTarget.occupier != null)
					{
						invalidTarget.inMovementRange = false;
					}
				}
				
			}
		}
	}//END of HighlightSquaresThroughEnemiesFromCreature()


	public void HighlightSquaresBetweenCreatures(Creature originCreature, Creature furtherCreature, int range)
	{

		List<BoardSquare> squaresBetweenList = new List<BoardSquare>();

		//Highlight squares around the originCreature
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTarget in originCreature.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTarget.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.hasAttackableTarget == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Creature>() != null && listedSqaure.occupier.GetComponent<Creature>().controllingPlayer == mTargetedCreature.controllingPlayer))
						{
							if (Vector3.Distance(potentialTarget.gameObject.transform.position, mTargetedCreature.currentSpace.gameObject.transform.position) == i)
							{
								potentialTarget.hasAttackableTarget = true;
							}
						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTarget in targetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (invalidTarget.occupier != null)
					{
						invalidTarget.hasAttackableTarget = false;
					}
				}
				
			}
		}//End of Highlighting squares around the originCreature

		//Add the highlighted squares to the squaresBetweenList
		BoardSquare[] targetToAddBetween = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
		foreach (BoardSquare betweenSquare in targetToAddBetween)//Unhighlight any squares that are Highlighted
		{
			if (betweenSquare.hasAttackableTarget)
			{
				squaresBetweenList.Add(betweenSquare);
			}
			betweenSquare.hasAttackableTarget = false;
		}


		//Highlight squares around the furtherCreature
		for (int i = 1; i <= range; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach (BoardSquare potentialTargetFurther in furtherCreature.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialTargetFurther.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedTargetsFurther = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareListFurther = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedTargetsFurther)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.hasAttackableTarget == true)
					{
						highlightedSquareListFurther.Add(newCombatTarget);
					}
				}
				foreach (BoardSquare listedSqaure in highlightedSquareListFurther)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach (BoardSquare potentialTargetFurther in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null ||
						    (listedSqaure.occupier.GetComponent<Creature>() != null && listedSqaure.occupier.GetComponent<Creature>().controllingPlayer == mTargetedCreature.controllingPlayer))
						{
							if (Vector3.Distance(potentialTargetFurther.gameObject.transform.position, furtherCreature.currentSpace.gameObject.transform.position) == i)
							{
								potentialTargetFurther.hasAttackableTarget = true;
							}
						}
					}
				}
				
			}
			//unhighlight occupied squares in range
			if (i == range)
			{
				BoardSquare[] targetToUnHighlightFurther = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare invalidTargetFurther in targetToUnHighlightFurther)//Unhighlight any squares that are Highlighted
				{
					if (invalidTargetFurther.occupier != null)
					{
						invalidTargetFurther.hasAttackableTarget = false;
					}
				}
				
			}
		}//End of Highlighting squares around the furtherCreature

		//Unhighlight the squares that don't overlap
		foreach (BoardSquare betweenSquare in targetToAddBetween)//Unhighlight any squares that are Highlighted
		{
			if (!squaresBetweenList.Contains(betweenSquare))
			{
				betweenSquare.hasAttackableTarget = false;
			}
		}
	}//END of HighlightSquaresBetweenCreatures()

    public virtual void BattleEffect(Creature targetedCreature)
    {

    }

    public virtual void AfterBattleEffect()
    {

    }

	public void DoAOEAttack(int attackRange, int attackStrength)
	{
		Creature attackingVoidBreacher = new Creature(); 

		Creature[] voidBreachersInPlay = FindObjectsOfType(typeof(Creature)) as Creature[];
		foreach (Creature voidBreacher in voidBreachersInPlay)
		{
			if (voidBreacher.tag == "VoidBreacher" && voidBreacher.controllingPlayer == playerNumber)
			{
				attackingVoidBreacher= voidBreacher;
			}
		}



		Creature originCreature = handClickController.selectedCreature;
		//find the targets of the AOE 
		
		List<Creature> aoeTargets = new List<Creature>();
		
		for (int i = 1; i <= attackRange; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach(BoardSquare potentialCombatTarget in originCreature.currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialCombatTarget.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedCombatTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newCombatTarget in highlightedCombatTargets)//Add any already highlighted squares to the list
				{
					if (newCombatTarget.hasAttackableTarget == true)
					{
						highlightedSquareList.Add(newCombatTarget);
					}
				}
				foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach(BoardSquare potentialCombatTarget in listedSqaure.neighborSquares)
					{
						if (listedSqaure.occupier == null || listedSqaure.occupier.GetComponent<Creature>() != null)
						{
							if (Vector3.Distance(potentialCombatTarget.gameObject.transform.position, originCreature.currentSpace.gameObject.transform.position) == i)
							{
								potentialCombatTarget.hasAttackableTarget = true;
							}
						}
					}
				}
				
			}
			//add enemy creatures in range to the list and then unhighlight all the squares
			if (i == attackRange)
			{
				//Debug.Log("Done Highlighting squares");
				BoardSquare[] combatTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare aoeCombatTarget in combatTargetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (aoeCombatTarget.hasAttackableTarget 
					    && aoeCombatTarget.occupier != null && aoeCombatTarget.occupier.GetComponent<Creature>() != null 
					    && aoeCombatTarget.occupier.GetComponent<Creature>().controllingPlayer != playerNumber 
					    && aoeCombatTarget.occupier.GetComponent<Creature>().tag != "VoidBreacher")
					{
						aoeTargets.Add(aoeCombatTarget.occupier.GetComponent<Creature>());
					}
					aoeCombatTarget.hasAttackableTarget = false;
					
				}
				
			}
		}//End of finding the AOE targets
		
		//Attack the enemy creatures in AOE range
		
		foreach(Creature aoeTargetedCreature in aoeTargets)
		{
			Creature targetedCreature = aoeTargetedCreature;
			bool didIHit = false;
			//Roll 2d6 and add your Strength
			int rollBonus = 0;
			
			for (int i = 0; i < 2; i++)
			{
				int dieRoll = 0;
				dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
				dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
				
				if (dieRoll > rollBonus)
				{
					rollBonus = dieRoll;
				}
			}
			attackStrength += rollBonus;
			
			//Compare your results to the target's Fortitude
			if (targetedCreature.tag == "VoidBreacher")//Automatically hit a VoidBreacher
			{
				//do nothing to a VoidBreacher
			}
			else if (attackStrength == attackStrength+2)//Automatically miss if you roll a 1 on both dice
			{
				Debug.Log(this.name + " missed " + targetedCreature.name);
				targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, attackingVoidBreacher.GetComponent<NetworkView>().viewID, false);
			}
			else if (attackStrength > targetedCreature.fortitude || attackStrength==attackStrength+12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
			{
				targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, attackingVoidBreacher.GetComponent<NetworkView>().viewID, true);
				didIHit = true;
				Debug.Log(this.name + " hit " + targetedCreature.name + " leaving it with " + targetedCreature.hitPoints + " HP");
			}
			else//Miss if you roll lower than the target's Fortitude
			{
				Debug.Log(this.name + " missed " + targetedCreature.name);
				targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, attackingVoidBreacher.GetComponent<NetworkView>().viewID, false);
			}
			
			//Done attacking now
			
			
			
			//if you took the target down to 0 HP, kill it and move into its space if it was adjacent
			if (targetedCreature.hitPoints == 0)
			{
				targetedCreature.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
			}
			
		}//End of attacking the AOE targets
	}//END of DoAOEAttack()
}
