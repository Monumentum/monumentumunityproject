﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Serialization.JsonFx;


public class DeckBuilder : MonoBehaviour 
{
    public enum CardTypePicking { VoidBreacher, Familiar, End};
    
    public List<GameObject> mVoidBreacher = new List<GameObject>();
    public List<GameObject> mFamiliars = new List<GameObject>();
    public List<GameObject> mActionCards = new List<GameObject>();

    public CardTypePicking mPhase = CardTypePicking.VoidBreacher;
    public Card.CardDominion mDeckType;

    public List<GameObject> mAllAvailableCards = new List<GameObject>();
    public List<GameObject> mFlintExpansionCards = new List<GameObject>();
    public List<GameObject> mRippleExpansionCards = new List<GameObject>();
    public List<GameObject> mLarkExpansionCards = new List<GameObject>();
    

    public float mGUIStartPosX;
    public float mGUIStartPosY;

    private bool mReadyToMoveForward = false;
    [SerializeField] private bool mStillSelecting = true;
    public Assets.CustomScripts.NetworkManager mNetworkManager;
    public Assets.CustomScripts.InformationManager mInformationManager;
	
	//Strings for text fields for saving/loading decks via strings of numeric ID codes
	private string mDeckCodeInput ="";
	private string mDeckCodeOutput = "";


	//For keeping track of what scene we are currently in
	public enum DeckBuilderScene{Lobby, Matchup, Game}; 
	public DeckBuilderScene mDeckBuilderCurrentScene;

	[SerializeField] private GUIStyle mCardDisplayStyle;


	//For using the new UI system -Adam
	[SerializeField] private GameObject mVBHolder;
	[SerializeField] private GameObject mFamHolder;
	[SerializeField] private GameObject mACHolder;
	[SerializeField] private GameObject mDCButtonPrefab;
	public DCChosenCard[] mDCChosenCards;

	// Use this for initialization
	void Start () 
    {

		if (mInformationManager == null)
		{
			mInformationManager = FindObjectOfType(typeof(Assets.CustomScripts.InformationManager)) as Assets.CustomScripts.InformationManager;
		}
		if (mNetworkManager == null)
		{
			mNetworkManager = FindObjectOfType(typeof(Assets.CustomScripts.NetworkManager)) as Assets.CustomScripts.NetworkManager;
		}

		//Make the VoidBreacher buttons (hopefully this will still work once new VBs are added in expansions) -Adam
		if(mVBHolder != null && mFamHolder != null && mACHolder != null)
		{
			foreach(GameObject vbCard in mAllAvailableCards)
			{
				if(vbCard.GetComponent<Card>().cardType == Card.CardType.VoidBreacher)
				{
					GameObject newVBButton = Instantiate (mDCButtonPrefab, Vector3.zero, Quaternion.identity) as GameObject;
					newVBButton.transform.SetParent(mVBHolder.transform);
					newVBButton.transform.localScale = new Vector3(1,1,1);
					newVBButton.GetComponent<DCSelectionButton>().mAssociatedCard = vbCard.GetComponent<Card>();
					newVBButton.GetComponent<DCSelectionButton>().mDeckBuilder = this;
					mVBHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards.Add(newVBButton);
				}
			}
		}
	}//END of Start()
	
	// Update is called once per frame
	void Update () 
    {
		if (Application.loadedLevelName == "CardPurchaseMenu")
		{
			Destroy(transform.gameObject);
		}

		//Showing/Hiding the new system UI for card selection -Adam
		if(mVBHolder != null && mFamHolder != null && mACHolder != null)
		{
			switch(mPhase)
			{
			case CardTypePicking.VoidBreacher:
				mVBHolder.SetActive(true);
				mFamHolder.SetActive(false);
				mACHolder.SetActive (false);
				break;
			case CardTypePicking.Familiar:
				mVBHolder.SetActive(true);
				mFamHolder.SetActive(true);
				mACHolder.SetActive (true);
				break;
//			case CardTypePicking.Action:
//				mVBHolder.SetActive(true);
//				mFamHolder.SetActive(false);
//				mACHolder.SetActive (true);
//				break;
			case CardTypePicking.End:
				mVBHolder.SetActive(false);
				mFamHolder.SetActive(false);
				mACHolder.SetActive (false);
				foreach(DCChosenCard chosenCard in mDCChosenCards)
				{
					chosenCard.gameObject.SetActive (false);
				}
				break;
			default:
				break;

			}
		}


	}


	void OnLevelWasLoaded()
	{
		mStillSelecting = true;

		if(Application.loadedLevelName == "Lobby")
		{
			mDeckBuilderCurrentScene = DeckBuilderScene.Lobby;
		}
		if(Application.loadedLevelName == "Matchup")
		{
			mDeckBuilderCurrentScene = DeckBuilderScene.Matchup;
			mFamHolder = GameObject.Find("FamHolder");
			mVBHolder = GameObject.Find("VBHolder");
			mACHolder = GameObject.Find("ACHolder");
			if(Network.isServer)
			{
				GameObject.Find("P1VB").GetComponent<Image>().sprite = mVoidBreacher[0].GetComponent<Card>().mCardArt;
			}
			else
			{
				GameObject.Find("P2VB").GetComponent<Image>().sprite = mVoidBreacher[0].GetComponent<Card>().mCardArt;
			}
		}


	}

	//For telling the Room Manager in Matchup to update voidbreacher names.
	public void CallToUpdateVoidBreacherNames()
	{
		MatchupRoomManager roomManager = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
		roomManager.GetComponent<NetworkView>().RPC("UpdateVoidBreacherNames", RPCMode.All, mVoidBreacher[0].GetComponent<Card>().cardName);

	}

	//For not deleting on moving to a new scene
	void Awake()
	{
		if (mDeckBuilderCurrentScene != DeckBuilderScene.Game)
		{
			DontDestroyOnLoad(transform.gameObject);
		}
		else
		{
			Destroy(transform.gameObject);
		}
	}

    void OnGUI()
    {
		if (mStillSelecting)
		{
//			//Where we display the Voidbreacher that's been chosen
//	        GUI.Label(new Rect(Screen.width * 0.23f, Screen.height * 0.10f, Screen.width * 0.40f, Screen.height * 0.05f), "Chosen VoidBreacher:");
//			float spacing = 0.05f;
//			//Display all the Voidbreachers (there should be a max of 1) that have been chosen for the deck
//			foreach (GameObject card in mVoidBreacher)
//			{
//				GUI.Label(new Rect(Screen.width * 0.23f, Screen.height * (0.10f + spacing), Screen.width * 0.40f, Screen.height * 0.05f),  new GUIContent(card.GetComponent<Card>().cardName, 
//				                                                                                                                                                      card.GetComponent<Card>().cardName));
//			}
//			//Where we display the Familiars that have been chosen
//			GUI.Label(new Rect(Screen.width * 0.43f, Screen.height * 0.10f, Screen.width * 0.60f, Screen.height * 0.20f), "Chosen Familiars:");
//			spacing = 0.05f;
//			//Display all the Familiars (there should be a max of 6) that have been chosen for the deck
//			foreach (GameObject card in mFamiliars)
//			{
//				GUI.Label(new Rect(Screen.width * 0.43f, Screen.height * (0.10f + spacing), Screen.width * 0.60f, Screen.height * 0.05f), new GUIContent(card.GetComponent<Card>().cardName, 
//				                                                                                                                                                     card.GetComponent<Card>().cardName));
////				                                                                                                                                                     + "\nDominion: " + card.GetComponent<Card>().cardDominion 
////					                                                                                                                                                     +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription));
//				spacing += 0.05f;
//			}
//			//Where we display the Action Cards that have been chosen
//			GUI.Label(new Rect(Screen.width * 0.63f, Screen.height * 0.10f, Screen.width * 0.80f, Screen.height * 0.20f), "Chosen Action Cards:");
//			spacing = 0.05f;
//			//Display all the Action Cards (there should be a max of 6) that have been chosen for the deck
//			foreach (GameObject card in mActionCards)
//			{
//				GUI.Label(new Rect(Screen.width * 0.63f, Screen.height * (0.10f + spacing), Screen.width * 0.80f, Screen.height * 0.05f), new GUIContent(card.GetComponent<Card>().cardName, 
//				                                                                                                                                                     card.GetComponent<Card>().cardName));
////				                                                                                                                                                     + "\nDominion: " + card.GetComponent<Card>().cardDominion 
////					                                                                                                                                                     +"\nSource Stone Cost: " + card.GetComponent<Card>().sourceStoneCost+ "\n" + card.GetComponent<Card>().mCardDescription));
//				spacing += 0.05f;
//			}
//			mGUIStartPosX = Screen.width * 0.1f;
//			mGUIStartPosY = Screen.height * 0.5f;
//
//			int guiCardNumber = 1;
			
			//A switch that checks which section of deck construction we are on (Picking Voidbreachers, Familiars, or Action Cards)
	        switch(mPhase)
	        {
			//Picking Voidbreachers
			case CardTypePicking.VoidBreacher:
				if (mDeckBuilderCurrentScene == DeckBuilderScene.Lobby)
				{

					if (mVoidBreacher.Count == 1)
					{
						mReadyToMoveForward = true;
					}
				}
				break; //End of picking Voidbreachers

			//Picking familiars
			case CardTypePicking.Familiar:

				if (mFamiliars.Count < 6 || mActionCards.Count < 6)
				{
					mReadyToMoveForward = false;
				}
				else if (mFamiliars.Count == 6 && mActionCards.Count == 6)
				{
					mReadyToMoveForward = true;
				}
				break;//End of picking familiars


			}

			//Buttons for going forward and back during the lobby
			if(mDeckBuilderCurrentScene == DeckBuilderScene.Lobby)
			{
				//Button to go to the next section once cards have been selected
				if (mReadyToMoveForward && mPhase != CardTypePicking.End)
				{
					if (GUI.Button(new Rect(Screen.width * 0.88f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Next")))
					{
						mPhase++;
						AdvancePhaseButtonSwap();
						mReadyToMoveForward = false;
					}
				}
				//Once all the cards have been picked, have a button to add them to the deck and move to the lobby
				else if (mPhase == CardTypePicking.End)
				{
					if (GUI.Button(new Rect(Screen.width * 0.875f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Move To Lobby")))
					{
						SendDeckToInformationManager();
					}
				}
				//Button to go back a step of picking
				if (mPhase != CardTypePicking.VoidBreacher)
				{
					if (GUI.Button(new Rect(Screen.width * 0.775f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Previous")))
					{
						mPhase--;
					}
				}
			}//End of Lobby scene forward/back buttons

			//Buttons for choosing to edit Familiar and Action cards in the Matchup scene
			if(mDeckBuilderCurrentScene == DeckBuilderScene.Matchup)
			{
				//Button to go to do last-minute editing of Familiars
				if (GUI.Button(new Rect(Screen.width * 0.775f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Edit Cards")))
				{
					mPhase = CardTypePicking.Familiar;
					mReadyToMoveForward = false;
					GenerateSelectionButtions();
					SetChosenCards();
				}

				//Once all the cards have been picked, have a button to add them to the deck and lock in the deck
				else if (mFamiliars.Count == 6 && mActionCards.Count == 6 && mVoidBreacher.Count == 1 && mNetworkManager.mFoundMatchupManager)
				{
					if (GUI.Button(new Rect(Screen.width * 0.875f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Lock In Deck")))
					{
						//Get rid of all the deck editing buttons before destroying self -Adam
						mVBHolder.SetActive(false);
						mFamHolder.SetActive(false);
						mACHolder.SetActive (false);
						foreach(DCChosenCard chosenCard in mDCChosenCards)
						{
							chosenCard.gameObject.SetActive (false);
						}

						//Finalize the deck and get ready to go to the main game
						mNetworkManager.mReadyToStartMatch = true;
						mStillSelecting = false;
						SendDeckToInformationManager();
						mDeckBuilderCurrentScene = DeckBuilderScene.Game;

						MatchupRoomManager roomManager = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
						roomManager.GetComponent<NetworkView>().RPC("UpdateReadyCount", RPCMode.All);

						Destroy(transform.gameObject);

					}
				}
			}
		




		

			//Buttons and Text fields for saving/loading decks via ID Codes
			//Load Deck
//			mDeckCodeInput = GUI.TextField(new Rect(Screen.width*0.875f, Screen.height*0.72f, Screen.width*0.1f, Screen.height*0.1f), mDeckCodeInput, 117);
//			if(GUI.Button(new Rect(Screen.width*0.775f, Screen.height*0.72f, Screen.width*0.1f, Screen.height*0.1f), "Load deck from code:"))
//			{
//				BuildDeckFromIDCode();
//			}
//			//Save Deck
//			mDeckCodeOutput = GUI.TextField(new Rect(Screen.width*0.875f, Screen.height*0.84f, Screen.width*0.1f, Screen.height*0.1f), mDeckCodeOutput, 117);
//			if(GUI.Button(new Rect(Screen.width*0.775f, Screen.height*0.84f, Screen.width*0.1f, Screen.height*0.1f), "Generate code for this deck:"))
//			{
//				GenerateDeckIDCode();
//			}
			//End of Buttons and Text Fields for saving/loading decks via ID Codes

		}

    }//END of OnGUI()

	//Function for swapping out card selection buttons when advancing the phase
	void AdvancePhaseButtonSwap()
	{
		switch(mPhase)
		{
		case CardTypePicking.Familiar:
			GenerateSelectionButtions();
			break;
//		case CardTypePicking.Action:
//
//			break;
		default:
			break;
		}
	}//END of AdvancePhaseButtonSwap()

	//Function for swapping out card selection buttons when going back a phase
	void RevertPhaseButtonSwap()
	{
		
	}//END of RevertPhaseButtonSwap()


	//Send the deck info from the DeckBuilder to the InformationManager that will actually go to the game scene
	void SendDeckToInformationManager()
	{
		mInformationManager.mDeck.Clear();
		mInformationManager.mDeck.Add(mVoidBreacher[0]);
		foreach (GameObject card in mFamiliars)
		{
			mInformationManager.mDeck.Add(card);
		}
		foreach (GameObject card in mActionCards)
		{
			mInformationManager.mDeck.Add(card);
		}
		
		if (mInformationManager.mDeck.Count == 13)
		{
			mNetworkManager.mShowLobby = true;
			mStillSelecting = false;
		}
	}//END of SendDeckToInformationManager

	//Take in a string and build a deck from it
	public void BuildDeckFromIDCode(InputField deckCode)
	{
		//create an array of strings by splitting up the ID code and taking out the hyphens
		string[] deckCardCodes = deckCode.text.Split(new Char [] {'-'});
		//Clear the existing deck
		mFamiliars.Clear();
		mActionCards.Clear();

		//Only have codes edit the Voidbreacher and deck type if you are in the lobby scene
		if(mDeckBuilderCurrentScene == DeckBuilderScene.Lobby)
		{
			mVoidBreacher.Clear();
			mPhase = CardTypePicking.VoidBreacher;

			//find the Voidbreacher in the deck code, add it to the deck, and set the deck type to match
			for (int i = 0; i < deckCardCodes.Length; i++)
			{
				foreach(GameObject availableCard in mAllAvailableCards)
				{
					if (availableCard.GetComponent<Card>().mCardIDCode == deckCardCodes[i] && availableCard.GetComponent<Card>().cardType == Card.CardType.VoidBreacher)
					{
						if (mVoidBreacher.Count == 0)
						{
							mVoidBreacher.Add(availableCard);
							mDeckType = availableCard.GetComponent<Card>().cardDominion;
						}
					}
				}
			}
		}
		//Add the rest of the cards if you have a Void breacher
		if (mVoidBreacher.Count == 1)
		{
			//Find the rest of the cards in the deck code
			for (int i = 0; i < deckCardCodes.Length; i++)
			{
				GenerateSelectionButtions();
				foreach(GameObject availableCard in mAllAvailableCards)
				{
					//Add Familiars
					if (availableCard.GetComponent<Card>().mCardIDCode == deckCardCodes[i] && availableCard.GetComponent<Card>().cardType == Card.CardType.Familiar)
					{
						if (mFamiliars.Count < 6 && mDeckType == availableCard.GetComponent<Card>().cardDominion)
						{
							mFamiliars.Add(availableCard);
						}
					}
					//Add Action Cards
					else if (availableCard.GetComponent<Card>().mCardIDCode == deckCardCodes[i] && availableCard.GetComponent<Card>().cardType != Card.CardType.VoidBreacher)
					{
						if (mActionCards.Count < 6 && mDeckType == availableCard.GetComponent<Card>().cardDominion)
						{
							mActionCards.Add(availableCard);
						}
					}
				}
			}
			SetChosenCards();
		}
	}//END of BuildDeckFromIDCode()

	//Read the current deck and output a string that can be saved and pasted in later
	public void GenerateDeckIDCode(InputField deckCode)
	{
		//reset the output code
		mDeckCodeOutput = "";


		if(mVoidBreacher.Count > 0)
		{
			mDeckCodeOutput += mVoidBreacher[0].GetComponent<Card>().mCardIDCode + "-";
		}
		if(mFamiliars.Count > 0)
		{
			for (int i = 0; i < mFamiliars.Count; i ++)
			{
				mDeckCodeOutput += mFamiliars[i].GetComponent<Card>().mCardIDCode + "-";
			}
		}
		if(mActionCards.Count > 0)
		{
			for (int i = 0; i < mActionCards.Count; i ++)
			{
				mDeckCodeOutput += mActionCards[i].GetComponent<Card>().mCardIDCode + "-";
			}
		}

		deckCode.text = mDeckCodeOutput;

	}//END of GenerateDeckIDCode()

	void GenerateSelectionButtions()
	{
		foreach(GameObject currentFamCard in mFamHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards)
		{
			Destroy(currentFamCard.gameObject);
		}
		mFamHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards.Clear();
		foreach(GameObject famCard in mAllAvailableCards)
		{
			if(famCard.GetComponent<Card>().cardType == Card.CardType.Familiar && famCard.GetComponent<Card>().cardDominion == mDeckType)
			{
				GameObject newFamButton = Instantiate (mDCButtonPrefab, Vector3.zero, Quaternion.identity) as GameObject;
				newFamButton.transform.SetParent(mFamHolder.transform);
				newFamButton.transform.localScale = new Vector3(1,1,1);
				newFamButton.GetComponent<DCSelectionButton>().mAssociatedCard = famCard.GetComponent<Card>();
				newFamButton.GetComponent<DCSelectionButton>().mDeckBuilder = this;
				mFamHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards.Add(newFamButton);
			}
		}
		
		foreach(GameObject currentACCard in mACHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards)
		{
			Destroy(currentACCard.gameObject);
		}
		mACHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards.Clear();
		foreach(GameObject acCard in mAllAvailableCards)
		{
			if( (acCard.GetComponent<Card>().cardType == Card.CardType.Action || acCard.GetComponent<Card>().cardType == Card.CardType.Minion)
			   && acCard.GetComponent<Card>().cardDominion == mDeckType)
			{
				GameObject newACButton = Instantiate (mDCButtonPrefab, Vector3.zero, Quaternion.identity) as GameObject;
				newACButton.transform.SetParent(mACHolder.transform);
				newACButton.transform.localScale = new Vector3(1,1,1);
				newACButton.GetComponent<DCSelectionButton>().mAssociatedCard = acCard.GetComponent<Card>();
				newACButton.GetComponent<DCSelectionButton>().mDeckBuilder = this;
				mACHolder.GetComponent<DCCardButtonHolder>().mHeldButtonCards.Add(newACButton);
			}
		}
	}//END of GenerateSelectionButtons()

	void SetChosenCards()
	{
		//Set up Chosen Cards -Adam
		for (int k = 0; k < 12; k++)
		{
			if(k<6)
			{
				if(k < mFamiliars.Count)
				{
					mDCChosenCards[k].mAssociatedCard = mFamiliars[k].GetComponent<Card>();
					mDCChosenCards[k].gameObject.SetActive (true);
				}
				else
				{
					mDCChosenCards[k].gameObject.SetActive (false);
				}
			}
			else
			{
				if(k < mActionCards.Count+6)
				{
					mDCChosenCards[k].mAssociatedCard = mActionCards[k-6].GetComponent<Card>();
					mDCChosenCards[k].gameObject.SetActive (true);
				}
				else
				{
					mDCChosenCards[k].gameObject.SetActive (false);
				}
				
			}
		}

	}//END of SetChosenCards()

}
