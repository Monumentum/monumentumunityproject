﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Creature : MonoBehaviour 
{
	//A master class that gets applied to anything capable of moving, including Familiars, Minions, AND Voidbreachers
	//Specific creatures get their own scripts that inherit from this one and add/modify additional behaviors
	//Whether a creature is counted as a Familiar, Minion, or Voidbreacher is determined by its GameObject's tag, which is set on its prefab in-editor

	//basic info
	public int controllingPlayer; //Who owns this creature
	public enum CreatureDominion{Irotar,Vosira,Rhavik}; //The creature's faction
	public CreatureDominion creatureDominion;
	public bool character = false;
	public GameObject currentSpace;
	protected bool selectedByPlayer = false;
	public string creatureName;
	public int sourceStoneCost = 0;
	public PlayerHandManager handManager;
	public PlayerClickController clickManager;

	//Summoning Related Info
	public List<GameObject> carriedLimboCreatures = new List<GameObject>();//Have a list of creatures summoned to Limbo sharing this space
	public List<int> limboCreatureHandCardNumber = new List<int>();//Have a list of associated hand card numbers of  Limbo creatures sharing this space
	public int handCardNumber = 0;
	public bool inLimbo = false;
	public int limboCreatureLookingAt;

	//Movement related info
	public bool canMove = false;
    public int mTurnsStunned = 0;
	public int moveSpeed = 4; //The number of squares this creature can move at once
	protected bool movementSquaresAreHighlighted = false;
	public bool lockedInCombat = false;
	public int crippled = 0;
    public bool mTemporarilyWeakened = false;
	public Vector3 mNavMeshDestination;
	public GameObject mDestinationSpace;
	public bool mIsMoving = false;
	public List<GameObject> mPathSquares;
	private Vector3 mOffsetFromSquare = new Vector3(0, 0.5f, 0);

	//Status effect info
    public List<string> mBoosts = new List<string>();
    public Dictionary<string, int> mTempBoosts = new Dictionary<string, int>();
    public List<string> mWeakeningBoosts = new List<string>();
    public List<Card> mCombatCards = new List<Card>();
	public List<Sprite> mRuneCardImages = new List<Sprite>();

	//Combat Related info
	public int strength = 0;
	public int fortitude = 0;
	public bool canAttack;
    public int mMaxAttacksATurn = 1;
    public int mNumberOfAttacksLeft = 1;
	public int attackRange = 1;
	public int hitPoints = 2;
	bool combatSquaresAreHighlighted = false;
	[HideInInspector] public int startingHitPoints = 2;
	public List<Creature> mCombatLockTargets = new List<Creature>();

    public bool mElusive = false;

	//monument related info
	public bool mCanAttackMonument = false;
	public Monument mAttackableMonument;

	#region the Rune mechanics were written by Sean Lambidn
	//Rune info
	public Dictionary<Rune.RuneType, List<Rune>> mActiveRunes = new Dictionary<Rune.RuneType,List<Rune>>();
    public int mNumberOfRunes = 0;

	protected TurnPhaseManager turnManager;
    public List<Rune> mAttackRunes = new List<Rune>();
    public List<Rune> mConclusionRunes = new List<Rune>();
    public List<Rune> mAttackedRunes = new List<Rune>();
    public List<Rune> mSummoningRunes = new List<Rune>();
    public List<Rune> mTriggerRunes = new List<Rune>();
    public List<Rune> mDamagedRunes = new List<Rune>();
	#endregion

	//Info for mouseover tooltip
	MouseOverInfoManager mMouseOverInfoManager;
	public string mCreatureDescription = "";


	//For the implementation of the 3D modeled-stand
	[SerializeField] GameObject mStandBase;

	// Use this for initialization
	public virtual void Start () 
	{
		//Find the various controllers/managers that need referencing
		turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
		clickManager = turnManager.playerControllers[controllingPlayer-1];
		handManager = clickManager.gameObject.GetComponent<PlayerHandManager>();
		
		//For moving a creature as it is summoned
		if (mPathSquares.Count>0)
		{
			MoveToSpace();
		}
       
		#region the Rune mechanics were written by Sean Lambidn
		//set up Rune lists
        mActiveRunes.Add(Rune.RuneType.Attacking, mAttackRunes);
        mActiveRunes.Add(Rune.RuneType.Conclusion, mConclusionRunes);
        mActiveRunes.Add(Rune.RuneType.IsAttacked, mAttackedRunes);
        mActiveRunes.Add(Rune.RuneType.Summoning, mSummoningRunes);
        mActiveRunes.Add(Rune.RuneType.OnTrigger, mTriggerRunes);
        mActiveRunes.Add(Rune.RuneType.IsDamaged, mDamagedRunes);
		#endregion

		//decide whether or not it is a "Character" (meaning Familiar or Minion)
		if (gameObject.tag == "Familiar" || gameObject.tag == "Minion")
		{
			character = true;
		}
		if (gameObject.tag == "VoidBreacher" || gameObject.tag == "Minion")
		{
			hitPoints = 1;
		}

		startingHitPoints = hitPoints;
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

		#region Temporary code to make it possible to tell the difference between units while still using placeholder artwork
		//Changing color based on Dominion.  TAKE THIS OUT ONCE WE HAVE ART
		switch(creatureDominion)
		{
		case CreatureDominion.Rhavik:
			this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
			if(tag == "VoidBreacher")
			{
				this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.magenta);
			}
			break;
		case CreatureDominion.Irotar:
			this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
			if(tag == "VoidBreacher")
			{
				this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
			}
			break;
		case CreatureDominion.Vosira:
			this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
			if(tag == "VoidBreacher")
			{
				this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
			}
			break;
		default:
			break;
		}
		#endregion

		if(mStandBase != null)
		{
			if(controllingPlayer == 1)
			{
				mStandBase.GetComponent<Renderer>().material.color = Color.white;
			}
			else if(controllingPlayer == 2)
			{
				mStandBase.GetComponent<Renderer>().material.color = Color.black;
			}
		}

	}//End of Start()
	
	// Update is called once per frame
	public virtual void Update () 
	{
		//For moving the creature to the space you told it to go to
		if (mIsMoving)
		{

			if (Vector3.Distance(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare) > 0.1f)
			{
				Quaternion spotToLook = Quaternion.LookRotation (mPathSquares[0].transform.position - transform.position);
				spotToLook = Quaternion.Euler (new Vector3(0f, spotToLook.eulerAngles.y + 90f, 0f));
				//transform.LookAt(mPathSquares[0].transform.position);
				transform.rotation = Quaternion.Slerp (transform.rotation, spotToLook,0.1f);
			//	transform.rotation = Quaternion.Euler(new Vector3(0f, transform.rotation.eulerAngles.y+90f, 0f));
				transform.position = Vector3.Lerp(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare, 0.1f);
			}
			else if (Vector3.Distance(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare) <= 0.1f  && mPathSquares.Count > 1)
			{
				Quaternion spotToLook = Quaternion.LookRotation (mPathSquares[0].transform.position - transform.position);
				//transform.LookAt(mPathSquares[0].transform.position);
				spotToLook = Quaternion.Euler (new Vector3(0f, spotToLook.eulerAngles.y + 90f, 0f));
				transform.rotation = Quaternion.Slerp (transform.rotation, spotToLook,0.1f);
				//transform.rotation = Quaternion.Euler(new Vector3(0f, transform.rotation.eulerAngles.y+90f, 0f));
				mPathSquares[0].GetComponent<BoardSquare>().mDistanceFromMover = 99;
				mPathSquares.RemoveAt(0);
			}
			else if (Vector3.Distance(transform.position, mPathSquares[0].transform.position+mOffsetFromSquare) <= 0.1f  && mPathSquares.Count == 1)
			{
				Quaternion spotToLook = Quaternion.LookRotation (Camera.main.transform.position - transform.position);
				//transform.LookAt(Camera.main.transform.position);
				spotToLook = Quaternion.Euler (new Vector3(0f, spotToLook.eulerAngles.y + 90f, 0f));
				transform.rotation = Quaternion.Slerp (transform.rotation, spotToLook,0.1f);
				//transform.rotation = Quaternion.Euler(new Vector3(0f, transform.rotation.eulerAngles.y+90f, 0f));
				LerpPathArrived();
			}
		}
		else if ((GetComponent<NetworkView>().isMine && turnManager.playerTurn == controllingPlayer))
		{
			transform.LookAt(Camera.main.transform.position);
			transform.rotation = Quaternion.Euler(new Vector3(0f, transform.rotation.eulerAngles.y+90f, 0f));
			
		}
		else// if((!GetComponent<NetworkView>().isMine && turnManager.playerTurn != controllingPlayer) )
		{
			//For making the model turn to look at the camera -Adam
			Quaternion spotToLook = Quaternion.LookRotation (Camera.main.transform.position - transform.position);
			spotToLook = Quaternion.Euler (new Vector3(0f, spotToLook.eulerAngles.y + 90f, 0f));
			transform.rotation = Quaternion.Slerp (transform.rotation, spotToLook,0.1f);

		}

	}//End of Update()

	//Providing a tooltip about the creature when a player mouses over it
	void OnMouseEnter()
	{
		mMouseOverInfoManager.SetCreatureToolTip(true, name+"\nHP: " + hitPoints + "/" + startingHitPoints 
		                                         + "\nStength: " + strength + "\nFortitude: " + fortitude 
		                                         + "\n Speed: " + moveSpeed + "\n\n" + mCreatureDescription);
		mMouseOverInfoManager.mCardImage = handManager.deck[handCardNumber].mCardArt;
		//Send the current Strength/Fortitude
		mMouseOverInfoManager.mCurrentStrength = strength;
		mMouseOverInfoManager.mCurrentFortitude = fortitude;
		//Decide whether or not to use Strength overlays
		if(strength > GetComponent<StatusDisplay>().mBaseStrength)
		{
			mMouseOverInfoManager.mStrengthStatus = 1;
		}
		else if(strength < GetComponent<StatusDisplay>().mBaseStrength)
		{
			mMouseOverInfoManager.mStrengthStatus = 2;
		}
		else
		{
			mMouseOverInfoManager.mStrengthStatus = 0;
		}
		//Decide whether or not to use Fortitude overlays
		if(fortitude > GetComponent<StatusDisplay>().mBaseFortitude)
		{
			mMouseOverInfoManager.mFortitudeStatus = 1;
		}
		else if(fortitude < GetComponent<StatusDisplay>().mBaseFortitude)
		{
			mMouseOverInfoManager.mFortitudeStatus = 2;
		}
		else
		{
			mMouseOverInfoManager.mFortitudeStatus = 0;
		}

		//Send images for applied runes
		mMouseOverInfoManager.mRuneCardImages.Clear();
		foreach(Sprite runeImage in mRuneCardImages)
		{
			mMouseOverInfoManager.mRuneCardImages.Add(runeImage);
		}

	}//END of OnMouseEnter()

	void OnMouseExit()
	{
		//mMouseOverInfoManager.mStrengthStatus = 0;
		//mMouseOverInfoManager.mFortitudeStatus = 0;
		mMouseOverInfoManager.SetCreatureToolTip(false, "");		
	}//END of OnMouseExit()





	//for getting selected/deselected by the player
	public void BeClickedOn()
	{
		selectedByPlayer = true;
	}//End of BeClickedOn()

	public void BeUnclicked()
	{
		selectedByPlayer = false;
		if (movementSquaresAreHighlighted || combatSquaresAreHighlighted)
        {
            UnHighlightSquares();
        }
			
	}//End of BeUnclicked

	//Highlight squares you can move to when clicked during the Movmement Phase
	public void HighlightMovableSquares()
	{
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && canMove && !movementSquaresAreHighlighted)
		{
			int moveRange = moveSpeed;
			//check for crippled status
			switch (crippled)
			{
				case 0:
					moveRange = moveSpeed;
					break;
				case 1:
					moveRange = 2;
					break;
				case 2:
					moveRange = 0;
					break;
				default:
					moveRange = moveSpeed;
					break;
			}
			Debug.Log("I'm selected an ready to move");
			if (moveRange == 0)
			{
				canMove = false;
			}
			else
			{
				for (int i = 1; i <= moveRange; i++)
				{
					//highlight adjacent squares
					if (i == 1)
					{
						foreach(BoardSquare potentialMoveTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
						{
							if (potentialMoveTarget.occupier == null || (potentialMoveTarget.occupier.GetComponent<Creature>()!= null && potentialMoveTarget.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer))
							{
								potentialMoveTarget.inMovementRange = true;
								potentialMoveTarget.mDistanceFromMover = i;
							}
						}
					}
					//Highlight other squares in range
					else
					{
						BoardSquare[] highlightedMoveTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
						List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
						foreach (BoardSquare newMoveTarget in highlightedMoveTargets)//Add any already highlighted squares to the list
						{
							if (newMoveTarget.inMovementRange == true)
							{
								highlightedSquareList.Add(newMoveTarget);
							}
							else
							{
								newMoveTarget.mDistanceFromMover = 99;
							}
						}
						foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
						{
							foreach(BoardSquare potentialMoveTarget in listedSqaure.neighborSquares)
							{
								if (potentialMoveTarget.occupier == null || (potentialMoveTarget.occupier.GetComponent<Creature>()!= null && potentialMoveTarget.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer))
								{
									potentialMoveTarget.inMovementRange = true;
									if (potentialMoveTarget.mDistanceFromMover == 99)
									{
										potentialMoveTarget.mDistanceFromMover = i;
									}
								}
							}
						}

					}
					//unhighlight occupied squares in range
					if (i == moveRange)
					{
						movementSquaresAreHighlighted = true;
						BoardSquare[] moveTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
						foreach (BoardSquare invalidMoveTarget in moveTargetToUnHighlight)//Unhighlight any squares that are Highlighted
						{
							if (invalidMoveTarget.occupier != null)
							{
								invalidMoveTarget.inMovementRange = false;
							}
						}
			

					}
				}
			}
		}
	} //End of HighlightMovableSquares()

	//Highlight squares creatures carried by this creature can be told to move to upon being summoned to the board
	public void HighlightSummonSquares(Creature creatureToSummon)
	{
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && !movementSquaresAreHighlighted)
		{
			Debug.Log("I'm selected an ready to move");
			for (int i = 1; i <= creatureToSummon.moveSpeed; i++)
			{
				//highlight adjacent squares
				if (i == 1)
				{
					foreach(BoardSquare potentialSummonTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
					{
						if (potentialSummonTarget.occupier == null || (potentialSummonTarget.occupier.GetComponent<Creature>()!=null && potentialSummonTarget.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer))
						{
							potentialSummonTarget.validSummonSpot = true;
							potentialSummonTarget.mDistanceFromMover = i;

						}
					}
				}
				//Highlight other squares in range
				else
				{
					BoardSquare[] highlightedSummonTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
					List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
					foreach (BoardSquare newSummonTarget in highlightedSummonTargets)//Add any already highlighted squares to the list
					{
						if (newSummonTarget.validSummonSpot == true)
						{
							highlightedSquareList.Add(newSummonTarget);
						}
						else
						{
							newSummonTarget.mDistanceFromMover = 99;
						}

					}
					foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
					{
						foreach(BoardSquare potentialSummonTarget in listedSqaure.neighborSquares)
						{
							if (potentialSummonTarget.occupier == null || (potentialSummonTarget.occupier.GetComponent<Creature>()!=null && potentialSummonTarget.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer))
							{
								potentialSummonTarget.validSummonSpot = true;
								if (potentialSummonTarget.mDistanceFromMover == 99)
								{
									potentialSummonTarget.mDistanceFromMover = i;
								}

							}
						}
					}
					
				}
				//unhighlight occupied squares in range
				if (i == creatureToSummon.moveSpeed)
				{
					movementSquaresAreHighlighted = true;
					BoardSquare[] summonTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
					foreach (BoardSquare invalidSummonTarget in summonTargetToUnHighlight)//Unhighlight any squares that are Highlighted
					{
						if (invalidSummonTarget.occupier != null)
						{
							invalidSummonTarget.validSummonSpot = false;
						}
					}


				}
			}
		}
	}//End of HighlightSummonSquares

	//Highlighting squares occupied by targets this creature can attack
	public void HighlightCombatSquares()
	{
		Debug.Log("Highlighting Squares");
		if (selectedByPlayer && canAttack && !combatSquaresAreHighlighted)
		{
			Debug.Log("I'm selected an ready to move");
			for (int i = 1; i <= attackRange; i++)
			{
				//highlight adjacent squares
				if (i == 1)
				{
					foreach(BoardSquare potentialCombatTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
					{
						potentialCombatTarget.hasAttackableTarget = true;
					}
				}
				//Highlight other squares in range
				else
				{
					BoardSquare[] highlightedCombatTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
					List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
					foreach (BoardSquare newCombatTarget in highlightedCombatTargets)//Add any already highlighted squares to the list
					{
						if (newCombatTarget.hasAttackableTarget == true)
						{
							highlightedSquareList.Add(newCombatTarget);
						}
					}
					foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
					{
						foreach(BoardSquare potentialCombatTarget in listedSqaure.neighborSquares)
						{
							if (listedSqaure.occupier == null || 
							    (listedSqaure.occupier.GetComponent<Creature>() !=null && listedSqaure.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer))
							{
                                if (Vector3.Distance(potentialCombatTarget.gameObject.transform.position, currentSpace.gameObject.transform.position) == i)
                                {
                                    potentialCombatTarget.hasAttackableTarget = true;
                                }
							}
						}
					}
					
				}
				//unhighlight unoccupied/friendly squares in range
				if (i == attackRange)
				{
					combatSquaresAreHighlighted = true;
					BoardSquare[] combatTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
                    foreach (BoardSquare invalidCombatTarget in combatTargetToUnHighlight)//Unhighlight any squares that are Highlighted
					{
                        if (invalidCombatTarget.occupier == null || (invalidCombatTarget.occupier.GetComponent<Creature>() != null && invalidCombatTarget.occupier.GetComponent<Creature>().controllingPlayer == controllingPlayer) 
                            || (lockedInCombat && invalidCombatTarget.occupier.GetComponent<Creature>() != null && invalidCombatTarget.occupier.GetComponent<Creature>().tag == "VoidBreacher"))
						{
                            invalidCombatTarget.hasAttackableTarget = false;
						}
					}
					
				}
			}
		}
	} //End of HighlightCombatSquares()

	//Function for attacking a target
	public virtual bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1)
	{
        if (mCombatCards.Count > 0)
        {
            foreach (Card card in mCombatCards)
            {
                card.BattleEffect(targetedCreature);
            }
        }
        List<Rune> attackRunes;
        if (mActiveRunes.TryGetValue(Rune.RuneType.Attacking, out attackRunes))
        {
            foreach (Rune rune in attackRunes)
            {
				Debug.Log("DOING THE ATTACKING RUNE " + rune.name);

                rune.ActivateRune(this);
            }
        }
        bool didIHit = false;
		//Roll 2d6 and add your Strength
		int thisAttackPower = strength;
        int rollBonus = 0;

        for (int i = 0; i < numberOfRolls; i++)
        {
            int dieRoll = 0;
            dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
            dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));

            if (dieRoll > rollBonus)
            {
                rollBonus = dieRoll;
            }
        }
        thisAttackPower += rollBonus;
		turnManager.GetComponent<CombatInfoDisplay>().DeclareAttackResult(rollBonus);

		//Compare your results to the target's Fortitude
		if (targetedCreature.tag == "VoidBreacher")//Automatically hit a VoidBreacher
		{
			targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, true);
            didIHit = true;
			Debug.Log(this.name + " hit " + targetedCreature.name + " leaving it with " + targetedCreature.hitPoints + " HP");
		}
		else if (thisAttackPower == strength+2)//Automatically miss if you roll a 1 on both dice
		{
			Debug.Log(this.name + " missed " + targetedCreature.name);
            targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, false);
		}
		else if (thisAttackPower > targetedCreature.fortitude || thisAttackPower==strength+12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
		{
			targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, true);
            didIHit = true;
			Debug.Log(this.name + " hit " + targetedCreature.name + " leaving it with " + targetedCreature.hitPoints + " HP");
		}
		else//Miss if you roll lower than the target's Fortitude
		{
			Debug.Log(this.name + " missed " + targetedCreature.name);
            targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, false);
		}

		//Done attacking now
        mNumberOfAttacksLeft--;
		UnHighlightSquares();
		if (mNumberOfAttacksLeft <= 0)
        {
            canAttack = false;
        }

        if (mCombatCards.Count > 0)
        {
            foreach (Card card in mCombatCards)
            {
                card.AfterBattleEffect();
            }
        }

		//if you took the target down to 0 HP, kill it and move into its space if it was adjacent
		if (targetedCreature.hitPoints == 0)
		{
            if (Vector3.Distance(this.transform.position, targetedCreature.transform.position) == 1)
            {
                //transform.position = targetedCreature.transform.position; 
				//Victory Rush into the target's vacated space
				mPathSquares.Clear();
				mPathSquares.Add(targetedCreature.currentSpace.gameObject);
				MoveToSpace();
                currentSpace.GetComponent<BoardSquare>().occupier = null;
                currentSpace = targetedCreature.currentSpace;
                foreach(Creature combatRival in mCombatLockTargets)
                {
                    combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, GetComponent<NetworkView>().viewID);
                }

                GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
            }
			targetedCreature.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
			Debug.Log(this.name + " killed " + targetedCreature.name);
		}
		//clickManager.selectedCreature = null;
        return didIHit;
	}//End of AttackTarget()

	[RPC] public virtual void TakeDamage(NetworkViewID whoHitMe, bool didHit = true)
	{
		if (whoHitMe != null && Vector3.Distance(this.transform.position, NetworkView.Find(whoHitMe).observed.transform.position) == 1)
        {
            List<Rune> attackedRunes;
            if (mActiveRunes.TryGetValue(Rune.RuneType.IsAttacked, out attackedRunes))
            {
                foreach (Rune rune in attackedRunes)
                {
					Debug.Log("DOING THE IS ATTACKED RUNE " + rune.name);
					rune.ActivateRune(NetworkView.Find(whoHitMe).observed.GetComponent<Creature>());
                }
            }
            lockedInCombat = true;
            this.mCombatLockTargets.Add(NetworkView.Find(whoHitMe).observed.GetComponent<Creature>());
            NetworkView.Find(whoHitMe).observed.GetComponent<Creature>().lockedInCombat = true;
            NetworkView.Find(whoHitMe).observed.GetComponent<Creature>().mCombatLockTargets.Add(this);
        }
        if (didHit)
        {
            List<Rune> damagedRunes;
            if (mActiveRunes.TryGetValue(Rune.RuneType.IsDamaged, out damagedRunes))
            {
                foreach (Rune rune in damagedRunes)
                {
					Debug.Log("DOING THE DAMAGED RUNE " + rune.name);
                    rune.ActivateRune(this);
                }
            }
            hitPoints--;
        }
	}

	//Function for removing a combat lock rival from this creature's list
    [RPC] public void RemoveFromCombatLock(NetworkViewID combatRival)
    {
		if (NetworkView.Find(combatRival) != null && NetworkView.Find(combatRival).observed.GetComponent<Creature>()!=null)
		{
	        Creature removedRival = NetworkView.Find(combatRival).observed.GetComponent<Creature>();
	        while (mCombatLockTargets.Contains(removedRival))
	        {
	            mCombatLockTargets.Remove(removedRival);
	        }
	        if (mCombatLockTargets.Count <= 0)
	        {
	            lockedInCombat = false;
				if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
				{
					canMove = true;
				}

	        }
		}
    }//END of RemoveFromCobatLock()

	//Function for leaving combat lock altogether, and removing itself from its rivals combat lock lists
    [RPC] public void BreakFromCombatLock()
    {
		bool wasInCombat = lockedInCombat;
		Debug.Log("Breaking from Combat");
		foreach (Creature combatRival in mCombatLockTargets)
		{
			Debug.Log("Removing myself from rival's combat lock");
			combatRival.RemoveFromCombatLock(this.GetComponent<NetworkView>().viewID);
		}

        mCombatLockTargets.Clear();
        lockedInCombat = false;
		if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement && wasInCombat)
		{
			canMove = true;
		}
		Debug.Log("Done breaking from combat");
    }//END of BreakFromCombatLock()

	//Function for getting killed by being reduced to 0 Hitpoints
	[RPC] public virtual void BeKilled()
	{
		if (tag == "Familiar" || tag == "Minion"  && GetComponent<NetworkView>())
		{
            lockedInCombat = false;
			handManager.deck[handCardNumber].cardState = Card.CardState.Cooldown;
		}
        foreach (Creature combatRival in mCombatLockTargets)
        {
            combatRival.RemoveFromCombatLock(this.GetComponent<NetworkView>().viewID);
        }
        foreach (List<Rune> runesList in mActiveRunes.Values)
        {
            for (int i = 0; i < runesList.Count; i++)
            {
                runesList[i].DestroyRune();
            }
        }
        if (tag == "VoidBreacher")
        {
            if (GetComponent<NetworkView>().isMine)
            {
                clickManager.mRemainingMonuments = 0;
            }
            clickManager.GetComponent<NetworkView>().RPC("RevealWinner", RPCMode.All, controllingPlayer);
        }
		//Make it so the game doesn't freeze up if the creature dies while moving (like, from a trap)
		if(mIsMoving)
		{
			handManager.mAnimationPlaying = false;
			clickManager.mAnimationPlaying = false;
		}
        Destroy(this.gameObject);
	}//END of BeKilled()


	//Function for forcing a creature to move (for things like Lava Chain) ~Adam
	[RPC] public void ForceMovement(string targetSpaceName)
	{
		mPathSquares.Clear();
		mPathSquares.Add(GameObject.Find(targetSpaceName));
		MoveToSpace();

	}

	public void UnHighlightSquares ()
	{
		BoardSquare[] squaresToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare unHighlightTarget in squaresToUnHighlight)
		{
			unHighlightTarget.inMovementRange = false;
			unHighlightTarget.hasAttackableTarget = false;
			unHighlightTarget.validSummonSpot = false;
		}
		movementSquaresAreHighlighted = false;
		combatSquaresAreHighlighted = false;
	}//End of UnHighlightSquares()

	//Functions for beginning each phase

	public virtual void BeginPreparationPhase()
	{
		canMove = false;
        currentSpace.GetComponent<BoardSquare>().occupier = this.gameObject;
	}//End of BeginPreparationPhase
	public virtual void BeginSummoningPhase()
	{
        
	}//End of BeginSummoningPhase
	public virtual void BeginMovementPhase()
	{
		if (!lockedInCombat || mElusive)
		{
            if (mTurnsStunned <= 0)
            {
                canMove = true;
            }
		}
	}//End of BeginMovementPhase

	public virtual void BeginCombatPhase()
	{
		//Debug.Log(handManager.name);
		canMove = false;
		if (movementSquaresAreHighlighted)
		{
			UnHighlightSquares();
		}
		if (tag != "VoidBreacher" && mTurnsStunned <= 0)
		{
			canAttack = true;
		}

		//Refund any Familiars/Minions still in limbo (being carried) that didn't get summoned
		if (limboCreatureHandCardNumber.Count > 0)
		{
			for (int i = 0; i < limboCreatureHandCardNumber.Count; i++)
			{
				Debug.Log("Refunding unsummoned creature");
				handManager.deck[limboCreatureHandCardNumber[i]].cardState = Card.CardState.Hand;
				clickManager.sourceStoneCount += handManager.deck[limboCreatureHandCardNumber[i]].sourceStoneCost;
			}

			carriedLimboCreatures.Clear();
			limboCreatureHandCardNumber.Clear();
		}
	}//End of BeginCombatPhase

	public virtual void BeginConclusionPhase()
	{
        List<Rune> conclusionRunes;
        if (mActiveRunes.TryGetValue(Rune.RuneType.Conclusion, out conclusionRunes))
        {
            foreach (Rune rune in conclusionRunes)
            {
				Debug.Log("DOING THE CONCLUSION RUNE " + rune.name);

                rune.ActivateRune(this);
            }
        }
		//Clean up anything leftover from the combat phase
		if (combatSquaresAreHighlighted)
		{
			UnHighlightSquares();
		}
		canAttack = false;

		//Get rid of temporary status effects
        if (mTemporarilyWeakened)
        {
            mTemporarilyWeakened = false;
        }
        else
        {
            crippled = 0;
            int temporaryBoost = 0;
            foreach (string weakeningBoost in mWeakeningBoosts)
            {
                if (mTempBoosts.TryGetValue(weakeningBoost, out temporaryBoost))
                {
                    UpdateFortitude(-temporaryBoost);
                    GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
                    mTempBoosts.Remove(weakeningBoost);
                }
            }
            mWeakeningBoosts.Clear();
        }
		//Reset the remaing attacks this creature can do
        mNumberOfAttacksLeft = mMaxAttacksATurn;

		//Unstun the creature
        if (mTurnsStunned > 0)
        {
            mTurnsStunned--;
            if (mTurnsStunned <= 0)
            {
                mTurnsStunned = 0;
                int temporaryBoost = 0;
                if (mTempBoosts.TryGetValue("Freezeboost", out temporaryBoost))
                {
                    UpdateFortitude(-temporaryBoost);
                    mTempBoosts.Remove("Freezeboost");
                    GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
                }
            }
        }
	}//End of BeginConclusionPhase


	//Functions related to summoning creatures from this space
	public void summonToLimbo (GameObject limboSummon, int limboCardNumber) //add to the list of creature sharing this space in Limbo
	{
		carriedLimboCreatures.Add(limboSummon);
		limboCreatureHandCardNumber.Add(limboCardNumber);
	}//End of summonToLimbo()

	public void summonFromLimbo()//Move a creature out of Limbo and onto the board;
	{
		List<Rune> summonRunes;
		if (mActiveRunes.TryGetValue(Rune.RuneType.Summoning, out summonRunes))
		{
			foreach (Rune rune in summonRunes)
			{
				Debug.Log("DOING THE SUMMONING RUNE " + rune.name);

				rune.ActivateRune(this);
			}
		}
		carriedLimboCreatures.RemoveAt(limboCreatureLookingAt);
		limboCreatureHandCardNumber.RemoveAt(limboCreatureLookingAt);
		UnHighlightSquares();
	}//End of summonFromLimbo()

	[RPC] public void SummonPlayerInfo(int summonerPlayerNumber, int summonerCardNumber, string properName, string description)
	{
		controllingPlayer = summonerPlayerNumber;
		handCardNumber = summonerCardNumber;
		name = properName;
		mCreatureDescription = description;
	}//End of SummonPlayerInfo()

	[RPC]
	public void AttackMonument()
	{
		Debug.Log(name + " Attacked a monument");
		mAttackableMonument.mAttacksLeftToWithstand--;
		mNumberOfAttacksLeft--;
		if(mNumberOfAttacksLeft <= 0)
		{
			canAttack = false;
		}
		if (mAttackableMonument.mAttacksLeftToWithstand <= 0)
		{
			mCanAttackMonument = false;
			mAttackableMonument = null;
		}
		UnHighlightSquares();
	}//End of AttackMonument ()

    [RPC] public void ModifyFortitude(int amount)
    {
        fortitude = fortitude + amount;
    }//End of ModifyFortitude()

    [RPC] public void ModifyStrength(int amount)
    {
        strength = strength + amount;
    }//End of ModifyFortitude()

	[RPC] public void GetCrippled()
	{
		crippled++;
	}//End of GetCrippled

    [RPC] public void SendStrengthandFortitude(int newStrength, int newFortitude)
    {
        strength = newStrength;
        fortitude = newFortitude;
    }//End of SendStrengthandFortitude

	//For making it so that the rune count will be right on both side for updating status icons,
	//but not double up on rune effects, which would happen if we actually applied runes on both sides ~Adam
	[RPC] public void AddRuneCount(int runeAmount)
	{
		Debug.Log("Updated Rune Count by " + runeAmount);
		mNumberOfRunes += runeAmount;
	}//END of AddRuneCount()
	[RPC] public void AddRuneCardImage(string runeCardName)
	{
		mRuneCardImages.Add(GameObject.Find(runeCardName).GetComponent<Card>().mCardArt);
	}//END of AddRuneCardImage()
    public void UpdateStrength(int amount)
    {
        strength = strength + amount;
    }//End of UpdateStrength

    public void UpdateFortitude(int amount)
    {
		fortitude = fortitude + amount;
    }//End of UpdateFortitude

    [RPC] public void ReduceHealth(int amount)
    {
        hitPoints = hitPoints - amount;
        if (hitPoints <= 0)
        {
            BeKilled();
        }
    }//End of ReduceHealth

    [RPC] public void UpdatePosition(Vector3 Position)
    {
        transform.position = Position;
    }//End of UpdatePosition

    [RPC] public void BeFrozen(string freeze, int amount, int timer)
    {
        if (!mTempBoosts.ContainsKey(freeze) && mTurnsStunned <= 0 && GetComponent<NetworkView>().isMine)
        {
            UpdateFortitude(amount);
            mTempBoosts.Add(freeze, amount);
			canMove = false;
            GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
            mTurnsStunned = timer;
        }
    }//End of BeFrozen

    [RPC] public void BeWeakened(string weakener, int amount, int timer)
    {
        if (!mTempBoosts.ContainsKey(weakener) && GetComponent<NetworkView>().isMine)
        {
            mTemporarilyWeakened = true;
            UpdateFortitude(amount);
            mTempBoosts.Add(weakener, amount);
            mWeakeningBoosts.Add(weakener);
            GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, strength, fortitude);
            crippled = timer;
        }
    }//End of BeWeakened

	public void MoveToSpace()
	{
		if(mElusive)
		{
			foreach(Creature combatRival in mCombatLockTargets)
			{
				combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, GetComponent<NetworkView>().viewID);
			}
			GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
		}
		//mIsMoving = true;
		GetComponent<NetworkView>().RPC("ToggleIsMoving", RPCMode.All, true);

		handManager.mAnimationPlaying = true;
		clickManager.mAnimationPlaying = true;
	}//End of MoveToSpace()

	[RPC] public void ToggleIsMoving(bool moveSetting)
	{
		mIsMoving = moveSetting;
	}

	//Called when a creature reaches the end of its movement
	public void LerpPathArrived()
	{
		transform.position = mPathSquares[0].transform.position+mOffsetFromSquare;
		mPathSquares[0].GetComponent<BoardSquare>().mDistanceFromMover = 99;
		//mIsMoving = false;
		GetComponent<NetworkView>().RPC("ToggleIsMoving", RPCMode.All, false);
		handManager.mAnimationPlaying = false;
		clickManager.mAnimationPlaying = false;
	}//End of LerpPathArrived()
    
}
