﻿using UnityEngine;
using System.Collections;

public class PonderousDroplet :Creature
{


	// Use this for initialization
	public override void Start()
	{
		base.Start();
		
		if (sourceStoneCost == 0)
		{
			sourceStoneCost = 0;
		}
		
		if (strength == 0)
		{
			strength = 0;
		}
		
		if (fortitude == 0)
		{
			fortitude = 6;
		}
		
		creatureDominion = CreatureDominion.Irotar;
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		if (crippled == 0)
		{
			crippled = 1;
		}
		base.Update();
	}

	public override void BeginConclusionPhase()
	{
		if(turnManager.playerTurn == controllingPlayer)
		{
			GetComponent<NetworkView>().RPC("ModifyStrength", RPCMode.All, 1); 
			GetComponent<NetworkView>().RPC("ModifyFortitude", RPCMode.All, 1); 
		}
	}

}
