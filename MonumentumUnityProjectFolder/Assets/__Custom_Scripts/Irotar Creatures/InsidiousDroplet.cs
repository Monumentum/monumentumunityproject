﻿using UnityEngine;
using System.Collections;

public class InsidiousDroplet : Creature
{
	
	
	// Use this for initialization
	public override void Start()
	{
		base.Start();
		
		if (sourceStoneCost == 0)
		{
			sourceStoneCost = 4;
		}
		
		if (strength == 0)
		{
			strength = 4;
		}
		
		if (fortitude == 0)
		{
			fortitude = 8;
		}
		
		creatureDominion = CreatureDominion.Irotar;
	}



	//Function for attacking a target
	public override bool AttackTarget(Creature targetedCreature, int numberOfRolls = 1) //Overrides to get stronger with each attack
	{
		//see if the target has already been damaged, and increase Strength vs. already damaged targets
		bool attackingWeakenedTarget = false; 
		if (targetedCreature.hitPoints < targetedCreature.startingHitPoints)
		{
			attackingWeakenedTarget = true;
			GetComponent<NetworkView>().RPC("ModifyStrength", RPCMode.All, 2); 
		}

		bool didIHit = base.AttackTarget(targetedCreature);//do the attack

		//reset Strength
		if (attackingWeakenedTarget)
		{
			GetComponent<NetworkView>().RPC("ModifyStrength", RPCMode.All, -2); 
		}

		return didIHit;
	}//End of AttackTarget()
}
