﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WailingandMiserySiren : Creature
{
	
	public int mBoostAmount = 1;
	public int warCryRange = 3;
	public List<Creature> warCryTargets = new List<Creature>();

	// Use this for initialization
	public override void Start()
	{
		base.Start();
		
		if (sourceStoneCost == 0)
		{
			sourceStoneCost = 6;
		}
		
		if (strength == 0)
		{
			strength = 6;
		}
		
		if (fortitude == 0)
		{
			fortitude = 10;
		}
		
		creatureDominion = CreatureDominion.Irotar;
	}
	


	public override void BeginCombatPhase()
	{
		//Creature[] creatures = FindObjectsOfType<Creature>();
		string boostName = creatureName + "WarCryBoost";

		foreach (Creature creature in warCryTargets)
		{
			if ((creature != null && creature.controllingPlayer == this.controllingPlayer) && (creature.tag == "Minion") && (creature.mBoosts.Contains(boostName))&& GetComponent<NetworkView>().isMine)
			{
				creature.ModifyStrength(-mBoostAmount);
				creature.mBoosts.Remove(boostName);
			}
		}

		warCryTargets.Clear();
		FindWarCryTargets();
		foreach (Creature creature in warCryTargets)
		{
			if (creature != null && creature.controllingPlayer == this.controllingPlayer && GetComponent<NetworkView>().isMine)
			{
				if (!creature.mBoosts.Contains(boostName))
				{
					//creature.networkView.RPC("ModfiyStrength", RPCMode.All, mBoostAmount);
                    creature.UpdateStrength(mBoostAmount);
                    creature.mBoosts.Add(boostName);
                    creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
				}
			}
		}
		
		base.BeginCombatPhase();
	}
	
	[RPC] public override void BeKilled()
	{
		//Creature[] creatures = FindObjectsOfType<Creature>();
		string boostName = creatureName + "WarCryBoost";
		
		foreach (Creature creature in warCryTargets)
		{
			if ((creature != null && creature.controllingPlayer == this.controllingPlayer) && (creature.tag == "Minion") 
			    && creature!= this && (creature.mBoosts.Contains(boostName)) && GetComponent<NetworkView>().isMine)
			{
				Debug.Log(creature.name);
                if (GetComponent<NetworkView>().isMine)
                {
                    creature.UpdateStrength(-mBoostAmount);
                    creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
                }
                creature.mBoosts.Remove(boostName);
			}
		}
		base.BeKilled();
	}

	public void FindWarCryTargets()
	{
		Debug.Log("I'm selected an ready to move");
		for (int i = 1; i <= warCryRange; i++)
		{
			//highlight adjacent squares
			if (i == 1)
			{
				//Debug.Log("Highlighting the first set of squares");
				foreach(BoardSquare potentialWarCryTarget in currentSpace.GetComponent<BoardSquare>().neighborSquares)
				{
					potentialWarCryTarget.hasAttackableTarget = true;
				}
			}
			//Highlight other squares in range
			else
			{
				//Debug.Log("Highlighting another set of squares");
				BoardSquare[] highlightedWarCryTargets = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				List<BoardSquare> highlightedSquareList = new List<BoardSquare>();//Make a list to contain squares
				foreach (BoardSquare newWarCryTarget in highlightedWarCryTargets)//Add any already highlighted squares to the list
				{
					if (newWarCryTarget.hasAttackableTarget == true)
					{
						highlightedSquareList.Add(newWarCryTarget);
					}
				}
				foreach(BoardSquare listedSqaure in highlightedSquareList)//Use the list to highlight suqares adjacent to the ones already highlighted
				{
					foreach(BoardSquare potentialWarCryTarget in listedSqaure.neighborSquares)
					{
						if (Vector3.Distance(potentialWarCryTarget.gameObject.transform.position, currentSpace.gameObject.transform.position) == i)
						{
							potentialWarCryTarget.hasAttackableTarget = true;
						}
		
					}
				}
					
			}
			//add the valid occupiers of highlighted squares to the warCryTargets 
			if (i == warCryRange)
			{
				BoardSquare[] warCryTargetToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];//Find all the squares on the board
				foreach (BoardSquare warCryTargetSquare in warCryTargetToUnHighlight)//Unhighlight any squares that are Highlighted
				{
					if (warCryTargetSquare.occupier == null)
					{
						warCryTargetSquare.hasAttackableTarget = false;
					}
					else if (warCryTargetSquare.occupier != null)
					{
						if (warCryTargetSquare.occupier.tag == "Minion" && warCryTargetSquare.hasAttackableTarget && warCryTargetSquare.occupier.GetComponent<Creature>().controllingPlayer == this.controllingPlayer)
						{
						warCryTargets.Add(warCryTargetSquare.occupier.GetComponent<Creature>());
						}
						warCryTargetSquare.hasAttackableTarget = false;

					}
				}
					
			}
		}
	} //End of FindBoostTargets()

}
