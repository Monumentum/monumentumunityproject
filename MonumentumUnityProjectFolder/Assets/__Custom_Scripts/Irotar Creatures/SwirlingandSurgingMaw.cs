﻿using UnityEngine;
using System.Collections;

public class SwirlingandSurgingMaw : Creature
{

    public int mBoostAmount = 1;
    // Use this for initialization
    public override void Start()
    {
        base.Start();

        if (sourceStoneCost == 0)
        {
            sourceStoneCost = 4;
        }

        if (strength == 0)
        {
            strength = 4;
        }

        if (fortitude == 0)
        {
            fortitude = 10;
        }

        creatureDominion = CreatureDominion.Irotar;
    }


    public override void BeginCombatPhase()
    {
        Creature[] creatures = FindObjectsOfType<Creature>();
        string boostName = creatureName + "boost";

        foreach (Creature creature in creatures)
        {
			if ((creature.controllingPlayer == this.controllingPlayer) && creature.character && GetComponent<NetworkView>().isMine)
            {
                if (!creature.mBoosts.Contains(boostName))
                {
                    //creature.networkView.RPC("ModifyFortitude", RPCMode.All, mBoostAmount);
                    creature.UpdateFortitude(mBoostAmount);
                    creature.mBoosts.Add(boostName);
                    creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
                }
            }
        }

        base.BeginCombatPhase();
    }

    [RPC]
    public override void BeKilled()
    {
        Creature[] creatures = FindObjectsOfType<Creature>();
        string boostName = creatureName + "boost";

        foreach (Creature creature in creatures)
        {
			if ((creature.controllingPlayer == this.controllingPlayer) && creature.character && GetComponent<NetworkView>().isMine && creature != this)
            {
                if (creature.mBoosts.Contains(boostName))
                {
                    if (GetComponent<NetworkView>().isMine)
                    {
                        creature.UpdateFortitude(-mBoostAmount);
                        creature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, creature.strength, creature.fortitude);
                    }
                    creature.mBoosts.Remove(boostName);
                }
            }
        }
        base.BeKilled();
    }
}
