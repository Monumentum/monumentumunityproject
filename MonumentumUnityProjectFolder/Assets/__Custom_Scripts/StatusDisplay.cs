﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Script for showing status and health icons above a creature's model during play
//It is attached as a component of the various creature prefabs

public class StatusDisplay : MonoBehaviour 
{
	//The creature this is showing the display for
	[SerializeField] private Creature mCreature;
	//The creature's default strength/fortitude so we can show whether its current strength/fortitude is higher or lower
	//[HideInInspector] 
	public int mBaseStrength;
	//[HideInInspector] 
	public int mBaseFortitude;


	//An array of all the status icons we're using
	[SerializeField] private Sprite[] mStatusMarkers;
	/*	
		Markers should have the following indices:
		0: Health
		1: Strength Up
		2: Strength Down
		3: Fortitude Up
		4: Fortitude Down
		5: Crippled
		6: Immobilized
		7: Rune Applied
		8: Locked in Combat is Active
	*/

	//A list of the currently applied status effects
	[SerializeField] private List<Sprite> mCurrentStatusEffects = new List<Sprite>();


	// Use this for initialization
	void Start () 
	{
		mCreature = GetComponent<Creature>();
		//Set the base stats
		mBaseStrength = mCreature.strength;
		mBaseFortitude = mCreature.fortitude;

	}//END of Start()
	
	// Update is called once per frame
	void Update () 
	{
		//Reset the current status effects
		mCurrentStatusEffects.Clear();
		//Re-apply current staus effects
		
		//Check difference between Strength and BaseStrength
		if(mCreature.strength > mBaseStrength)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[1]);
		}
		else if(mCreature.strength < mBaseStrength)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[2]);
		}
		
		//Check difference between Fortitude and BaseFortitude
		if(mCreature.fortitude > mBaseFortitude)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[3]);
		}
		else if(mCreature.fortitude < mBaseFortitude)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[4]);
		}
		
		//Check if Crippled/Immobilized
		if(mCreature.crippled == 1)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[5]);
		}
		else if(mCreature.crippled == 2)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[6]);
		}
		
		//Check if the creature has any Runes
		if(mCreature.mNumberOfRunes > 0)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[7]);
		}

		//Check if the creature is locked in combat
		if (mCreature.mCombatLockTargets.Count > 0)
		{
			mCurrentStatusEffects.Add(mStatusMarkers[8]);
		}
		
	}//END of Update()

	void LateUpdate()
	{
	}//END of LateUpdate()


	void OnGUI()
	{
		//Find where the creature is on screen so we can use that as the origin for our GUI positioning
		Vector3 creatureScreenPos = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);

		creatureScreenPos = new Vector3(creatureScreenPos.x, Screen.height-creatureScreenPos.y, creatureScreenPos.z);
		//Offsets for GUI elements
		float healthXOffset = Screen.width*(-0.01f*(mCurrentStatusEffects.Count+mCreature.hitPoints)/2f);
		float healthYOffset = Screen.height*-0.055f;

//		if(mCurrentStatusEffects.Count<=0)
//		{
//			healthYOffset = Screen.height*-0.043f;
//		}
		float statusXOffset;// = Screen.width*(-0.03f*mCurrentStatusEffects.Count/2f);
		float statusYOffset = Screen.height*-0.055f;
		//For determining the width of the box to put behind all this
		float backgroundBoxWidth =  mCreature.hitPoints+mCurrentStatusEffects.Count;
		//Put down a box as a background for the icons
		GUI.Box(new Rect(creatureScreenPos.x+healthXOffset, creatureScreenPos.y+healthYOffset, Screen.height*0.015f*backgroundBoxWidth, Screen.height*0.015f),"");
		//Display Health icons
		for(int i = 1; i <= mCreature.hitPoints; i++)
		{
			GUI.DrawTexture(new Rect(creatureScreenPos.x+healthXOffset, creatureScreenPos.y+healthYOffset, Screen.height*0.015f, Screen.height*0.015f), mStatusMarkers[0].texture);
			//GUI.Box(new Rect(creatureScreenPos.x+healthXOffset, creatureScreenPos.y+healthYOffset, Screen.width*0.1f, Screen.height*0.1f), mCreature.name+","+creatureScreenPos);

			healthXOffset+=Screen.height*0.015f;
		}
		statusXOffset = healthXOffset;
		//Display status effects
		if(mCurrentStatusEffects.Count > 0)
		{
			for (int k = 0; k< mCurrentStatusEffects.Count; k++)
			{
				GUI.DrawTexture(new Rect(creatureScreenPos.x+statusXOffset, creatureScreenPos.y+statusYOffset, Screen.height*0.015f, Screen.height*0.015f), mCurrentStatusEffects[k].texture);
				//GUI.Box(new Rect(creatureScreenPos.x+statusXOffset, creatureScreenPos.y+statusYOffset, Screen.width*0.1f, Screen.height*0.1f), mCreature.name);

				statusXOffset+=Screen.height*0.015f;

			}
		}

	}//END of OnGUI
}
