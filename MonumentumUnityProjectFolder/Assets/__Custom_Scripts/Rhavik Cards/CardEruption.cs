﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardEruption : Card 
{
	Creature mVoidBreacher; 
	// Use this for initialization
	void Start () 
	{
        cardName = "Eruption";
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		DoAOEAttack(3,4);

		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;	
	}//END of CardEffect()



	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}


}
