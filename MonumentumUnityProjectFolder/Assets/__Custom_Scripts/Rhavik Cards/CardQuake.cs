﻿using UnityEngine;
using System.Collections;

//Target friendly creature causes another creature within LOS(3) to suffer 'crippled'.

public class CardQuake : Card 
{
	public int mTargetRange = 3;
	public Creature mCrippledCreature;

	// Use this for initialization
	void Start () 
	{
        cardName = "Quake";
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		mTargetedCreature = handClickController.selectedCreature;
		TargetAllCharactersInLOS(mTargetRange);        
		handManager.mCurrentCard = this;
		//handManager.mTarget = mTargetedCreature;
		handManager.mTargetting = true;
		handManager.playingActionCard = false;

//		foreach(BoardSquare aoeSquare in handClickController.selectedCreature.currentSpace.GetComponent<BoardSquare>().neighborSquares)
//		{
//			if (aoeSquare.occupier != null && aoeSquare.occupier.GetComponent<Creature>() != null
//			    && aoeSquare.occupier.tag != "VoidBreacher" && aoeSquare.occupier.GetComponent<Creature>().controllingPlayer != playerNumber)
//			{
//				aoeSquare.occupier.GetComponent<Creature>().GetComponent<NetworkView>().RPC("GetCrippled", RPCMode.All);
//			}
//		}
//
//		//finish up the action and send this card to the Cooldown pile
//		handManager.playingActionCard = false;
//		handManager.mTargetting = false;
//		handManager.actionCardToPlay = null;
//		cardState = CardState.Cooldown;

	}


	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}


	public override void TargetAcquired()
	{
		mCrippledCreature = handClickController.mLastSelectedCreature;
		handManager.mTargetting = false;

		//Cripple the target
		Debug.Log("Doing effect of " + cardName);
		mCrippledCreature.GetComponent<NetworkView>().RPC("GetCrippled", RPCMode.All);

		//finish up the action and send this card to the Cooldown pile
		handManager.mActivatingActionCard = false;
		handManager.actionCardToPlay = null;
		cardState = CardState.Cooldown;
	}

}
