﻿using UnityEngine;
using System.Collections;

//Target friendly character gains an additional attack this turn 
//but during your next conclusion phase is damaged.
public class Berserk : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mRecoilDamage = 1;

	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "Berserk";
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if(mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        mTargetedCreature = handClickController.selectedCreature;
        mTargetedCreature.mNumberOfAttacksLeft++;
		mTargetedCreature.canAttack = true;

        handManager.playingActionCard = false;
        handManager.actionCardToPlay = null;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public void OnConclusionPhase()
    {
        mTargetedCreature.GetComponent<NetworkView>().RPC("ReduceHealth", RPCMode.All, mRecoilDamage);
        cardState = CardState.Cooldown;
    }

    
}
