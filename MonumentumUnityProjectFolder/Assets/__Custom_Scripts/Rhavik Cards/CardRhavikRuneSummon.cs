﻿using UnityEngine;
using System.Collections;

public class CardRhavikRuneSummon : Card 
{
	Creature myVoidBreacher;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


	public override void CardEffect()
	{
		Debug.Log("Doing effect of " + cardName);
		SummonRune();

		//For applying Flint's Rune Boost
		Creature[] creatures = FindObjectsOfType<Creature>();
		foreach (Creature creature in creatures)
		{
			if ((creature.controllingPlayer == playerNumber) && creature.tag == "VoidBreacher")
			{
				myVoidBreacher = creature;
			}
		}
		if (myVoidBreacher.GetComponent<FlintDross>() != null)
		{
			myVoidBreacher.GetComponent<FlintDross>().ApplyFlintRuneBoost();
		}

	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}
}
