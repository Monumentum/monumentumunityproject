﻿using UnityEngine;
using System.Collections;

//Target friendly character. Target may pull another 
//character within LOS(3) up to, 2 squares towards the target.
public class LavaChain : Card 
{
    public int mTargetRange = 3;
    public int mMovementRange = 2;
    public Creature mMovingCreature;
	// Use this for initialization
	void Start () 
    {
        cardName = "LavaChain";
	}
	
	// Update is called once per frame
	void Update () 
    {
		if(cardState != CardState.Table)
		{
			mMovingCreature = null;
			mTargetedCreature = null;
		}
	}

    public override void CardEffect()
    {
        mTargetedCreature = handClickController.selectedCreature;
        TargetAllCharactersInLOS(mTargetRange);        
        handManager.mCurrentCard = this;
        //handManager.mTarget = mTargetedCreature;
        handManager.mTargetting = true;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public override void TargetAcquired()
    {
        mMovingCreature = handClickController.mLastSelectedCreature;
		HighlightSquaresBetweenCreatures(mTargetedCreature, mMovingCreature, mMovementRange);
        handManager.mActivatingActionCard = true;
        handManager.mTargetting = false;
    }

    public override void ActivateOnTarget()
    {
        //mMovingCreature.transform.position = handClickController.mClickedSquare.transform.position + new Vector3(0, 0.5f, 0);
       // mMovingCreature.networkView.RPC("UpdatePosition", RPCMode.All, mMovingCreature.transform.position);
		mMovingCreature.GetComponent<NetworkView>().RPC("ForceMovement", RPCMode.All, handClickController.mClickedSquare.gameObject.name);
		mMovingCreature.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All, true);
		Debug.Log("Pulled the target");
        handManager.mActivatingActionCard = false;
        handManager.actionCardToPlay = null;
        cardState = CardState.Cooldown;
    }
}
