﻿using UnityEngine;
using System.Collections;

public class ChangeSceneButton : MonoBehaviour 
{
	[SerializeField] private string mSceneName;
	[SerializeField] private string mButtonText;
	[SerializeField] bool mMakeGUIButton = true;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnGUI()
	{
		if(mMakeGUIButton)
		{
			if (GUI.Button(new Rect(Screen.width*0.01f, Screen.height*0.875f, Screen.width*0.2f, Screen.height*0.1f), mButtonText))
			{
				if(Application.loadedLevelName == "Matchup")
				{
					Destroy(FindObjectOfType<DeckBuilder>().gameObject);
					FindObjectOfType<Assets.CustomScripts.NetworkManager>().ReturnToLobby();
				}
				else
				{
					Application.LoadLevel(mSceneName);
				}
			}
		}
	}

	public void GoToLevel()
	{
		if(Application.loadedLevelName == "Matchup")
		{
			Destroy(FindObjectOfType<DeckBuilder>().gameObject);
			FindObjectOfType<Assets.CustomScripts.NetworkManager>().ReturnToLobby();
		}
		else
		{
			Application.LoadLevel(mSceneName);
		}
	}
}
