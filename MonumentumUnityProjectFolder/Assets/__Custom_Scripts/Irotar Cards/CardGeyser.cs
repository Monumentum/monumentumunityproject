﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Target friendly character, all enemy characters within AOE(3) of target suffer a 6S attack. Target is destroyed during your next conclusion phase.

public class CardGeyser : Card 
{
	Creature mVoidBreacher; 
	public TurnPhaseManager mTurnPhaseManager;

	// Use this for initialization
	void Start () 
	{
		mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mTurnPhaseManager && cardState == CardState.Table)
		{
			if(mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
			{
				OnConclusionPhase();
			}
		}

	}


	public override void CardEffect()
	{
		//For keeping track of the creature to kill it on the conclusion phase
		mTargetedCreature = handClickController.selectedCreature;



		DoAOEAttack(3,6);
		
		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
	}
	
	
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}


	public void OnConclusionPhase()
	{
		mTargetedCreature.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All);
		cardState = CardState.Cooldown;
	}

}
