﻿using UnityEngine;
using System.Collections;


//All friendly creatures 'break combat'.
public class Dissipate : Card 
{
	public TurnPhaseManager mTurnPhaseManager;

	// Use this for initialization
	void Start () 
	{
		cardName = "Dissipate";
		mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		Creature[] creatures = FindObjectsOfType<Creature>();
		string boostName = cardName + "boost";
		
		foreach (Creature creature in creatures)
		{
			if ((creature.controllingPlayer == this.playerNumber) && creature.character)
			{
				if(creature.lockedInCombat == true && mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
				{
					creature.canMove = true;
				}
				creature.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All, true);
			}
		}
		
		
		
		//finish up the action and send this card to the Cooldown pile
		cardState = CardState.Cooldown;
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		CardEffect();
	}
}
