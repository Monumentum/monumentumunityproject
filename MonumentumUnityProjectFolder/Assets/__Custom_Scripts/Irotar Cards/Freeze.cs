﻿using UnityEngine;
using System.Collections;

//Target a character within LOS(3) of a friendly character, target gains +4F 
//but cannot move and cannot attack until the end of your next turn.
public class Freeze : Card 
{
    public int mBoostAmount = 4;
    public int mTurnsCreatureWillBeStunned = 1;
    public int mTargetRange = 3;
    public Creature mFrozenCreature;
	// Use this for initialization
	void Start () 
    {
        cardName = "Freeze";
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public override void CardEffect()
    {
        mTargetedCreature = handClickController.selectedCreature;
        TargetAllCharactersInLOS(mTargetRange);
		mTargetedCreature.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget = true;
        handManager.mCurrentCard = this;
        //handManager.mTarget = mTargetedCreature;
        handManager.mTargetting = true;
        handManager.playingActionCard = false;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public override void TargetAcquired()
    {
        string boostName = cardName + "boost";
        mFrozenCreature = handClickController.mLastSelectedCreature;
        mFrozenCreature.GetComponent<NetworkView>().RPC("BeFrozen", RPCMode.All, boostName, mBoostAmount, mTurnsCreatureWillBeStunned);
        handManager.mTargetting = false;
        handManager.actionCardToPlay = null;
        cardState = CardState.Cooldown;
    }
}
