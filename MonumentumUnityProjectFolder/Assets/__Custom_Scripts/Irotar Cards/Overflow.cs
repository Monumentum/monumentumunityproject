﻿using UnityEngine;
using System.Collections;

//Target friendly character gains +4S but is destroyed during your next conclusion phase.

public class Overflow : Card 
{
    public TurnPhaseManager mTurnPhaseManager;
    public int mBoostAmount = 4;
	Creature mVoidBreacher; 

	// Use this for initialization
	void Start () 
    {
        mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
        cardName = "Overflow";
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (mTurnPhaseManager && cardState == CardState.Table)
        {
            if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
            {
                OnConclusionPhase();
            }
        }
	}

    public override void CardEffect()
    {
        mTargetedCreature = handClickController.selectedCreature;
        string boostName = cardName + "boost";

        if ((mTargetedCreature.controllingPlayer == this.playerNumber) && mTargetedCreature.character)
        {
            mTargetedCreature.UpdateStrength(mBoostAmount);
            mTargetedCreature.mBoosts.Add(boostName);
            mTargetedCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, mTargetedCreature.strength, mTargetedCreature.fortitude);
        }

        handManager.playingActionCard = false;
        handManager.actionCardToPlay = null;
    }

    public override void CardTarget()
    {
        Debug.Log("Playing " + cardName);
        cardState = CardState.Table;
        TargetAllFriendlyCharacters();
    }

    public void OnConclusionPhase()
    {
       // mTargetedCreature.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All);
		//Find your voidbreacher to use as the source of the damage caused by this card
		Creature[] voidBreachersInPlay = FindObjectsOfType(typeof(Creature)) as Creature[];
		foreach (Creature voidBreacher in voidBreachersInPlay)
		{
			if (voidBreacher.tag == "VoidBreacher" && voidBreacher.controllingPlayer == playerNumber)
			{
				mVoidBreacher= voidBreacher;
			}
		}
		if(mTargetedCreature != null)
		{
			mTargetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, true);
			Debug.Log("Damaging " + mTargetedCreature.name + " with Overflow.");
		}
		cardState = CardState.Cooldown;
    }
}
