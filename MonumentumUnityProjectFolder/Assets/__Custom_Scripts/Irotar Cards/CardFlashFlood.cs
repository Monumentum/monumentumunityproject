﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//During your conclusion phase, all friendly characters may move 1 square. This does not 'break combat'.

public class CardFlashFlood : Card 
{
	public TurnPhaseManager mTurnPhaseManager;

	public List<Creature> mMyCreatures = new List<Creature>();
	public List<int> mMyCreatureSpeeds = new List<int>();


	[SerializeField] bool mHasLetCreaturesMove = false;

	// Use this for initialization
	void Start () 
	{
		mTurnPhaseManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mTurnPhaseManager && cardState == CardState.Table)
		{
			if (mTurnPhaseManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion && !mHasLetCreaturesMove)
			{
				Debug.Log("Ready to do Flash Floods conclusion phase effect");
				OnConclusionPhase();
			}
		}
		if (mTurnPhaseManager && cardState == CardState.Table)
		{
			if (mTurnPhaseManager.playerTurn != playerNumber)
			{
				Debug.Log("Ready to do Flash Floods next turn effect");
				OnOpponentTurn();
			}
		}

	}

	public override void CardEffect()
	{
		Creature[] creatures = FindObjectsOfType<Creature>();
		
		foreach (Creature creature in creatures)
		{
			if ((creature.controllingPlayer == this.playerNumber) && creature.character)
			{
				mMyCreatures.Add(creature);
				mMyCreatureSpeeds.Add(creature.moveSpeed);
			}
		}

		//finish up the action and send this card to the Cooldown pile
		handManager.playingActionCard = false;
		handManager.mTargetting = false;
		handManager.actionCardToPlay = null;
		
	}
	
	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		CardEffect();
	}
	
	public void OnConclusionPhase()
	{
		Debug.Log("Doing Flash Floods conclusion phase effect");

		for (int i = 0; i < mMyCreatures.Count; i++)
		{
			if (mMyCreatures[i] != null && !mMyCreatures[i].lockedInCombat)
			{
				Debug.Log("Letting the following creature move: " + mMyCreatures[i].name);

				mMyCreatures[i].canMove = true;
				mMyCreatures[i].moveSpeed = 1;
			}
		}
		mHasLetCreaturesMove = true;
	}

	public void OnOpponentTurn()
	{
		Debug.Log("Doing Flash Floods next turn effect");

		for (int i = 0; i < mMyCreatures.Count; i++)
		{
			if (mMyCreatures[i] != null)
			{
				Debug.Log("Reseting speed for the following creature: " + mMyCreatures[i].name);
				if (mMyCreatureSpeeds[i] != null)
				{
					mMyCreatures[i].moveSpeed = mMyCreatureSpeeds[i];
				}
				else
				{
					mMyCreatures[i].moveSpeed = 4;
				}
				mMyCreatures[i].canMove = false;

			}
		}
		mMyCreatures.Clear();
		mMyCreatureSpeeds.Clear();
		cardState = CardState.Cooldown;
		mHasLetCreaturesMove = false;
	}


}
