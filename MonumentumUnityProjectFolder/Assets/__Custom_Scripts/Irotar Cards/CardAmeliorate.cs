﻿using UnityEngine;
using System.Collections;

public class CardAmeliorate : Card 
{
	public bool mShowDiscardedCharacters = false;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void CardEffect()
	{
		mShowDiscardedCharacters = true;
		Debug.Log("mShowDiscardedCharacters is now " + mShowDiscardedCharacters);

	}

	public override void CardTarget()
	{
		Debug.Log("Playing " + cardName);
		cardState = CardState.Table;
		TargetAllFriendlyCharacters();
	}

	void OnGUI ()
	{
		if (mShowDiscardedCharacters)
		{
			float handGUIXPos;
			float handGUIYPos;
			int handGUINumber;
			Card currentCardForGUI;

			//display list of Familiars/Minions in Cooldown/Recharge
			handGUIXPos = Screen.width*0.1f;
			handGUIYPos = Screen.height*0.5f;
			handGUINumber=1;

			for (int i = 0; i < handManager.deck.Count; i++)
			{
				currentCardForGUI = handManager.deck[i].GetComponent<Card>();
				//Debug.Log("Looking at the deck card " + currentCardForGUI.name);
				if ((currentCardForGUI.cardType == Card.CardType.Minion || currentCardForGUI.cardType == Card.CardType.Familiar) 
					    && (currentCardForGUI.cardState == CardState.Cooldown || currentCardForGUI.cardState == CardState.Recharge) )
				{
					//Debug.Log(currentCardForGUI.name + " is in the discard pile.");
					if(GUI.Button(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.1f, Screen.height*0.1f), new GUIContent(currentCardForGUI.cardName, "Replace with this character")) )
					{
						//get the old creature's position then kill it
						Vector3 swapPosition = handClickController.selectedCreature.transform.position;
						handClickController.selectedCreature.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All);

						//summon the new creature into the vacated space
						GameObject newSummon = (GameObject)Network.Instantiate(currentCardForGUI.summonedGameObject, swapPosition, Quaternion.identity,0);
						newSummon.GetComponent<NetworkView>().RPC("SummonPlayerInfo", RPCMode.All, playerNumber, currentCardForGUI.handNumber, currentCardForGUI.cardName, currentCardForGUI.mCardDescription);
						currentCardForGUI.cardState = CardState.Table;

						mShowDiscardedCharacters = false;

						//finish up the action and send this card to the Cooldown pile
						handManager.playingActionCard = false;
						handManager.mTargetting = false;
						handManager.actionCardToPlay = null;
						cardState = CardState.Cooldown;
					//	Debug.Log ("Done with Ameliorate.");

					}
					handGUIXPos += Screen.width*0.11f;
					if ((handGUINumber)%6==0)
					{
						handGUIXPos = Screen.width*0.1f;
						handGUIYPos += Screen.height*0.11f;
					}
					handGUINumber++;
						
				}
				
			}
		}
	}
}
