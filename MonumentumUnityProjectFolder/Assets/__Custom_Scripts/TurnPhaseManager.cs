﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script is used to control which player's turn it currently is and what phase that turn is currently on
//It is also used at the start of a match to assign the incoming player controller game objects their proper player numbers and tell them to instantiate the game objects that contstitute their decks/hands

public class TurnPhaseManager : MonoBehaviour 
{
	private int numberOfPlayers = 2;  //how many people are playing (right now it's only ever going to be 2, but that might change in future expansions, so leaving it as a varialbe)
	public int playerTurn = 1; //whoes turn it currently is
	public enum TurnPhase{Preparation, Summoning, Movement, Combat, Conclusion}; //the Phases of a turn
	public TurnPhase currentPhase = TurnPhase.Preparation; //What phase it currently is
	public List<PlayerClickController> playerControllers = new List<PlayerClickController>();
	public Assets.CustomScripts.NetworkManager mNetworkManager;
	public BoardCreation mBoardCreator;	
	// Use this for initialization
	void Start () 
	{
		if (!mNetworkManager)
		{
			mNetworkManager = FindObjectOfType(typeof(Assets.CustomScripts.NetworkManager)) as Assets.CustomScripts.NetworkManager;
		}
		if (!mBoardCreator)
		{
			mBoardCreator = FindObjectOfType(typeof(BoardCreation)) as BoardCreation;
		}
	}//End of Start()
	
	// Update is called once per frame
	void Update () 
	{
	
	}//End of Update()

	//Gets called when a new player enters the scene.  Used to keep track of the players
    public void UpdatePlayerCount(PlayerClickController newPlayer)
    {
		//If this is the first player to enter the scene, add them, assign them a player number, and build their hand/deck
		if (playerControllers.Count == 0)
		{
        	playerControllers.Add(newPlayer);
        	newPlayer.playerNumber = playerControllers.Count;
			playerControllers[0].handManager.deck.Clear();
			playerControllers[0].handManager.CreateHand();

		}
		//When it's not the first player to enter the scene, make sure they get added in the right order, 
			//with the server being player 1 and the client being player 2
		else
		{
			bool newPlayerAdded = false;
			for (int i = 0; i < playerControllers.Count; i++)
			{
				if(int.Parse(playerControllers[i].GetComponent<NetworkView>().owner.ToString()) > int.Parse(newPlayer.GetComponent<NetworkView>().owner.ToString()) && !newPlayerAdded)
				{
					playerControllers.Insert(i, newPlayer);
					newPlayerAdded = true;
					//Once you've got multiple players in, randomize the shape of the board/arena
					mNetworkManager.RandomizeLocations();
					mNetworkManager.GetComponent<NetworkView>().RPC("SetMapSettings", RPCMode.All, mNetworkManager.mCorridorTilePosition, mNetworkManager.mJunctionTilePosition, mNetworkManager.mTileRotations);
					mBoardCreator.GetComponent<NetworkView>().RPC("StartBoardCreation", RPCMode.All);
				}
			}
			if (!newPlayerAdded)
			{
				playerControllers.Add(newPlayer);
			}
			//Make sure that all players have their numbers accuratlely assigned, their decks/hands built, and their camera positions assigned
			for (int k = 0; k < playerControllers.Count; k++)
			{
				playerControllers[k].playerNumber = k+1;
				playerControllers[k].SetCameraPos();
				playerControllers[k].handManager.deck.Clear();
				playerControllers[k].handManager.CreateHand();
			}
		}
	}//END of UpdatePlayerCount()

	void OnGUI()
	{


	
	}//End of OnGUI()

    
	//Function that gets called accross the network to advance the current phase/turn.
	//Gets called from the player clicking a GUI button in PlayerClickController
    [RPC]
	public void AdvancePhase()
	{
		PlayerHandManager[] playerHands = FindObjectsOfType<PlayerHandManager>();
		foreach(PlayerHandManager hand in playerHands)
		{
			hand.viewActionCards = false;
		}
		//go to the next phase of the turn
		if (currentPhase != TurnPhase.Conclusion)
		{
			currentPhase ++;
		}
		//if it's the Conclusion Phase, go to the next Preparation Phase of the next player's turn
		else
		{
            PlayerClickController[] players = FindObjectsOfType<PlayerClickController>();
            foreach (PlayerClickController player in players)
            {
                player.mCanSwap = false;
            }
            Creature[] creatures = FindObjectsOfType<Creature>();
            foreach (Creature creature in creatures)
            {
                creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
            }
			if (playerTurn != playerControllers.Count)
			{
				playerTurn++;
			}
			else
			{
				playerTurn = 1;
			}
			currentPhase = TurnPhase.Preparation;
		}

		//find all the current player's creatures
		Creature[] creaturesOnBoard = FindObjectsOfType(typeof(Creature)) as Creature[];
		List<Creature> currentPlayerCreatures = new List<Creature>();
		foreach (Creature boardCreature in creaturesOnBoard)
		{
			if (boardCreature.controllingPlayer == playerTurn)
			{
				currentPlayerCreatures.Add(boardCreature);
			}
			   
		}

		//What to do if it is now the Preparation Phase
		if(currentPhase == TurnPhaseManager.TurnPhase.Preparation)
		{
			//refill player source stones and move cards from Recharge->Cooldown->Hand
			playerControllers[playerTurn-1].GetComponent<PlayerClickController>().RefillSourceStones();
			for (int i = 0; i < playerControllers[playerTurn-1].GetComponent<PlayerHandManager>().deck.Count; i++)
			{
				if (playerControllers[playerTurn-1].GetComponent<PlayerHandManager>().deck[i].cardState == Card.CardState.Recharge)
				{
					playerControllers[playerTurn-1].GetComponent<PlayerHandManager>().deck[i].cardState = Card.CardState.Hand;
				}
				else if (playerControllers[playerTurn-1].GetComponent<PlayerHandManager>().deck[i].cardState == Card.CardState.Cooldown)
				{
					playerControllers[playerTurn-1].GetComponent<PlayerHandManager>().deck[i].cardState = Card.CardState.Recharge;
				}

			}
			foreach(Creature playerCreature in currentPlayerCreatures)
			{
				playerCreature.BeginPreparationPhase();
			}
		}
		//What to do if it is now the Summoning Phase
		if(currentPhase == TurnPhaseManager.TurnPhase.Summoning)
		{
			foreach(Creature playerCreature in currentPlayerCreatures)
			{
				playerCreature.BeginSummoningPhase();
			}
		}
		//What to do if it is now the Movement Phase
		if(currentPhase == TurnPhaseManager.TurnPhase.Movement)
		{
			foreach(Creature playerCreature in currentPlayerCreatures)
			{
				playerCreature.BeginMovementPhase();
			}
		}
		//What to do if it is now the Combat Phase
		if(currentPhase == TurnPhaseManager.TurnPhase.Combat)
		{
			foreach(Creature playerCreature in currentPlayerCreatures)
			{
				playerCreature.BeginCombatPhase();
			}
		}
		//What to do if it is now the Conclusion Phase
		if(currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
		{
			if (GetComponent<CombatInfoDisplay>() != null)
			{
				GetComponent<NetworkView>().RPC("ClearCombatants", RPCMode.All);
			}
			foreach(Creature playerCreature in currentPlayerCreatures)
			{
				playerCreature.BeginConclusionPhase();
			}
		}


	}//END of AdvancePhase()
}
