﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour 
{
    public BoardSquare mOccupiedSquare;
    public int mControllingPlayer;
    protected Creature mVoidBreacher;
    public Card mAssociatedCard;
	MouseOverInfoManager mMouseOverInfoManager;

	string mTrapDescription = "";


	// Use this for initialization
	public virtual void Start () 
    {
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public virtual void ActivateTrap(Creature victim)
    {

    }

	[RPC] public virtual void SetTrap(int myPlayerNumber, string trapName, string description)
	{
		mControllingPlayer = myPlayerNumber;
        Creature[] voidBreachersInPlay = FindObjectsOfType(typeof(Creature)) as Creature[];
        foreach (Creature voidBreacher in voidBreachersInPlay)
        {
            if (voidBreacher.tag == "VoidBreacher" && voidBreacher.controllingPlayer == mControllingPlayer)
            {
                mVoidBreacher = voidBreacher;
            }
        }
		name = trapName;
		mTrapDescription = description;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Creature creature = other.GetComponent<Creature>();
        if (creature)
        {
            if (creature.controllingPlayer != this.mControllingPlayer)
            {
                ActivateTrap(creature);
            }
        }
//        else if (creature && creature.networkView.isMine)
//        {
//            if (creature.controllingPlayer != this.mControllingPlayer)
//            {
//                Destroy(this.gameObject);
//            }
//        }
    }

	void OnMouseEnter()
	{
		mMouseOverInfoManager.SetCreatureToolTip(true, name + "\n" + mTrapDescription);
	}//END of OnMouseEnter()
	
	void OnMouseExit()
	{
		mMouseOverInfoManager.SetCreatureToolTip(false, "");		
	}//END of OnMouseExit()

}
