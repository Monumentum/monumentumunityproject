﻿using UnityEngine;
using System.Collections;

//Trap: When activated by target enemy creature, target suffers a 8S strike.
public class TornadoTrap : Trap 
{
    public int mStrength = 8;

    public override void ActivateTrap(Creature victim)
    {
		if (GetComponent<NetworkView>().isMine)
		{
	        int thisAttackPower = mStrength;
	        int rollBonus = 0;

	        for (int i = 0; i < 2; i++)
	        {
	            int dieRoll = 0;
	            dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
	            dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));

	            if (dieRoll > rollBonus)
	            {
	                rollBonus = dieRoll;
	            }
	        }
	        thisAttackPower += rollBonus;

	        if (victim.tag == "VoidBreacher")
	        {
	            //do nothing to a VoidBreacher
	        }
	        else if (thisAttackPower == 4 + 2)//Automatically miss if you roll a 1 on both dice
	        {
	            Debug.Log(this.name + " missed " + victim.name);
	            victim.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
	        }
	        else if (thisAttackPower > victim.fortitude || thisAttackPower == 4 + 12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
	        {
	            victim.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, true);
	            Debug.Log(this.name + " hit " + victim.name + " leaving it with " + victim.hitPoints + " HP");
	        }
	        else//Miss if you roll lower than the target's Fortitude
	        {
	            Debug.Log(this.name + " missed " + victim.name);
	            victim.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, mVoidBreacher.GetComponent<NetworkView>().viewID, false);
	        }

	        //Done attacking now

	        //if you took the target down to 0 HP, kill it and move into its space if it was adjacent
	        if (victim.hitPoints == 0)
	        {
	            victim.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
	        }

	        this.mAssociatedCard.cardState = Card.CardState.Cooldown;
		}

        Destroy(this.gameObject);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Creature creature = other.GetComponent<Creature>();
    //    if (creature && !creature.networkView.isMine)
    //    {
    //        if (creature.controllingPlayer != this.mControllingPlayer)
    //        {
    //            ActivateTrap(creature);
    //        }
    //    }
    //}
}
