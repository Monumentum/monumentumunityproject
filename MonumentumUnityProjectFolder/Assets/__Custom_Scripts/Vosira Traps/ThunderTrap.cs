﻿using UnityEngine;
using System.Collections;

//Trap: When activated by target enemy creature, target suffers -2F.
public class ThunderTrap : Trap 
{
    public int mDecreaseAmount = -2;
	// Use this for initialization



    public override void ActivateTrap(Creature victim)
    {
		if (GetComponent<NetworkView>().isMine)
		{
	        victim.ModifyFortitude(mDecreaseAmount);
	        victim.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.All, victim.strength, victim.fortitude);

	        this.mAssociatedCard.cardState = Card.CardState.Cooldown;
		}

        Destroy(this.gameObject);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Creature creature = other.GetComponent<Creature>();
    //    if (creature && !creature.networkView.isMine)
    //    {
    //        if (creature.controllingPlayer != this.mControllingPlayer)
    //        {
    //            ActivateTrap(creature);
    //        }
    //    }
    //}
}
