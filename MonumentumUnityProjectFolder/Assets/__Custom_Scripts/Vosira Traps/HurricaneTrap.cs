﻿using UnityEngine;
using System.Collections;

//Trap: When activated by target enemy creature, target cannot move further this turn and is 'crippled'.
public class HurricaneTrap : Trap 
{


	public override void ActivateTrap(Creature victim)
	{
		victim.mPathSquares.Clear();
		victim.mPathSquares.Add(mOccupiedSquare.gameObject);
		victim.canMove = false;
		victim.crippled++;
		if (GetComponent<NetworkView>().isMine)
		{
			this.mAssociatedCard.cardState = Card.CardState.Cooldown;
		}
		Destroy(this.gameObject);
		//victim.networkView.RPC("GetCrippled", RPCMode.All);
	}
	

}
