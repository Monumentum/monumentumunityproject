﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab.Examples;
using PlayFab;
using PlayFab.ClientModels;

public class CustomEventLogger : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	
	}


	public void LogCustomEvent(string eventName, Dictionary<string,object> body)
	{
		PlayFab.ClientModels.LogEventRequest request = new LogEventRequest ();
		request.EventName = eventName;
		request.Body = body;
		if (PlayFabData.AuthKey != null)
		{
			Debug.Log("Doing LogCustomEvent()");
			PlayFabClientAPI.LogEvent (request, EventLogged, OnPlayFabError);
		}
	}

	private void SavePlayerState()
	{
			// first save as a player property (poor man's save game)
			Debug.Log ("Saving player data...");
			UpdateUserDataRequest request = new UpdateUserDataRequest ();
			request.Data = new Dictionary<string, string> ();
			request.Data.Add ("TotalKills", PlayFabGameBridge.totalKills.ToString ());
		Debug.Log("Doing SavePlayerState()");
		PlayFabClientAPI.UpdateUserData (request, PlayerDataSaved, OnPlayFabError);
			
//			// also save score as a stat for the leaderboard use...
//			Dictionary<string,int> stats = new Dictionary<string,int> ();
//			stats.Add("score", PlayFabGameBridge.totalKills);
//			storeStats(stats);

	}


	private void PlayerDataSaved(UpdateUserDataResult result)
	{
		Debug.Log ("PLayer Data saved.");
	}

	public void storeStats(Dictionary<string,int> stats)
	{
		PlayFab.ClientModels.UpdateUserStatisticsRequest request = new PlayFab.ClientModels.UpdateUserStatisticsRequest ();
		request.UserStatistics = stats;
		if (PlayFabData.AuthKey != null)
		{
			Debug.Log("Doing storeStats()");
			PlayFabClientAPI.UpdateUserStatistics (request, StatsUpdated, OnPlayFabError);
			Debug.Log ("Updating User Statistics");
		}
		else
		{
			Debug.Log ("Can't find PlayFab Data Auth Key");
		}
	}
	
	private void StatsUpdated(PlayFab.ClientModels.UpdateUserStatisticsResult result)
	{
		Debug.Log ("Stats updated");
	}


	private void EventLogged(LogEventResult result)
	{
		Debug.Log ("Event error " + result);
	}
	
	void OnPlayFabError(PlayFabError error)
	{
		Debug.Log ("Got an error: " + error.ErrorMessage);
	}
}
