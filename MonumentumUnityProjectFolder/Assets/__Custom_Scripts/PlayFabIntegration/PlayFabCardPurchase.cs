using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Serialization.JsonFx;
using PayPalLibrary;

namespace PlayFab.MonumentumIntegration
{

	// hack to help deserialize the custom data out of the catalog item
	public class IconClass 
	{
		public string iconName;
	}


	public class PlayFabCardPurchase : MonoBehaviour 
	{

		public Texture2D marketIcon;
		public Texture2D marketMenu;
		public Texture2D rhavikPack;
		public Texture2D irotarPack;
		public Texture2D vosiraPack;
		public Texture2D dominionsPack;
		public Texture2D close;
		public Texture2D cursor;

		public int titleSize;
		public int textSize;
		public int priceTextSize;
		public int textY;
		public int iconsSpace;
		public int iconsY;
		public int buttonX;
		public int buttonY;

		public bool showMenu;

		private bool drawCursor;

		private List<CatalogItem> items; 
		private Dictionary<string,Texture2D> icons;
		private Dictionary<string,Texture2D> itemIcons;

		[SerializeField] private bool renderCatalog = false;

		private bool mHaveIrotarPack = false;
		private bool mHaveVosiraPack = false;
		private bool mHaveRhavikPack = false;
		private bool mHaveTwoPacks = false;

		private  List<ItemInstance> mPlayerInventory;


		public int mCurrentVCBalance;
		public Text mCurrentBalanceText;

		//For PayPal integration
		public PayPalHandler mPayPalHandler;

		//Variables to hold for ConfirmPurchase
		public StartPurchaseResult mPlayFabStartResult;

		private void Start () 
		{
			if (PlayFabData.AuthKey != null)
			{
				PlayFabClientAPI.GetUserInventory (new GetUserInventoryRequest(),OnGetUserInventory, OnPlayFabError);
			}


			////
			/// Get's the specified version of the title's catalog of virtual goods, including purchase options and pricing details
			/// associated with the game title and catalog verion set in Playfab / Developer settings
			////
			icons = new Dictionary<string,Texture2D> ();
			icons.Add ("vosiraPack", vosiraPack);
			icons.Add ("rhavikPack", rhavikPack);
			icons.Add ("irotarPack", irotarPack);
			icons.Add ("dominionsPack", dominionsPack);

			GetCatalogItemsRequest request = new GetCatalogItemsRequest();
			request.CatalogVersion = PlayFabData.CatalogVersion;
			if (PlayFabData.AuthKey != null)
			{
				PlayFabClientAPI.GetCatalogItems (request,ConstructCatalog,OnPlayFabError);
			}
			//Time.timeScale = 0;
		}//END of Start()
		
		void OnGUI () 
		{

//			if(renderCatalog)
//			{
//				Rect marketIconRect = new Rect (iconsSpace,Screen.height-marketIcon.height-iconsSpace,marketIcon.width,marketIcon.height );
////				if (GUI.Button (marketIconRect, "Return to Lobby",GUIStyle.none)) 
////				{
////					Application.LoadLevel("Lobby");
////				}
//				drawCursor = false;
//				if (Input.mousePosition.x < marketIconRect.x + marketIconRect.width && Input.mousePosition.x > marketIconRect.x && Screen.height - Input.mousePosition.y < marketIconRect.y + marketIconRect.height && Screen.height - Input.mousePosition.y > marketIconRect.y)
//				{
//					drawCursor = true;
//				}
//
//
//					PlayFabGameBridge.mouseOverGui = true;
//					Rect winRect = new Rect (Screen.width * 0.5f - (marketMenu.width+90) *0.5f,100,marketMenu.width+90,marketMenu.height );
//					GUI.DrawTexture (winRect, marketMenu);
//					if (Input.mousePosition.x < winRect.x + winRect.width && Input.mousePosition.x > winRect.x && Screen.height - Input.mousePosition.y < winRect.y + winRect.height && Screen.height - Input.mousePosition.y > winRect.y)
//						drawCursor = true;
//
//
//
//					GUIStyle centeredStyle = GUI.skin.GetStyle("Label");
//					centeredStyle.alignment = TextAnchor.UpperCenter;
//
//					int btnWidth = 95;
//					int btnHeight = 165;
//
//					for(int x = 0; x < items.Count;  x++)
//					{
//						bool shouldDrawButton = true;
//
//						if ( (items[x].ItemId == "irotar_pack_1" && mHaveIrotarPack) || 
//					    	(items[x].ItemId == "vosira_pack_1" && mHaveVosiraPack) || 
//					    	(items[x].ItemId == "rhavik_pack_1" && mHaveRhavikPack) || 
//					    	(items[x].ItemId == "dominions_pack_1" && mHaveTwoPacks) )
//						{
//							shouldDrawButton = false;
//						}
//
//						if (shouldDrawButton)
//						{
//							Texture2D texture = itemIcons[items[x].ItemId];
//							Rect btn1Rect = new Rect (winRect.x+buttonX+(btnWidth*x)+(iconsSpace*x),winRect.y+buttonY,btnWidth,btnHeight );	
//							Rect labelRect = GUILayoutUtility.GetRect(new GUIContent("<size="+titleSize+">"+items[x].DisplayName+"</size>"), "label");
//							labelRect.x = btn1Rect.x + btn1Rect.width*0.5f-labelRect.width*0.5f;
//							labelRect.y = btn1Rect.y+textY;
//							Rect rhavikPackRect = new Rect (btn1Rect.x+btn1Rect.width*0.5f-texture.width*0.5f,btn1Rect.y+iconsY,texture.width, texture.height);
//							Rect labelRectb = GUILayoutUtility.GetRect(new GUIContent("<size="+textSize+">"+items[x].Description+"</size>"), "label");
//							labelRectb.width = 100;
//							labelRectb.height = 80;
//							labelRectb.x = btn1Rect.x + btn1Rect.width*0.5f-labelRectb.width*0.5f;
//							labelRectb.y = rhavikPackRect.y+rhavikPackRect.height+textY;
//
//							foreach (KeyValuePair<string, uint> price in items[x].VirtualCurrencyPrices)
//							{
//
//								if (GUI.Button(btn1Rect,""))
//								{
//									PurchaseItemRequest request = new PurchaseItemRequest();
//									request.CatalogVersion = items[x].CatalogVersion;
//									request.VirtualCurrency = price.Key;
//									request.Price = Convert.ToInt32(price.Value);
//									request.ItemId = items[x].ItemId;
//									PlayFabClientAPI.PurchaseItem(request,PlayFabItemsController.OnPurchase,OnPlayFabError);
//									
//									switch (items[x].ItemId)
//									{
//									case "irotar_pack_1":
//										mHaveIrotarPack = true;
//										break;
//									case "vosira_pack_1":
//										mHaveVosiraPack = true;
//										break;
//									case "rhavik_pack_1":
//										mHaveRhavikPack = true;
//										break;
//									case "dominions_pack_1":
//										mHaveIrotarPack = true;
//										mHaveVosiraPack = true;
//										mHaveRhavikPack = true;
//										mHaveTwoPacks = true;
//										break;
//									default:
//											break;
//									}
//								}
//								GUI.Label (new Rect (btn1Rect.x+btn1Rect.width*0.5f-10,labelRectb.y+50,40, 40), "<size="+priceTextSize+">"+price.Value+" </size>",centeredStyle);
//							}
//							GUI.Label (labelRect, "<size="+titleSize+">"+items[x].DisplayName+"</size>",centeredStyle);
//							GUI.DrawTexture (rhavikPackRect, texture);
//							GUI.Label (labelRectb, "<size="+textSize+">"+items[x].Description+"</size>",centeredStyle);
//						}
//					}
//				
//				if (drawCursor)
//				{
//					Rect cursorRect = new Rect (Input.mousePosition.x,Screen.height-Input.mousePosition.y,cursor.width,cursor.height );
//					GUI.DrawTexture (cursorRect, cursor);
//					PlayFabGameBridge.mouseOverGui = true;
//				}
//			}
		}//END of OnGUI()

		void Update ()
		{
			RefreshCatalog();
			if( (mHaveRhavikPack&&mHaveIrotarPack) || (mHaveRhavikPack&&mHaveVosiraPack) || (mHaveIrotarPack&&mHaveVosiraPack) )
			{
				mHaveTwoPacks = true;
			}

			mCurrentBalanceText.text = "Current Balance: " + mCurrentVCBalance;

				GetUserCombinedInfoRequest combinedInfoRequest = new GetUserCombinedInfoRequest();
				PlayFabClientAPI.GetUserCombinedInfo(combinedInfoRequest, UpdateCurrencyBalance, OnPlayFabError);

		}//END of Update()

		/////
		/// 
		/// 
		/// 		Construct and Render the Catalog based on the Catalog Version
		/// 
		/// 
		/////
		
		private void ConstructCatalog(GetCatalogItemsResult result)
		{
			items = result.Catalog;
			renderCatalog = true;

			itemIcons = new Dictionary<string, Texture2D> ();


			for (int x = 0; x < items.Count; x++)
			{
				Dictionary<string,string> customData = JsonReader.Deserialize<Dictionary<string,string>>(items[x].CustomData);
				itemIcons.Add(items[x].ItemId, icons[customData["Icon"]]);

			}
			Time.timeScale = 1;
		}//END of ContstructCatalog()

		void OnPlayFabError(PlayFabError error)
		{
			Debug.Log ("Got an error: " + error.ErrorMessage);
		}//END of OnPlayFabError()

		void OnStartPurchaseSuccess(StartPurchaseResult startResut)
		{

		}

		private void UpdateCurrencyBalance(GetUserCombinedInfoResult userCombinedInfo)
		{
			//Debug.Log("Updating Currency Balance: " + userCombinedInfo.VirtualCurrency["MC"]);
			mCurrentVCBalance = userCombinedInfo.VirtualCurrency["MC"];
		}

		void RefreshCatalog()
		{
			//variables for turning off the bundle pack option when 2/3 packs have been purchased
			int expansionSetOneCount = 0;
			bool checkedForIrotarPackOne = false;
			bool checkedForVosiraPackOne = false;
			bool checkedForRhavikPackOne = false;

			List<CatalogItem> removedItems =  new List<CatalogItem>();
			if (items != null && mPlayerInventory != null && mPlayerInventory.Count > 0)
			{
				foreach(ItemInstance inventoryItem in mPlayerInventory)
				{
					//Check to update the count of how many individual packs have been bought for the purposes of turning off the bundle pack option
					if(inventoryItem.ItemId == "irotar_pack_1" && !checkedForIrotarPackOne)
					{
						expansionSetOneCount++;
						checkedForIrotarPackOne = true;
						mHaveIrotarPack = true;
					}
					if(inventoryItem.ItemId == "vosira_pack_1" && !checkedForVosiraPackOne)
					{
						expansionSetOneCount++;
						checkedForVosiraPackOne = true;
						mHaveVosiraPack = true;
					}
					if(inventoryItem.ItemId == "rhavik_pack_1" && !checkedForRhavikPackOne)
					{
						expansionSetOneCount++;
						checkedForRhavikPackOne = true;
						mHaveRhavikPack = true;
					}
					//Compare items already owned with items in the store, ignoring items that purchase currency
//					foreach(CatalogItem itemToRemove in items)
//					{
//						if( (itemToRemove != null) && (inventoryItem.ItemClass != "MonumentumCurrencyBundle") )
//						{
//							if (inventoryItem.ItemId == itemToRemove.ItemId)
//							{
//								removedItems.Add(itemToRemove);
//							}
//						}
//					}
				}
			}

//			//Remove the option to buy the three-pack if the player already owns 2/3
//			if (expansionSetOneCount >= 2)
//			{
//				foreach(CatalogItem bundlePackToRemove in items)
//				{
//					if(bundlePackToRemove.ItemId == "dominions_pack_1")
//					{
//						removedItems.Add(bundlePackToRemove);
//					}
//				}
//			}
//			//Actually remove the items from the store list
//			foreach(CatalogItem removedItem in removedItems)
//			{
//				items.Remove(removedItem);
//			}
		}//END of RefreshCatalog()
		
		private void OnGetUserInventory(GetUserInventoryResult result)
		{
			mPlayerInventory = result.Inventory;
		}//END of OnGetUserInventory()



		public void PlayfabPackPurchase(int packNumber)
		{
			//Pack numbers:
				//1: Irotar(Ripple)
				//2: Vosira(Lark)
				//3: Rhavik (Flint)
				//4: Dominion bundle
			foreach(Button buttonToDisable in FindObjectsOfType<Button>())
			{
				buttonToDisable.interactable = false;
			}

			foreach (KeyValuePair<string, uint> price in items[packNumber-1].VirtualCurrencyPrices)
			{
				PurchaseItemRequest request = new PurchaseItemRequest();
				request.CatalogVersion = items[packNumber-1].CatalogVersion;
				request.VirtualCurrency = price.Key;
				request.Price = Convert.ToInt32(price.Value);
				request.ItemId = items[packNumber-1].ItemId;

				GetUserCombinedInfoRequest combinedInfoRequest = new GetUserCombinedInfoRequest();
				PlayFabClientAPI.GetUserCombinedInfo(combinedInfoRequest, UpdateCurrencyBalance, OnPlayFabError);
				if(mCurrentVCBalance >= request.Price)
				{
					switch (packNumber)
					{
					case 1:
						
						mHaveIrotarPack = true;
						break;
					case 2:
						mHaveVosiraPack = true;
						break;
					case 3:
						mHaveRhavikPack = true;
						break;
					case 4:
						mHaveIrotarPack = true;
						mHaveVosiraPack = true;
						mHaveRhavikPack = true;
						mHaveTwoPacks = true;
						break;
					default:
						break;
					}
				}
				else
				{
					Debug.Log (mCurrentVCBalance);
					Debug.Log ("Not enough VC");

				}



				PlayFabClientAPI.PurchaseItem(request,PlayFabItemsController.OnPurchase,OnPlayFabError);
				PlayFabClientAPI.GetUserCombinedInfo(combinedInfoRequest, UpdateCurrencyBalance, OnPlayFabError);

			}

			foreach(Button buttonToEnsable in FindObjectsOfType<Button>())
			{
				buttonToEnsable.interactable = true;
			}

		}

		public void CheckIfAlreadyPurchased(int packNumber, GameObject checkingObject, Button buyButton)
		{
			//Pack numbers:
				//1: Irotar(Ripple)
				//2: Vosira(Lark)
				//3: Rhavik (Flint)
				//4: Dominion bundle
			switch (packNumber)
			{
			case 1:
				if(mHaveIrotarPack)
				{
					Destroy(checkingObject);
				}
				else if (items.Count > 0)
				{
					buyButton.interactable = true;
				}
				break;
			case 2:
				if(mHaveVosiraPack)
				{
					Destroy(checkingObject);
				}
				else if (items.Count > 0)
				{
					buyButton.interactable = true;
				}
				break;
			case 3:
				if(mHaveRhavikPack)
				{
					Destroy(checkingObject);
				}
				else if (items.Count > 0)
				{
					buyButton.interactable = true;
				}
				break;
			case 4:
				if(mHaveTwoPacks)
				{
					Destroy(checkingObject);
				}
				else if (items.Count > 0)
				{
					buyButton.interactable = true;
				}
				break;
			default:
				break;
			}

		}

		public void PlayFabCurrencyPurchase(int CurrencyPackNumber)
		{
			//Currency Pack numbers:
			//1: 100
			//2: 250
			//3: 500
			foreach(Button buttonToDisable in FindObjectsOfType<Button>())
			{
				buttonToDisable.interactable = false;
			}
			
			foreach (KeyValuePair<string, uint> price in items[CurrencyPackNumber+3].VirtualCurrencyPrices)
			{

				
				//GetUserCombinedInfoRequest combinedInfoRequest = new GetUserCombinedInfoRequest();
				//PlayFabClientAPI.GetUserCombinedInfo(combinedInfoRequest, UpdateCurrencyBalance, OnPlayFabError);
				//Try to make the purchase ~Adam
				switch (CurrencyPackNumber)
				{
				case 1:
					//Try to purchase the 100 MC pack.  Second variable is the price in Real Money of the pack. ~Adam
					MakeCurrencyPurchase(1, 1f);


						break;
				case 2:
					//Try to purchase the 250 MC pack.  Second variable is the price in Real Money of the pack. ~Adam
					MakeCurrencyPurchase(2, 1f);
					break;
				case 3:
					//Try to purchase the 500 MC pack.  Second variable is the price in Real Money of the pack. ~Adam
					MakeCurrencyPurchase(3, 1f);
					break;
				default:
					break;
				}

				
				
//				PlayFabClientAPI.PurchaseItem(request,PlayFabItemsController.OnPurchase,OnPlayFabError);
//				PlayFabClientAPI.GetUserCombinedInfo(combinedInfoRequest, UpdateCurrencyBalance, OnPlayFabError);
				
			}
			
			foreach(Button buttonToEnsable in FindObjectsOfType<Button>())
			{
				buttonToEnsable.interactable = true;
			}

		}//END of PlayFabCurrencyPurchase()

		void MakeCurrencyPurchase(int packNumber, float packPrice)
		{
			Debug.Log ("Running MakeCurrencyPurchase!");
			#region Start the Purchase
			mPayPalHandler.mPrice = packPrice.ToString();
			//Declare the item to purchase ~Adam
			ItemPuchaseRequest itemToPurchase = new ItemPuchaseRequest();
			itemToPurchase.ItemId = items[packNumber+3].ItemId;
			
			itemToPurchase.Quantity = 1;
			Debug.Log(itemToPurchase);

			List<ItemPuchaseRequest> itemArray = new List<ItemPuchaseRequest>();
			itemArray.Add(itemToPurchase);
			//Create a request to purchase the item ~adam


			StartPurchaseRequest startRequest = new StartPurchaseRequest();
			startRequest.CatalogVersion = items[packNumber+3].CatalogVersion;
			startRequest.StoreId = "MMStore";
			Debug.Log (startRequest.Items);
			startRequest.Items = itemArray;
			Debug.Log (startRequest.Items);

			
			//I have no idea what these two lines are doing but I think they're working? ~adam
			PlayFabClientAPI.StartPurchaseCallback startCallback = new PlayFabClientAPI.StartPurchaseCallback((StartPurchaseResult startResult) => OnStartPurchase (startResult));
			ErrorCallback purchaseErrorCallback = new ErrorCallback((PlayFabError error) => new PlayFabError());
			Debug.Log ("Calling StartPurchase");
			PlayFabClientAPI.StartPurchase (startRequest,startCallback, purchaseErrorCallback);
			

			#endregion
		}

		void OnStartPurchase(StartPurchaseResult result)
		{
			Debug.Log ("Running OnStartPurchase!");
			// process the StartPurchaseResult, call PayForPurchase
			ErrorCallback purchaseErrorCallback = new ErrorCallback((PlayFabError error) => new PlayFabError());

			#region Pay for the Purchase
			//Create a request to pay for the item ~Adam
			PayForPurchaseRequest payRequest = new PayForPurchaseRequest();
			payRequest.OrderId = result.OrderId;
			payRequest.ProviderName = "PayPal";
			payRequest.Currency = "RM";

			PlayFabClientAPI.PayForPurchaseCallback payCallback = new PlayFabClientAPI.PayForPurchaseCallback((PayForPurchaseResult payResult) => OnPayForPurchase (payResult));
			//PlayFabClientAPI.PayForPurchaseCallback payCallback = new PlayFabClientAPI.PayForPurchaseCallback((PayForPurchaseResult payResult) => new PayForPurchaseResult());

			Debug.Log (payCallback);
			Debug.Log ("Attempting to Pay for the Purchase!");

			PlayFabClientAPI.PayForPurchase(payRequest,payCallback, purchaseErrorCallback);
			Debug.Log ("Think I paid for the purchase?");

			
			#endregion

			
			mPlayFabStartResult = result;
			mPayPalHandler.mWaitingForConfirm = true;
			mPayPalHandler.SetPayPalCredentials ();
			mPayPalHandler.PayPalExpressCheckout ();

//			Debug.Log ("Running OnPayForPurchase!");
			// process the StartPurchaseResult, call PayForPurchase

			

		}//END of OnStartPurchase()

		void OnPayForPurchase(PayForPurchaseResult result)
		{
			Debug.Log ("Running OnPayForPurchase!");
			Debug.Log ("Setting mPrice");
			mPayPalHandler.mPrice = result.PurchasePrice.ToString();
			
		}//END of OnPayForPurchase()

		public void OnConfirmPurchase()
		{
			ErrorCallback purchaseErrorCallback = new ErrorCallback((PlayFabError error) => new PlayFabError());

			#region Confirm the Purchase
			ConfirmPurchaseRequest confirmRequest = new ConfirmPurchaseRequest();
			confirmRequest.OrderId = mPlayFabStartResult.OrderId;
			
			
			PlayFabClientAPI.ConfirmPurchaseCallback confirmCallback = new PlayFabClientAPI.ConfirmPurchaseCallback((ConfirmPurchaseResult confirmResult) => new ConfirmPurchaseResult());
			Debug.Log ("Attempting to Confirm the Purchase!");
			
			PlayFabClientAPI.ConfirmPurchase (confirmRequest, confirmCallback, purchaseErrorCallback);
			Debug.Log ("Think I Confirmed the purchase?");
			
			#endregion
		}



	}//END of Monobehaviour


}
