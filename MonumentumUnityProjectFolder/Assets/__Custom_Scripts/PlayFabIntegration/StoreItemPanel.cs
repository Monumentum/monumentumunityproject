﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using PlayFab.MonumentumIntegration;

public class StoreItemPanel : MonoBehaviour 
{
	public PlayFabCardPurchase mCardPurchaseManager;
	public int mPackCheckNumber;
	public Button mBuyButton;
	// Use this for initialization
	void Start () 
	{
		//mCardPurchaseManager.CheckIfAlreadyPurchased(mPackCheckNumber, this.gameObject, mBuyButton);
	}
	
	// Update is called once per frame
	void Update () 
	{
		mCardPurchaseManager.CheckIfAlreadyPurchased(mPackCheckNumber, this.gameObject, mBuyButton);
	}
}
