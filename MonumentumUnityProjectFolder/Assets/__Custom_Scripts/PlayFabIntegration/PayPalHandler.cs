﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Serialization.JsonFx;
using PayPalLibrary;
using PlayFab.MonumentumIntegration;

public class PayPalHandler : MonoBehaviour 
{
	private PayPalAPICaller PayPalAPI;
	[SerializeField] private PlayFabCardPurchase mPlayFabManager;
	public bool mWaitingForConfirm = false;

	[SerializeField] private GameObject mStoreInventory;
	[SerializeField] private GameObject mConfirmWindow;
	[SerializeField] private GameObject[] mCoinButtons;
	public string mPrice;


	// Use this for initialization
	void Start () 
	{
		PayPalAPI = new PayPalLibrary.PayPalAPICaller();
		PayPalAPI.Sandbox = true;
		PayPalAPI.ReturnMessage = "";
	}//END of Start()
	
	// Update is called once per frame
	void Update () 
	{
		mStoreInventory.SetActive (!mWaitingForConfirm);
		mConfirmWindow.SetActive (mWaitingForConfirm);
		foreach (GameObject coinButton in mCoinButtons)
		{
			coinButton.SetActive (!mWaitingForConfirm);
		}
	}//END of Update()


	public void SetPayPalCredentials()
	{
		PayPalAPI.SetCredentials("Harvey-facilitator-1_api1.Play-Monumentum.com", "673T59RQS86FU2EU", "An5ns1Kso7MWUdW4ErQKJJJ4qi4-AHg3LReVXynexvtH3-hg4ualDvXI");
	}


	public void PayPalExpressCheckout()
	{
		Debug.Log("mPrice is " +mPrice);
		PayPalAPI.ReturnMessage = "";
		bool success1 = PayPalAPI.ExpressCheckout("http://www.Success.link", "http://www.Cancel.link", "Product", mPrice, "USD", true);
		Debug.Log(success1 + "    " + PayPalAPI.Token + "    " + PayPalAPI.ReturnMessage);
		if (success1) Application.OpenURL(PayPalAPI.ReturnMessage + "&useraction=commit");
	}

	void GetPayPalDetails()
	{
		PayPalAPI.ReturnMessage = "";
		bool success2  = PayPalAPI.GetShippingDetails();
		Debug.Log(success2 + "    " + PayPalAPI.PayerID + "    " + PayPalAPI.ReturnMessage + "    " + PayPalAPI.Address);
	}

	void ConfirmPayPalPayment()
	{
		PayPalAPI.ReturnMessage = "";
		bool success3 = PayPalAPI.ConfirmPayment(mPrice, "USD");
		Debug.Log(success3 + "    " + PayPalAPI.ReturnMessage);
		if(success3)
		{
		}
		mPlayFabManager.OnConfirmPurchase ();
		mWaitingForConfirm = false;
	}
	public void CancelPayment()
	{
		mWaitingForConfirm = false;
	}

	public void ConfirmButtonPress()
	{
		GetPayPalDetails();
		ConfirmPayPalPayment();
	}
}
