﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class DCSelectionButton : MonoBehaviour, IPointerEnterHandler
{
	public Card mAssociatedCard;
	public DeckBuilder mDeckBuilder;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(mAssociatedCard!= null)
		{
			GetComponent<Image>().sprite = mAssociatedCard.mCardArt;

			if(mDeckBuilder!= null)
			{
				switch (mAssociatedCard.cardType)
				{
				case Card.CardType.Familiar:
					if(mDeckBuilder.mFamiliars.Contains(mAssociatedCard.gameObject) 
					   || mDeckBuilder.mPhase != DeckBuilder.CardTypePicking.Familiar)
					{
						GetComponent<Button>().interactable = false;
					}
					else
					{
						GetComponent<Button>().interactable = true;
					}
					break;
				case Card.CardType.Action:
					if(mDeckBuilder.mActionCards.Contains(mAssociatedCard.gameObject) 
					   || mDeckBuilder.mPhase != DeckBuilder.CardTypePicking.Familiar)
					{
						GetComponent<Button>().interactable = false;
					}
					else
					{
						GetComponent<Button>().interactable = true;
					}
					break;
				case Card.CardType.Minion:
					if(mDeckBuilder.mActionCards.Contains(mAssociatedCard.gameObject) 
					   || mDeckBuilder.mPhase != DeckBuilder.CardTypePicking.Familiar)
					{
						GetComponent<Button>().interactable = false;
					}
					else
					{
						GetComponent<Button>().interactable = true;
					}
					break;
				case Card.CardType.VoidBreacher:
					if(mDeckBuilder.mVoidBreacher.Contains(mAssociatedCard.gameObject) 
					   || mDeckBuilder.mPhase != DeckBuilder.CardTypePicking.VoidBreacher)
					{
						GetComponent<Button>().interactable = false;
					}
					else
					{
						GetComponent<Button>().interactable = true;
					}
					break;
				default:
					break;
				}
			}

		}
	}//END of Update()

	public void AddCardToDeck()
	{
		switch (mAssociatedCard.cardType)
		{
		case Card.CardType.Familiar:
			if(mDeckBuilder.mFamiliars.Count < 6)
			{
				mDeckBuilder.mFamiliars.Add (mAssociatedCard.gameObject);
				for(int i = 0; i < 6; i++)
				{
					if(!mDeckBuilder.mDCChosenCards[i].gameObject.activeInHierarchy)
					{
						mDeckBuilder.mDCChosenCards[i].mAssociatedCard = mAssociatedCard;
						mDeckBuilder.mDCChosenCards[i].gameObject.SetActive(true);
						break;
					}
				}
			}
			break;
		case Card.CardType.Action:
			if(mDeckBuilder.mActionCards.Count < 6)
			{
				mDeckBuilder.mActionCards.Add (mAssociatedCard.gameObject);
				for(int i = 6; i < 12; i++)
				{
					if(!mDeckBuilder.mDCChosenCards[i].gameObject.activeInHierarchy)
					{
						mDeckBuilder.mDCChosenCards[i].mAssociatedCard = mAssociatedCard;
						mDeckBuilder.mDCChosenCards[i].gameObject.SetActive(true);
						break;
					}
				}
			}
			break;
		case Card.CardType.Minion:
			if(mDeckBuilder.mActionCards.Count < 6)
			{
				mDeckBuilder.mActionCards.Add (mAssociatedCard.gameObject);
				for(int i = 6; i < 12; i++)
				{
					if(!mDeckBuilder.mDCChosenCards[i].gameObject.activeInHierarchy)
					{
						mDeckBuilder.mDCChosenCards[i].mAssociatedCard = mAssociatedCard;
						mDeckBuilder.mDCChosenCards[i].gameObject.SetActive(true);
						break;
					}
				}
			}
			break;
		case Card.CardType.VoidBreacher:
			if(mDeckBuilder.mVoidBreacher.Count == 0)
			{
				mDeckBuilder.mVoidBreacher.Add (mAssociatedCard.gameObject);
				mDeckBuilder.mDeckType = mAssociatedCard.cardDominion;
			}
			else
			{
				if(mDeckBuilder.mDeckType != mAssociatedCard.cardDominion)
				{
					mDeckBuilder.mFamiliars.Clear ();
					mDeckBuilder.mActionCards.Clear ();
					foreach(DCChosenCard chosenCard in mDeckBuilder.mDCChosenCards)
					{
						chosenCard.gameObject.SetActive(false);
					}
				}
				mDeckBuilder.mVoidBreacher.Clear();
				mDeckBuilder.mVoidBreacher.Add (mAssociatedCard.gameObject);
				mDeckBuilder.mDeckType = mAssociatedCard.cardDominion;
			}
			break;
		default:
			break;
		}

	}//END of AddCardToDeck()


	//Call this on highlighting the button
	public void OnPointerEnter(PointerEventData eventData)
	{
		if(GameObject.Find("CornerImage") != null)
		{
			GameObject.Find("CornerImage").GetComponent<Image>().sprite = mAssociatedCard.mCardArt;
		}
	}//END of OnPointerEnter()

}
