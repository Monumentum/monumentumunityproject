﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeckDisplayButton : MonoBehaviour 
{
	public PlayerHandManager mHandManager;
	public Card mAssociatedCard;
	public int mDeckNumber;
	[SerializeField] private Image mCardButtonArt;
	[SerializeField] private GameObject mOutOfHandOverlay; 
	[SerializeField] private Text mCardStateText;
	MouseOverInfoManager mMouseOverInfoManager;

	// Use this for initialization
	void Start () 
	{
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

	}//END of Start()
	
	// Update is called once per frame
	void Update () 
	{
		//Show whether the card or out of hand and what state it's in.  
		//This should also prevent the card from being clicked when out-of-hand ~Adam
		if(mAssociatedCard != null)
		{
			if(mAssociatedCard.cardState == Card.CardState.Hand)
			{
				mOutOfHandOverlay.SetActive(false);
				this.GetComponent<Button>().enabled = true;
			}
			else
			{
				mOutOfHandOverlay.SetActive (true);
				mCardStateText.text = mAssociatedCard.cardState.ToString();
				this.GetComponent<Button>().enabled = false;
			}
		}

		//This should prevent cards from being clicked/played outside your turn ~Adam
		if (mHandManager != null)
		{
			if(mHandManager.playerNumber != mHandManager.turnManager.playerTurn)
			{
				GetComponent<Button>().interactable = false;
			}
			else
			{
				GetComponent<Button>().interactable = true;
			}
		}
	}//END of Update


	public void AssignCard()
	{
		mCardButtonArt.sprite = mAssociatedCard.mCardArt;
	}//END of AssignCard()

	public void PlayCard()
	{
		if(mHandManager != null)
		{
			Debug.Log("Playing "+ mAssociatedCard.cardName + " from button " + this.name);
			mHandManager.PlayCardFromUIButton(mDeckNumber);
		}
		else
		{
			Debug.Log("Hand Manager not assigned to " + this.name);
		}
	}//END of PlayCard()

	public void UpdateCornerCard()
	{
		mMouseOverInfoManager.mCardImage = mCardButtonArt.sprite;
		mMouseOverInfoManager.mStrengthStatus = 0;
		mMouseOverInfoManager.mFortitudeStatus = 0;
		mMouseOverInfoManager.mRuneCardImages.Clear();
	}
}
