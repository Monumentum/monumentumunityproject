﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeckTrayToggle : MonoBehaviour 
{
	public ScrollRect mDeckScroller;
	public bool mDeckDisplayed = false;
	[SerializeField] private Text mToggleButtonText;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(mDeckDisplayed)
		{
			mToggleButtonText.text = "Hide Deck";
			mDeckScroller.normalizedPosition = Vector2.Lerp(mDeckScroller.normalizedPosition, new Vector2(0f,0.5f), 0.1f);
		}
		else
		{
			mToggleButtonText.text = "Show Deck";
			mDeckScroller.normalizedPosition = Vector2.Lerp(mDeckScroller.normalizedPosition, new Vector2(1f,0.5f), 0.1f);

		}
	}

	public void ToggleDeckDisplay()
	{
		mDeckDisplayed = !mDeckDisplayed;
	}
}
