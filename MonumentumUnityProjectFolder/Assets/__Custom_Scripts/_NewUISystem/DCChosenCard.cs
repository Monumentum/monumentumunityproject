﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DCChosenCard : MonoBehaviour, IPointerEnterHandler
{

	public Card mAssociatedCard;
	public DeckBuilder mDeckBuilder;
	public int mCardNumber;

	// Use this for initialization
	void Start () 
	{
		mDeckBuilder = FindObjectOfType<DeckBuilder>() as DeckBuilder;
		mDeckBuilder.mDCChosenCards[mCardNumber] = this;
		this.gameObject.SetActive(false);
	}//END of Start()
	
	// Update is called once per frame
	void Update () 
	{
		if(mAssociatedCard != null)
		{
			GetComponent<Image>().sprite = mAssociatedCard.mCardArt;
		}
		else
		{
			GetComponent<Image>().sprite = null;
		}
	}//END of Update()

	//Remove this card from the deck selection when clicked ~Adam
	public void RemoveFromDeck()
	{
		GameObject cardToRemove = new GameObject();
		//See if it was a familiar  ~Adam
		if(mAssociatedCard.cardType == Card.CardType.Familiar)
		{
			foreach(GameObject familiarCard in mDeckBuilder.mFamiliars)
			{
				if(familiarCard.GetComponent<Card>() == mAssociatedCard)
				{
					cardToRemove = familiarCard;
				}
			}
			if(cardToRemove != null)
			{
				mDeckBuilder.mFamiliars.Remove(cardToRemove);
			}
		}
		//See if this card was an action card ~Adam
		else if (mAssociatedCard.cardType == Card.CardType.Action || mAssociatedCard.cardType == Card.CardType.Minion)
		{
			foreach(GameObject actionCard in mDeckBuilder.mActionCards)
			{
				if(actionCard.GetComponent<Card>() == mAssociatedCard)
				{
					cardToRemove = actionCard;
				}
			}
			if(cardToRemove != null)
			{
				mDeckBuilder.mActionCards.Remove(cardToRemove);
			}
		}
		this.gameObject.SetActive(false);
	}//END of RemoveFromDeck()

	//Call this on highlighting the button
	public void OnPointerEnter(PointerEventData eventData)
	{
		if(GameObject.Find("CornerImage") != null)
		{
			GameObject.Find("CornerImage").GetComponent<Image>().sprite = mAssociatedCard.mCardArt;
		}
	}//END of OnPointerEnter()
}
