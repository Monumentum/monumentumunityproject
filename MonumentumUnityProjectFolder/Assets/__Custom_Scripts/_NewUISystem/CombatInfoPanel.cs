﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CombatInfoPanel : MonoBehaviour 
{
	TurnPhaseManager mTurnManager;

	[SerializeField] private GameObject mInfoPanel;
	[SerializeField] private Image mAttackerImage;
	[SerializeField] private Text mAttackerBase;
	[SerializeField] private Text mAttackerRoll;
	[SerializeField] private Image mDefenderImage;
	[SerializeField] private Text mDefenderBase;

	[SerializeField] private Texture2D mDefaultCardImage;

	public GameObject mAttacker;
	public GameObject mDefender;
	

	// Use this for initialization
	void Start () 
	{
		mTurnManager = FindObjectOfType<TurnPhaseManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(mTurnManager != null)
		{
			if(mTurnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
			{
				mInfoPanel.SetActive(true);
			}
			else
			{
				mInfoPanel.SetActive(false);
			}
		}
		else
		{
			mTurnManager = FindObjectOfType<TurnPhaseManager>();
		}
	}
}
