﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;

namespace Assets.CustomScripts
{
    public class NetworkManager : MonoBehaviour
    {

        private const string typeName = "Monumentum";
        private const string gameName = "Monumentum Room";

        public GameObject mPlayerPrefab;
        public GameObject mVoidBreacher;
        public bool mMapRandomized = false;
        public bool mShowLobby = false;

        private HostData[] hostList;

        public int mCorridorTilePosition;
        public int mJunctionTilePosition;
        public float[] mTileRotations = new float[14];
     //   public bool mInLobby = true;

        public InformationManager mInfoManager;

		//For keeping track of what scene we are currently in
		public enum NetworkManagerScene{Lobby, Matchup, Game}; 
		public NetworkManagerScene mNetManCurrentScene;

		//For instantiating the room manager in the Matchup scene
		[SerializeField] private GameObject mMatchupManager;
		public bool mFoundMatchupManager = false;

		//For making sure both players are ready before starting
		public bool mReadyToStartMatch = false;


		//For returning the player to the lobby when the game disconnects
		private bool mGotDisconnected = false;

		private bool mReturningToLobby = false;

        // Use this for initialization
        void Start()
        {
			mReadyToStartMatch = false;

			if (mInfoManager == null)
			{
				mInfoManager = FindObjectOfType(typeof(InformationManager)) as InformationManager;
			}
        }

        // Update is called once per frame
        void Update()
        {
			if (Application.loadedLevelName == "CardPurchaseMenu")
			{
				Destroy(transform.gameObject);
			}

			//Check if the player's been disconnected mid-game
			if( Network.connections.Length == 0 && (Application.loadedLevelName == "GridScene"))// || Application.loadedLevelName == "Matchup") )
			{
				Time.timeScale = 0;
				mGotDisconnected = true;
			}
			else
			{
				Time.timeScale = 1;
				mGotDisconnected = false;
			}

        }

        void Awake()
        {
			DontDestroyOnLoad(transform.gameObject);
			mReadyToStartMatch = false;


        }

        private void StartServer()
        {
            //Network.InitializeServer(4, 25000, !Network.HavePublicAddress());
            Network.InitializeServer(2, 26543, !Network.HavePublicAddress());
            MasterServer.RegisterHost(typeName, gameName);
        }

        void OnServerInitialized()
        {
            //mInLobby = false;
			mNetManCurrentScene = NetworkManagerScene.Matchup;
            Debug.Log("Server Initializied");
           // Network.isMessageQueueRunning = false;
            Application.LoadLevel("Matchup");
            //SpawnPlayer();
        }

        private void SpawnPlayer()
        {
            Network.Instantiate(mPlayerPrefab, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        }

        void OnGUI()
        {
			if (mShowLobby && !mGotDisconnected)
			{
				if (!Network.isClient && !Network.isServer)
                {
					if (GUI.Button(new Rect(Screen.width * 0.775f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Start Server")))
					{
                        StartServer();
                    }


					if (GUI.Button(new Rect(Screen.width * 0.875f, Screen.height * 0.475f, Screen.width * 0.095f, Screen.height * 0.05f), new GUIContent("Refresh Hosts")))
					{
                        RefreshHostList();
                    }

                    if (hostList != null)
                    {
                        for (int i = 0; i < hostList.Length; i++)
                        {
							if (GUI.Button(new Rect(Screen.width*0.4f, Screen.height*0.2f + (Screen.height*0.06f * i), Screen.width*0.2f, Screen.height*0.05f), hostList[i].gameName))
							{
                                JoinServer(hostList[i]);
							}
                        }
                    }


                }

            }

			else if (mGotDisconnected)
			{
				GUI.Box(new Rect(0,0,Screen.width,Screen.height), "");
				GUI.Box(new Rect(Screen.width*0.35f, Screen.height*0.15f + (Screen.height*0.06f), Screen.width*0.3f, Screen.height*0.15f), "The other player has disconnected.");

				if (GUI.Button(new Rect(Screen.width*0.4f, Screen.height*0.2f + (Screen.height*0.06f), Screen.width*0.2f, Screen.height*0.05f), "Return to Lobby"))
				{
					Time.timeScale = 1;
					ReturnToLobby();
				}
			}
			
			
		}
		
		private void RefreshHostList()
        {
            MasterServer.RequestHostList(typeName);
        }

        void OnMasterServerEvent(MasterServerEvent msEvent)
        {
            if (msEvent == MasterServerEvent.HostListReceived)
                hostList = MasterServer.PollHostList();
        }

        private void JoinServer(HostData hostData)
        {
            Network.Connect(hostData);
        }

        void OnConnectedToServer()
        {
           // mInLobby = false;
			mNetManCurrentScene = NetworkManagerScene.Matchup;
			Debug.Log("Server Joined");
          //  Network.isMessageQueueRunning = false;
			Application.LoadLevel("Matchup");
            //SpawnPlayer();
        }

		public void StartMatch()
		{
			MatchupRoomManager roomMan = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
			if (roomMan != null)
			{
//				if(Network.isServer && GetComponent<CustomEventLogger>() != null)
//				{
//					//Log a custom event saying what two dominions/VBs are being used
//					Dictionary<string,object> eventBody = new Dictionary<string,object>();
//
//					eventBody.Add("P1VB", roomMan.mSelectedVoidBreacherNames[0]);
//					eventBody.Add("P2VB", roomMan.mSelectedVoidBreacherNames[1]);
//
//					GetComponent<CustomEventLogger>().LogCustomEvent("New Match Data", eventBody);
//					Debug.Log("Logging match VB data");
//				}
				if(GetComponent<CustomEventLogger>() != null)
				{
					Debug.Log(Application.loadedLevelName + " is the current scene.");
					if(FindObjectOfType<DeckBuilder>() != null)
					{
						Dictionary<string,int> stats = new Dictionary<string,int> ();
						switch (FindObjectOfType<DeckBuilder>().mDeckType)
						{
						case Card.CardDominion.Irotar:
							stats.Add("Irotar Chosen", 1);
							GetComponent<CustomEventLogger>().storeStats(stats);
							break;
						case Card.CardDominion.Rhavik:
							stats.Add("Rhavik Chosen", 1);
							GetComponent<CustomEventLogger>().storeStats(stats);
							break;
						case Card.CardDominion.Vosira:
							stats.Add("Vosira Chosen", 1);
							GetComponent<CustomEventLogger>().storeStats(stats);
							break;
						default:
							break;
						}
						Debug.Log("Logging match VB data");

					}
					else
					{
						Debug.Log("Couldn't find Deck Builder");
					}
				}
				else
				{
					Debug.Log("Couldn't find event logger.");
				}
				Destroy(roomMan.gameObject);
			}
			else
			{
				Debug.Log("Couldn't find Room Manager");
			}

			mNetManCurrentScene = NetworkManagerScene.Game;



			Time.timeScale = 1;
			Application.LoadLevel("GridScene");

		}

        void OnLevelWasLoaded()
        {
			mFoundMatchupManager = false;

			if (mNetManCurrentScene == NetworkManagerScene.Matchup)
			{
				Network.isMessageQueueRunning = true;

				if (Network.isClient)
				{
					mFoundMatchupManager = true;
					GameObject roomManager = (GameObject)Network.Instantiate(mMatchupManager, transform.position, transform.rotation, 0);
					Debug.Log("Instantiating matchup manager " + roomManager.GetComponent<NetworkView>().viewID.ToString());
//					NetworkViewID roomManagerViewID = Network.AllocateViewID();
//					//roomManagerViewID = Network.AllocateViewID();
//					Debug.Log("Room Manger ID: " + roomManagerViewID.ToString());
//					roomManager.networkView.viewID = roomManagerViewID;
					roomManager.GetComponent<NetworkView>().RPC("UpdatePlayerCount", RPCMode.AllBuffered);

					DeckBuilder myDeck = GameObject.Find("DeckBuilder").GetComponent<DeckBuilder>();
					myDeck.CallToUpdateVoidBreacherNames();

				}

			}

			if (mNetManCurrentScene == NetworkManagerScene.Game)
            {
                Network.isMessageQueueRunning = true;
                mInfoManager.LoadDeck();
                SpawnPlayer();
            }
        }

		//For finding the Matchup Room Manager as the Server
		public void FindMatchupRoomManager()
		{
			if(!mFoundMatchupManager)
			{
				MatchupRoomManager roomManager = FindObjectOfType(typeof(MatchupRoomManager)) as MatchupRoomManager;
				roomManager.GetComponent<NetworkView>().RPC("UpdatePlayerCount", RPCMode.All);
				
				DeckBuilder myDeck = GameObject.Find("DeckBuilder").GetComponent<DeckBuilder>();
				myDeck.CallToUpdateVoidBreacherNames();
				mFoundMatchupManager = true;
			}
		}

        public void RandomizeLocations()
        {
            mCorridorTilePosition = Mathf.RoundToInt(Random.Range(0f, 13f));
            mJunctionTilePosition = Mathf.RoundToInt(Random.Range(0f, 13f));

            while (mCorridorTilePosition == mJunctionTilePosition)
            {
                mJunctionTilePosition = Mathf.RoundToInt(Random.Range(0f, 14f));
            }

            for (int i = 0; i < mTileRotations.Length; i++)
            {
                mTileRotations[i] = Mathf.RoundToInt(Random.Range(0f, 3f)) * 90f;
            }

        }

        //[RPC]
        //void LoadLevel()
        //{
        //    Application.LoadLevel("GridScene");
        //}

        [RPC]
        void SetMapSettings(int corridor, int junction, float[] tileRotations)
        {
            this.mCorridorTilePosition = corridor;
            this.mJunctionTilePosition = junction;
            for (int i = 0; i < mTileRotations.Length; i++)
            {
                mTileRotations[i] = tileRotations[i];
            }
            mMapRandomized = true;
        }

        [RPC]
        public void ReturnToLobby()
        {
			if(!mReturningToLobby)
			{
				mReturningToLobby = true;
	           // mInLobby = true;
				mNetManCurrentScene = NetworkManagerScene.Lobby;

	            mShowLobby = false;
				if (Network.isServer)
				{
					GetComponent<NetworkView>().RPC("GoToLobby", RPCMode.Others);
				}
	            Network.Disconnect();
	            MasterServer.UnregisterHost();
	            MasterServer.ClearHostList();
				Network.isMessageQueueRunning = false;
	            Application.LoadLevel("Lobby");
				Destroy(this.gameObject);
	            //if (Network.isClient)
	            //{
	            //    Application.LoadLevel("Lobby");
	            //    Network.CloseConnection(Network.connections[0], true);
	            //}
	            //else if(Network.isServer)
	            //{
	            //    foreach (NetworkPlayer player in Network.connections)
	            //    {
	            //        Network.CloseConnection(player, true);
	            //        Application.LoadLevel("Lobby");
	            //    }
	            //}
			}
        }

        [RPC]
        public void GoToLobby()
        {
           // mInLobby = true;
			mNetManCurrentScene = NetworkManagerScene.Lobby;

            mShowLobby = false;
            mInfoManager.mDeck.Clear();
            Network.Disconnect();
            MasterServer.UnregisterHost();
            MasterServer.ClearHostList();
            Application.LoadLevel("Lobby");
			Destroy(this.gameObject);

        }

        void OnDisconnectedFromServer(NetworkDisconnection info)
        {
            //if (Network.isServer)
            //{
            //    Debug.Log("Local server connection disconnected");
            //}
            //else
            //{
            //    if (info == NetworkDisconnection.LostConnection)
            //    {
            //        Debug.Log("Lost connection to the server");
            //    } 
            //    else
            //    {
            //        Debug.Log("Successfully diconnected from the server");
            //    }
            //    GoToLobby(); 
            //}
                
        }
    }

}
