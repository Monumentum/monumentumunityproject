﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MatchupRoomManager : MonoBehaviour 
{
	public int mPlayerCount = 0;
	public int mReadyPlayers = 0;

	public List<string> mSelectedVoidBreacherNames = new List<string>();
	public List<Card> mPossibleVBs = new List<Card>(); //This list will need updated on the MatchupRoomManager prefab whenever a new VoidBreacher card is made

//	public NetworkView nv;
	// Use this for initialization
	void Start () 
	{
		//find any network managers that have not found this object yet
		Assets.CustomScripts.NetworkManager[] networkManagers = FindObjectsOfType(typeof(Assets.CustomScripts.NetworkManager)) as Assets.CustomScripts.NetworkManager[];
		foreach(Assets.CustomScripts.NetworkManager netman in networkManagers)
		{
			netman.FindMatchupRoomManager();
		}
	}
	
	void OnLevelWasLoaded()
	{
		//nv = gameObject.AddComponent("NetworkView") as NetworkView;

	}
	// Update is called once per frame
	void Update () 
	{
		if(GameObject.Find("OpponentFindingStatus") != null)
		{
			GameObject.Find("OpponentFindingStatus").GetComponent<Text>().text = "Opponent Found!";
		}
		if(Network.connections.Length == 0)
		{
			GameObject.Find("OpponentFindingStatus").GetComponent<Text>().text = "Opponent Disconnected...";
		}

		if(mReadyPlayers == mPlayerCount && mReadyPlayers != 0)
		{
			GameObject.Find("NetworkManager").GetComponent<Assets.CustomScripts.NetworkManager>().StartMatch();
		}
		//Update the images for the Void Breacher Vs. cards -Adam
		foreach(Card voidBreacher in mPossibleVBs)
		{

			if(voidBreacher.cardName == mSelectedVoidBreacherNames[0])
			{
				GameObject.Find("P1VB").GetComponent<Image>().sprite = voidBreacher.mCardArt;
			}
			if(voidBreacher.cardName == mSelectedVoidBreacherNames[1])
			{
				GameObject.Find("P2VB").GetComponent<Image>().sprite = voidBreacher.mCardArt;
			}
		}
	}


	[RPC]
	public void UpdatePlayerCount()
	{
		mPlayerCount++;
	}

	[RPC]
	public void UpdateReadyCount()
	{
		mReadyPlayers++;
		Debug.Log("Ready Players: " + mReadyPlayers);
	}


	[RPC]
	public void UpdateVoidBreacherNames(string tempName)
	{
		mSelectedVoidBreacherNames.Insert(0,tempName);
	}

}
