﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneofBloodlust : Rune 
{
    public int mBoostAmount = 1;

	// Use this for initialization
	public override void Start () 
    {
        base.Start();
        mType = Rune.RuneType.Attacking;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void ActivateRune(Creature targetCreature)
    {
        //base.ActivateRune();
        if (targetCreature.GetComponent<NetworkView>().isMine)
        {
            targetCreature.ModifyStrength(mBoostAmount);
            targetCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, targetCreature.strength, targetCreature.fortitude);
        }
        
    }
}
