﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Rune: During your conclusion phase, this character causes a 4S attack to all enemey characters within AOE(1).

public class RuneofBoilingBlood : Rune
{

	// Use this for initialization
	void Start () 
	{
		base.Start();
		mType = Rune.RuneType.Conclusion;

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public override void ActivateRune(Creature targetCreature)
	{
		Debug.Log("Attacking With Rune of Boiling Blood");
		//base.ActivateRune();
		foreach(BoardSquare aoeSquare in targetCreature.currentSpace.GetComponent<BoardSquare>().neighborSquares)
		{
			if (aoeSquare.occupier && aoeSquare.occupier.GetComponent<Creature>() != null)
			{
				if (aoeSquare.occupier.GetComponent<Creature>().controllingPlayer != mEquippedCreature.controllingPlayer)
				{
					Creature targetedCreature = aoeSquare.occupier.GetComponent<Creature>();
					
					//Roll 2d6 and add your Strength
					int thisAttackPower = 4;
					int rollBonus = 0;
					
					for (int i = 0; i < 2; i++)
					{
						int dieRoll = 0;
						dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
						dieRoll += Mathf.RoundToInt(Random.Range(1f, 6f));
						
						if (dieRoll > rollBonus)
						{
							rollBonus = dieRoll;
						}
					}
					thisAttackPower += rollBonus;
					
					//Compare your results to the target's Fortitude
					if (targetedCreature.tag == "VoidBreacher")//Automatically hit a VoidBreacher
					{
						//do nothing to a VoidBreacher
					}
					else if (thisAttackPower == 4+2)//Automatically miss if you roll a 1 on both dice
					{
						Debug.Log(this.name + " missed " + targetedCreature.name);
						targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, false);
					}
					else if (thisAttackPower > targetedCreature.fortitude || thisAttackPower==4+12) //hit if you roll a 6 on both dice and/or roll higher than the target's Fortitude
					{
						targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, true);
						Debug.Log(this.name + " hit " + targetedCreature.name + " leaving it with " + targetedCreature.hitPoints + " HP");
					}
					else//Miss if you roll lower than the target's Fortitude
					{
						Debug.Log(this.name + " missed " + targetedCreature.name);
						targetedCreature.GetComponent<NetworkView>().RPC("TakeDamage", RPCMode.All, GetComponent<NetworkView>().viewID, false);
					}
					
					//Done attacking now

					
					
					//if you took the target down to 0 HP, kill it and move into its space if it was adjacent
					if (targetedCreature.hitPoints == 0)
					{
						targetedCreature.GetComponent<NetworkView>().RPC("BeKilled", RPCMode.All); //kill the target
					}
				}
			}
		}
	}
}
