﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Rune: This character may be used as a summoning stone. If this character is used as a summoning stone, this character is destroyed.

public class RuneofCharredAsh : Rune
{
	string mOriginalTag="Familiar";
	// Use this for initialization
	void Start ()
	{
		base.Start();
		mType = Rune.RuneType.Summoning;
	}

	
	// Update is called once per frame
	void Update () 
	{
		if (mTurnManager && mEquippedCreature && mTurnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning && mEquippedCreature.tag != "VoidBreacher")
		{
			mOriginalTag = mEquippedCreature.tag;
			mEquippedCreature.tag="VoidBreacher";
		}
		else if (mTurnManager && mEquippedCreature && mTurnManager.currentPhase != TurnPhaseManager.TurnPhase.Summoning && mEquippedCreature.tag == "VoidBreacher")
		{
			mEquippedCreature.tag = mOriginalTag;
		}
	}


	public override void ActivateRune(Creature targetCreature)
	{
		//base.ActivateRune();
		//Destroy the equipped creature if they just summoned a familiar (as opposed to a minion)
		if (targetCreature.carriedLimboCreatures[targetCreature.limboCreatureLookingAt].tag == "Familiar")
		{
			targetCreature.BeKilled();
		}
	}


}
