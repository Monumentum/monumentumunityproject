﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneoftheFurious : Rune 
{

    public int mExtraAttacksGiven = 1;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        mType = Rune.RuneType.OnTrigger;
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void ActivateRune(Creature targetCreature)
    {
        //base.ActivateRune();
        targetCreature.mMaxAttacksATurn = targetCreature.mMaxAttacksATurn + mExtraAttacksGiven;
		targetCreature.mNumberOfAttacksLeft += mExtraAttacksGiven;

    }

    [RPC]
    public override void EquipRune(NetworkViewID creatureID)
    {
        base.EquipRune(creatureID);
        ActivateRune(mEquippedCreature);
    }

    [RPC]
    public override void DestroyRune()
    {
        mEquippedCreature.mMaxAttacksATurn = mEquippedCreature.mMaxAttacksATurn - mExtraAttacksGiven;
        base.DestroyRune();
    }
}
