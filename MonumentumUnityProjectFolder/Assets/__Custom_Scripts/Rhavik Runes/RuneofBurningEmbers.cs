﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RuneofBurningEmbers : Rune
{
    public int mDecreaseAmount = 1;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        mType = Rune.RuneType.IsAttacked;
    }
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    public override void ActivateRune(Creature targetCreature)
    {
        //base.ActivateRune();
        targetCreature.ModifyFortitude(-mDecreaseAmount);
        targetCreature.GetComponent<NetworkView>().RPC("SendStrengthandFortitude", RPCMode.Others, targetCreature.strength, targetCreature.fortitude);
    }
}
