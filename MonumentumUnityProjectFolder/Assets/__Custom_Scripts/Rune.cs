﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rune : MonoBehaviour 
{

    public enum RuneType { Attacking, Conclusion, IsAttacked, Summoning, OnTrigger, IsDamaged };

    public RuneType mType;
    public Creature mEquippedCreature;
	public Card mAssociatedCard;
	public TurnPhaseManager mTurnManager;
	// Use this for initialization
	public virtual void Start () 
    {
		mTurnManager = GameObject.FindObjectOfType(typeof(TurnPhaseManager)) as TurnPhaseManager;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public virtual void ActivateRune(Creature targetCreature)
    {

    }

    [RPC] public virtual void EquipRune(NetworkViewID creatureID)
    {
        mEquippedCreature = NetworkView.Find(creatureID).observed.GetComponent<Creature>();
        List<Rune> runes;
        if (mEquippedCreature.mActiveRunes.TryGetValue(mType, out runes))
        {
            if (!runes.Contains(this))
            {
                runes.Add(this);
				Debug.Log("Updating Rune Count");
				mEquippedCreature.GetComponent<NetworkView>().RPC("AddRuneCount", RPCMode.All, 1);
			}
        }
    }

    [RPC] public virtual void DestroyRune()
    {
		Debug.Log(name + " destroyed.");
        //put in code to move card to cooldown pile
		mAssociatedCard.cardState = Card.CardState.Cooldown;

        Destroy(this.gameObject);
    }

    [RPC] public virtual void RemoveRune()
    {
        List<Rune> runes;
        if (mEquippedCreature.mActiveRunes.TryGetValue(mType, out runes))
        {
            runes.Remove(this);
            mEquippedCreature.mNumberOfRunes--;
        }
    }
}
