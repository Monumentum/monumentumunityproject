﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

public class BoardSquare : MonoBehaviour 
{
	[SerializeField] private bool shouldExist = false;
	public List<BoardSquare>  neighborSquares;
	public GameObject occupier;
	public bool inMovementRange = false;
	public bool hasAttackableTarget = false;
	public bool validSummonSpot = false;
	public bool voidBreacherStartSpot = false;
	public int playerStartSpot = 0;
	public int mDistanceFromMover = 99;

	//Fore changing colors when highlighted by the mouse ~Adam
	private Color mDefaultColor;
	private bool mMousedOver = false;

	private TurnPhaseManager turnManager;
	// Use this for initialization
	void Start () 
	{
		turnManager = FindObjectOfType<TurnPhaseManager>();
		mDefaultColor = Color.white;
		GetComponent<Renderer>().material.color = mDefaultColor;
		mDefaultColor = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Change HighlightColors
		if(!mMousedOver)
		{
			if (inMovementRange)
			{
				GetComponent<Renderer>().material.color = Color.green;
				GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
			}
			else if (hasAttackableTarget)
			{
				GetComponent<Renderer>().material.color = Color.red;
				GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
			}
			else if (validSummonSpot)
			{
				GetComponent<Renderer>().material.color = Color.green;
				GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
			}
			else if (voidBreacherStartSpot && turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation && turnManager.playerTurn == playerStartSpot)
			{
				GetComponent<Renderer>().material.color = Color.green;
				GetComponent<Renderer>().material.color = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
			}
			else
			{
				GetComponent<Renderer>().material.color = mDefaultColor;
			}
		}
		else
		{
			GetComponent<Renderer>().material.color = mDefaultColor;
		}
	}//End of Update()

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "BoardInitializer")
		{
			shouldExist = true;

		}
		if (other.GetComponent<Creature>() != null && occupier == null)
		{
			other.GetComponent<Creature>().currentSpace = this.gameObject;
			occupier = other.gameObject;
		}
		if (other.GetComponent<Trap>() != null)
		{
			other.GetComponent<Trap>().mOccupiedSquare = this;
		}

	}//End of OnTriggerEnter()
	void OnTriggerExit (Collider other)
	{
		if (other.GetComponent<Creature>() != null && other.gameObject == occupier)
		{
			occupier = null;
		}

	}//End of OnTriggerExit

	public void DeleteIfUnused ()
	{
		if (!shouldExist)
		{
			Destroy(this.gameObject);
		}
	}//End of DeleteIfUnused()
	public void FindNeighbors ()
	{
		BoardSquare[] adjacentSquares = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare square in adjacentSquares)
		{
			if (Vector3.Distance(transform.position, square.transform.position) == 1f)
				neighborSquares.Add(square);
		}
	}//End of FindNeighbors()



	void OnMouseEnter()
	{
		mMousedOver = true;
		mDefaultColor = Color.blue;
		GetComponent<Renderer>().material.color = mDefaultColor;
		mDefaultColor = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
	}

	void OnMouseExit()
	{
		mMousedOver = false;
		mDefaultColor = Color.white;
		GetComponent<Renderer>().material.color = mDefaultColor;
		mDefaultColor = new Vector4 (GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.b, GetComponent<Renderer>().material.color.g, 0.3f);
	}
}
