﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MouseOverInfoManager : MonoBehaviour 
{
	//for creature tooltip info
	public bool mShowCreatureInfoTooltip = false;
	public string mCreatureTooltipInfo = "";

	public bool mShowMonumentInfoTooltip = false;
	public string mMonumentTooltipInfo = "";

	public bool mShowTrapInfoTooltip = false;
	public string mTrapTooltipInfo = "";

	public GUIStyle mMouseOverStyle;
	public GUIStyle mCardDisplayStyle;

	//The image of the card we display
	public Sprite mCardImage;
	public List<Sprite> mRuneCardImages = new List<Sprite>();

	bool mExpandCardImage = false;
	//The images we display to overlay to show changes in Strength/Fortitude, there should be two items in each of these arrays
	[SerializeField] private Sprite[] mStrengthOverlay;
	[SerializeField] private Sprite[] mFortitudeOverlay;
	//Ints telling us to show a bonus, penalty, or default to Strength/Fortitude
	public int mStrengthStatus = 0;
	public int mFortitudeStatus = 0;
	//Ints for what the creature's current Strength/Fortitude is
	public int mCurrentStrength = 0;
	public int mCurrentFortitude = 0;

	//For the new UI system
	[SerializeField] private RectTransform mCornerCardPanel;
	[SerializeField] private Image mUICard;
	[SerializeField] private Image mUIStrengthOverlay;
	[SerializeField] private Image mUIFortOverlay;
	[SerializeField] private Text mUIStrengthAmount;
	[SerializeField] private Text mUIFortAmount;
	[SerializeField] private Image[] mUIRuneImages;
	[SerializeField] private Text mUIToggleSizeInstructions;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//mTooltipXPos = Input.mousePosition.x)
		if(Input.GetKeyDown("tab"))
		{
			mExpandCardImage = !mExpandCardImage;
		}

		//Updating corner card based on new UI system
		if(mExpandCardImage)
		{
			mCornerCardPanel.localScale = Vector3.Lerp(mCornerCardPanel.localScale, new Vector3(1f,1f,1f), 0.1f);
			mUIToggleSizeInstructions.text = "Tab: Shrink Image";
		}
		else
		{
			mCornerCardPanel.localScale = Vector3.Lerp(mCornerCardPanel.localScale, new Vector3(0.5f,0.5f,1f), 0.1f);
			mUIToggleSizeInstructions.text = "Tab: Enlarge Image";
		}
		//Display the card image for the last moused-over thing
		mUICard.sprite = mCardImage;
		if(mUICard.sprite == null)
		{
			mUICard.enabled = false;
		}
		else
		{
			mUICard.enabled = true;
		}

		//Display Strength overlays
		switch(mStrengthStatus)
		{
		case 0:
			mUIStrengthOverlay.sprite = null;
			mUIStrengthOverlay.enabled = false;
			mUIStrengthAmount.text = "";
			break;
		case 1:
			mUIStrengthOverlay.enabled = true;
			mUIStrengthOverlay.sprite = mStrengthOverlay[0];
			mUIStrengthAmount.text = mCurrentStrength.ToString();
			break;
		case 2:
			mUIStrengthOverlay.enabled = true;
			mUIStrengthOverlay.sprite = mStrengthOverlay[1];
			mUIStrengthAmount.text = mCurrentStrength.ToString();
			break;
		default:
			break;
		}
		//Display Fortitude overlays
		switch(mFortitudeStatus)
		{
		case 0:
			mUIFortOverlay.sprite = null;
			mUIFortOverlay.enabled = false;
			mUIFortAmount.text = "";
			break;
		case 1:
			mUIFortOverlay.enabled = true;
			mUIFortOverlay.sprite = mStrengthOverlay[0];
			mUIFortAmount.text = mCurrentStrength.ToString();
			break;
		case 2:
			mUIFortOverlay.enabled = true;
			mUIFortOverlay.sprite = mStrengthOverlay[1];
			mUIFortAmount.text = mCurrentStrength.ToString();
			break;
		default:
			break;
		}
		//Display runes on the thing being displayed
		for(int i = 0; i < mUIRuneImages.Length; i++)
		{
			if(mRuneCardImages.Count > i && mRuneCardImages[i] != null)
			{
				mUIRuneImages[i].gameObject.SetActive(true);
				mUIRuneImages[i].sprite = mRuneCardImages[i];
			}
			else
			{
				mUIRuneImages[i].gameObject.SetActive(false);
				mUIRuneImages[i].sprite = null;
			}
		}


	}

	void OnGUI ()
	{
		if (mShowCreatureInfoTooltip)
		{
			GUI.Box(new Rect(Input.mousePosition.x+Screen.width*0.01f, Screen.height-Input.mousePosition.y, Screen.width*0.2f, Screen.height*0.2f), mCreatureTooltipInfo, mMouseOverStyle);
		}
		if (mShowMonumentInfoTooltip)
		{
			GUI.Box(new Rect(Input.mousePosition.x+Screen.width*0.01f, Screen.height-Input.mousePosition.y, Screen.width*0.2f, Screen.height*0.2f), mMonumentTooltipInfo, mMouseOverStyle);
		}
		if (mShowTrapInfoTooltip)
		{
			GUI.Box(new Rect(Input.mousePosition.x+Screen.width*0.01f, Screen.height-Input.mousePosition.y, Screen.width*0.2f, Screen.height*0.2f), mTrapTooltipInfo, mMouseOverStyle);
		}

//		//Display card text for runes on the current creature (if any)
//		if(mRuneCardImages.Count > 0)
//		{
//			for(int i = 0; i < mRuneCardImages.Count; i++)
//			{
//				if(!mExpandCardImage)
//				{
//					GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f+(Screen.width*0.15f*(mRuneCardImages.Count-i)*0.3f),Screen.width*0.1f,Screen.width*0.15f), mRuneCardImages[i].texture);
//				}
//				else
//				{
//					GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f+(Screen.width*0.2f*(mRuneCardImages.Count-i)*0.45f),Screen.width*0.2f,Screen.width*0.3f), mRuneCardImages[i].texture);
//				}
//			}
//		}
//		//Display the card of the last thing you had your mouse over in the top left corner of the screen in one of two possible sizes
//		if(mCardImage != null)
//		{
//			if(!mExpandCardImage)
//			{
//				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mCardImage.texture);
//				GUI.Box(new Rect(Screen.width*0.89f,Screen.height*.01f+Screen.width*0.15f+(Screen.width*0.15f*(mRuneCardImages.Count)*0.3f), Screen.width*0.1f, Screen.height*0.05f), "TAB: Enlarge");
//				DisplayCardOverlays();
//			}
//			else
//			{
//				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mCardImage.texture);
//				GUI.Box(new Rect(Screen.width*0.69f,Screen.height*.01f+Screen.width*0.3f+(Screen.width*0.2f*(mRuneCardImages.Count)*0.45f), Screen.width*0.2f, Screen.height*0.05f), "TAB: Minimize");
//				DisplayCardOverlays();
//			}
//		}

	}

	public void SetCreatureToolTip(bool shouldShow, string infoToShow)
	{
		mShowCreatureInfoTooltip = shouldShow;
		mCreatureTooltipInfo = infoToShow;
	}

	public void SetMonumentToolTip(bool shouldShow, string infoToShow)
	{
		mShowMonumentInfoTooltip = shouldShow;
		mMonumentTooltipInfo = infoToShow;
	}

	public void SetTrapToolTip(bool shouldShow, string infoToShow)
	{
		mShowTrapInfoTooltip = shouldShow;
		mTrapTooltipInfo = infoToShow;
	}

	void DisplayCardOverlays()
	{
		//Small card overlays
		if(!mExpandCardImage)
		{
			mCardDisplayStyle.fontSize = Mathf.RoundToInt(Screen.width*0.01f);
			switch(mStrengthStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mStrengthOverlay[0].texture);
				GUI.Label(new Rect(Screen.width*0.9075f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mStrengthOverlay[1].texture);
				GUI.Label(new Rect(Screen.width*0.9075f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
			
			switch(mFortitudeStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mFortitudeOverlay[0].texture);
				GUI.Label(new Rect(Screen.width*0.965f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.01f,Screen.width*0.1f,Screen.width*0.15f), mFortitudeOverlay[1].texture);
				GUI.Label(new Rect(Screen.width*0.965f,Screen.width*0.1275f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
		}
		//Expanded card overlays
		else
		{
			mCardDisplayStyle.fontSize = Mathf.RoundToInt(Screen.width*0.015f);
			switch(mStrengthStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mStrengthOverlay[0].texture);
				GUI.Label(new Rect(Screen.width*0.725f,Screen.width*0.251f,Screen.width*0.01f,Screen.width*0.01f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mStrengthOverlay[1].texture);
				GUI.Label(new Rect(Screen.width*0.725f,Screen.width*0.251f,Screen.width*0.01f,Screen.width*0.01f), mCurrentStrength.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
			
			switch(mFortitudeStatus)
			{
			case 1:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mFortitudeOverlay[0].texture);
				GUI.Label(new Rect(Screen.width*0.844f,Screen.width*0.251f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			case 2:
				GUI.Label(new Rect(Screen.width*0.69f,Screen.height*0.01f,Screen.width*0.2f,Screen.width*0.3f), mFortitudeOverlay[1].texture);
				GUI.Label(new Rect(Screen.width*0.844f,Screen.width*0.251f,Screen.width*0.1f,Screen.width*0.1f), mCurrentFortitude.ToString(), mCardDisplayStyle);
				break;
			default:
				break;
			}
		}

	}//END of DisplayCardOverlays()

}
