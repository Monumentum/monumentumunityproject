﻿using UnityEngine;
using System.Collections;

namespace Assets.CustomScripts
{
    static class BoardLocations
    {
        public static int mCorridorTilePosition;
        public static int mJunctionTilePosition;
        public static float[] mTileRotations = new float[14];
        
        
        public static void RandomizeLocations()
        {
            mCorridorTilePosition = Mathf.RoundToInt(Random.Range(0f, 13f));
            mJunctionTilePosition = Mathf.RoundToInt(Random.Range(0f, 13f));

            Debug.Log(mCorridorTilePosition);
            Debug.Log(mJunctionTilePosition);

            while (mCorridorTilePosition == mJunctionTilePosition)
            {
                mJunctionTilePosition = Mathf.RoundToInt(Random.Range(0f, 14f));
            }

            for (int i = 0; i < mTileRotations.Length; i++)
            {
                mTileRotations[i] = Mathf.RoundToInt(Random.Range(0f, 3f)) * 90f;
            }
            
        }

    }
}

