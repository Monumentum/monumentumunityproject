﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script, along with PlayerHandManager serve as the two primary components of the PlayerController prefab
//This script handles all the input from the player clicking on non-GUI objects in the scene, 
		//from summoning creatures, to telling them to move, to declaring targets for attacks or card effects

public class PlayerClickController : MonoBehaviour 
{
	public int playerNumber;//which player this is
	public Creature selectedCreature;
	public Creature mVoidBreacher;//This player's VoidBreacher (like a main commander unit)
	public Creature mLastSelectedCreature;
	TurnPhaseManager turnManager;
	[HideInInspector] public PlayerHandManager handManager;
	int selectedCardInHand;
	public int sourceStoneCount = 12;//Resource used for playing cards/summoning creatures
	public bool mouseOverGUI = false;//Whether or not the mouse is hovering over a GUI element and should display tooltip text
	public int mRemainingMonuments = 3;
	public bool mGameOver = false;
	public int mLosingPlayer = 0;
	private List<Creature> mSwappingCreatures = new List<Creature>();//For a type of effect that swaps the position of two creatures on the board
	public bool mCanSwap = false;
	public BoardSquare mClickedSquare;
	public Assets.CustomScripts.NetworkManager mNetworkManager;

	public bool mAnimationPlaying = false; //When a creature is doing an action, like moving between spaces, don't let the player click on things or interfere
	public BoardCreation mBoardCreator;

	[HideInInspector] public GameObject gameObjectToSummon; //Used for placing creatures and traps onto the board

	bool hasSummonedVoidBreacher = false; //for keeping the player from exiting the Prepartion Phase without summoning their VoidBreacher

	//for whether or not to show the GUI for selecting which creature in a shared space to move
	bool showLimboSelectGUI = false;


	//For setting the camera position
	[SerializeField] private Vector3 mPlayerOneCameraPos;
	[SerializeField] private Vector3 mPlayerOneCameraRot;
	[SerializeField] private Vector3 mPlayerTwoCameraPos;
	[SerializeField] private Vector3 mPlayerTwoCameraRot;


	// Use this for initialization
	void Start () 
	{
		//Finding other objects that are going to be referenced
		mNetworkManager = GameObject.Find("NetworkManager").GetComponent<Assets.CustomScripts.NetworkManager>();
		handManager = GetComponent<PlayerHandManager>();
		turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
		turnManager.UpdatePlayerCount(this);
		mBoardCreator = FindObjectOfType(typeof(BoardCreation)) as BoardCreation;


	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mRemainingMonuments <= 1 && !mGameOver)
		{
			GetComponent<NetworkView>().RPC("RevealWinner", RPCMode.All, playerNumber);
		}
		//only do stuff on your turn
		if (turnManager.playerTurn == playerNumber && this.GetComponent<NetworkView>().isMine)
		{
			//clicking to select things
			if (Input.GetMouseButtonDown(0) && !mouseOverGUI && !mAnimationPlaying)
			{
				showLimboSelectGUI = false;
				//Raycasting from the camera to see what you clicked on
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast(ray, out hit))
				{
					//What to do if the player clicks on a Creature
					if (hit.collider.GetComponent<Creature>() != null)
					{
						Debug.Log("Clicked a creature");
						//What to do if the player clicks on a Creature they own
						if (hit.collider.GetComponent<Creature>().controllingPlayer == playerNumber)
						{

							DeselectCreature();//Deselect whatever you were selecting already
							hit.collider.GetComponent<Creature>().BeClickedOn();//select your creature
							selectedCreature = hit.collider.GetComponent<Creature>();

							//what to do if they clicked on that creature as a part of an Action Card
							if(handManager.playingActionCard)
							{
								//Set that creature as the origin for the action card's effect if you were on that step of playing the card
								if (selectedCreature.currentSpace.GetComponent<BoardSquare>().inMovementRange)
								{
									handManager.actionCardToPlay.UnTargetAllSpaces();
									handManager.actionCardToPlay.CardEffect();

								}
							}
							else if(handManager.mTargetting)
							{
								//Set that creature as the target of the action card's effect if you were on that step of playing the card
								if (selectedCreature.currentSpace.GetComponent<BoardSquare>().inMovementRange || selectedCreature.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget)
								{
									mLastSelectedCreature = selectedCreature;
									handManager.actionCardToPlay.UnTargetAllSpaces();
									Debug.Log("Clicked a creature you own");
									handManager.actionCardToPlay.TargetAcquired();
								}
							}

							//what to do for clicking on that creature outside of playing an action card
							else
							{
								//What to do during the Preparation Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
								{
									
								}
								//What to do during the Summoning Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
								{
									if (gameObjectToSummon != null)
									{
										//Familiar type creatures are attached to VoidBreachers for summoning
										if (gameObjectToSummon.tag == "Familiar")
										{
											if (hit.collider.tag == "VoidBreacher")
											{
												//Tell the VoidBreacher that it is now holding a creature to summon
												hit.collider.GetComponent<Creature>().summonToLimbo(gameObjectToSummon, selectedCardInHand);
												//Pay the summoned creature's cost
												sourceStoneCount -= gameObjectToSummon.GetComponent<Creature>().sourceStoneCost;
												//Set it so that you are no longer preparing to summon a creature
												gameObjectToSummon = null;
												//Put the game object for the summoned creature's card onto the table
												handManager.deck[selectedCardInHand].cardState = Card.CardState.Table;
												//Unhighlight spaces
												Creature[] creatures = FindObjectsOfType<Creature>();
												foreach (Creature creature in creatures)
												{
													if (creature.controllingPlayer == this.playerNumber)
													{
														creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
													}
												}
											}
										}
										//Minion type creatures are attached to Familiars for summoning
										else if (gameObjectToSummon.tag == "Minion")
										{
											if (hit.collider.tag == "Familiar")
											{
												//Tell the Familiar that it is now holding a creature to summon
												hit.collider.GetComponent<Creature>().summonToLimbo(gameObjectToSummon, selectedCardInHand);
												//Pay the summoned creature's cost
												sourceStoneCount -= gameObjectToSummon.GetComponent<Creature>().sourceStoneCost;
												//Set it so that you are no longer preparing to summon a creature
												gameObjectToSummon = null;
												//Put the game object for the summoned creature's card onto the table
												handManager.deck[selectedCardInHand].cardState = Card.CardState.Table;
												//Unhighlight spaces
												Creature[] creatures = FindObjectsOfType<Creature>();
												foreach (Creature creature in creatures)
												{
													if (creature.controllingPlayer == this.playerNumber)
													{
														creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
													}
												}
											}
										}

									}
								}//end of Summoning Phase

								//what to do with your creature duing the Movement Phase
								if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
								{
									//if the clicked creature isn't carrying any creatures in Limbo for summoning, just move it
									if (selectedCreature.carriedLimboCreatures.Count == 0)
									{
										selectedCreature.HighlightMovableSquares();
									}
									//otherwise, select which creature you want to move
									else
									{
										showLimboSelectGUI = true;
									}
								}//end of MovementPhase

								//What to do during the Combat Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
								{
									//Highlight the squares conntaining targets the clicked creature can attack
									selectedCreature.HighlightCombatSquares();
									turnManager.GetComponent<NetworkView>().RPC("ClearCombatants", RPCMode.All);
									turnManager.GetComponent<NetworkView>().RPC("SetPlayerController", RPCMode.All,this.GetComponent<NetworkView>().viewID);
									turnManager.GetComponent<NetworkView>().RPC("SetAttacker", RPCMode.All,selectedCreature.GetComponent<NetworkView>().viewID);
								}

								//What to do during the Conclusion Phase
								if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
								{
									//for action cards that allow movement during the conclusion phase
									if (selectedCreature.canMove && !mCanSwap)
									{
										selectedCreature.HighlightMovableSquares();
									}

									#region This segment of code was written by my partner on the project to take into account a specific character's ability to swap the positions of creature on the board
									//For Ripples position swap ability
									//Could be re-used if we introduce other cards that swap the poisitions of creatures
	                                if (selectedCreature.currentSpace.GetComponent<BoardSquare>().inMovementRange && mCanSwap)
	                                {
	                                    if (mSwappingCreatures.Contains(selectedCreature))
	                                    {
	                                        mSwappingCreatures.Remove(selectedCreature);
	                                    }
	                                    else
	                                    {
	                                        mSwappingCreatures.Add(selectedCreature);
	                                        if ((mSwappingCreatures.Count == 2) && GetComponent<NetworkView>().isMine)
	                                        {
	                                            GetComponent<NetworkView>().RPC("SwapCharacters", RPCMode.All, mSwappingCreatures[0].GetComponent<NetworkView>().viewID, mSwappingCreatures[1].GetComponent<NetworkView>().viewID);
	                                            mSwappingCreatures.Clear();
	                                            Creature[] creatures = FindObjectsOfType<Creature>();
	                                            foreach (Creature creature in creatures)
	                                            {
	                                                if (creature.controllingPlayer == this.playerNumber)
	                                                {
	                                                    creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
	                                                }
	                                            }
	                                            
	                                        }
	                                    }
	                                }
									#endregion
								}
							}
						}

						//What to do if the player clicks on a Creature that they don't own
						else
						{
							mLastSelectedCreature = hit.collider.GetComponent<Creature>();
							//what to do if they clicked on that creature as a part of an Action Card
							if (handManager.playingActionCard)
							{
								if (mLastSelectedCreature.currentSpace.GetComponent<BoardSquare>().inMovementRange)
								{
									//Set that creature as the origin of the action card's effect if you were on that step of playing the card
									handManager.actionCardToPlay.UnTargetAllSpaces();
									handManager.actionCardToPlay.CardEffect();
								}
							}
							else if (handManager.mTargetting)
							{
								if (mLastSelectedCreature.currentSpace.GetComponent<BoardSquare>().inMovementRange || mLastSelectedCreature.currentSpace.GetComponent<BoardSquare>().hasAttackableTarget)
								{
									//Set that creature as the target of the action card's effect if you were on that step of playing the card
									handManager.actionCardToPlay.UnTargetAllSpaces();
									Debug.Log("Clicked a creature you don't own");
									handManager.actionCardToPlay.TargetAcquired();
								}
							}
							//What to do during the Preparation Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
							{
								
							}
							//What to do during the Summoning Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
								
							}
							//what to do with the enemy creature duing the Movement Phase
							if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
							{

							}
							//What to do during the Combat Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
							{
								//If you have one of your own creatures selected and the creature you just clicked on is in range, attack it
								if (selectedCreature != null)
								{
									if (selectedCreature.canAttack && hit.collider.GetComponent<Creature>().currentSpace.GetComponent<BoardSquare>().hasAttackableTarget)
									{
										turnManager.GetComponent<NetworkView>().RPC("SetDefender", RPCMode.All, hit.collider.GetComponent<NetworkView>().viewID);
										selectedCreature.AttackTarget(hit.collider.GetComponent<Creature>());
									}
									else if (selectedCreature.mCanAttackMonument && selectedCreature.canAttack && hit.collider.GetComponent<Monument>() && !selectedCreature.lockedInCombat)
									{
										Debug.Log("Telling " + selectedCreature.name + " to attack " + hit.collider.name); 
										selectedCreature.GetComponent<NetworkView>().RPC("AttackMonument", RPCMode.All);
									}
								}
							}
							//What to do during the Conclusion Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
							{
								
							}

						}
					}//End of Clicking on Creatures


					//What to do if the player clicks on a Monument
					if (hit.collider.GetComponent<Monument>() != null)
					{
						//What to do if the player clicks on a Monument they own
						if (hit.collider.GetComponent<Monument>().mOwner == playerNumber)
						{

							//What to do during the Preparation Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
							{
							}//END of Preparation Phase
							//What to do during the Summoning Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
							}//END of Summoning Phase
							
							//what to do with your monument duing the Movement Phase
							if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
							{
							}//END of MovementPhase
							
							//What to do during the Combat Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
							{
							}//END of CombatPhase
							
							//What to do during the Conclusion Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
							{
							}
						}
						//What to do if the player clicks on a Monument that they don't own
						else
						{
							//What to do during the Preparation Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
							{
							}
							//What to do during the Summoning Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
							}
							//what to do with the enemy monument duing the Movement Phase
							if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
							{
							}
							//What to do during the Combat Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
							{
								//If you have one of your creatures selected and it's in range to attack the monument, then have it attack that monument
								if (selectedCreature != null)
								{
									if (selectedCreature.mCanAttackMonument && selectedCreature.canAttack && hit.collider.GetComponent<Monument>()==selectedCreature.mAttackableMonument)
									{
										Debug.Log("Telling " + selectedCreature.name + " to attack " + hit.collider.name); 
										selectedCreature.GetComponent<NetworkView>().RPC("AttackMonument", RPCMode.All);
									}
								}
							}
							//What to do during the Conclusion Phase
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
							{
								
							}
							
						}
					}//End of Clicking on Monuments

					//What to do if the player clicks on a Square
					if (hit.collider.GetComponent<BoardSquare>() != null)
					{
						mClickedSquare = hit.collider.GetComponent<BoardSquare>();
						BoardSquare clickedSquare = hit.collider.GetComponent<BoardSquare>();
						//For when you click on a high-lighted square while playing an action card
						if (handManager.playingActionCard)
						{
							if (clickedSquare.hasAttackableTarget || clickedSquare.inMovementRange)
							{
								handManager.actionCardToPlay.UnTargetAllSpaces();
								if(clickedSquare.occupier!= null && clickedSquare.occupier.GetComponent<Creature>() != null)
								{
									mLastSelectedCreature = clickedSquare.occupier.GetComponent<Creature>();
									selectedCreature = mLastSelectedCreature;
								}

								handManager.actionCardToPlay.CardEffect();
							}
						}
						else if (handManager.mTargetting)
						{
							if (clickedSquare.hasAttackableTarget || clickedSquare.inMovementRange)
							{
								handManager.actionCardToPlay.UnTargetAllSpaces();
								mClickedSquare = clickedSquare;
								if(clickedSquare.occupier!= null && clickedSquare.occupier.GetComponent<Creature>() != null)
								{
									mLastSelectedCreature = clickedSquare.occupier.GetComponent<Creature>();
								}
								Debug.Log("Clicked a Square");
								handManager.actionCardToPlay.TargetAcquired();
							}
						}
						else if (handManager.mActivatingActionCard)
						{
							if (clickedSquare.hasAttackableTarget || clickedSquare.inMovementRange)
							{
								handManager.actionCardToPlay.UnTargetAllSpaces();
								mClickedSquare = clickedSquare;
								handManager.actionCardToPlay.ActivateOnTarget();
							}
						}

						//What to do during the Preparation Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
						{
							//summon your VoidBreacher on the First Turn
							if (clickedSquare.playerStartSpot == playerNumber && playerNumber == turnManager.playerTurn && clickedSquare.voidBreacherStartSpot == true)
							{
								Card myVoidBreacherCard = handManager.deck[0];
								int voidBreacherCardNumber =0;
								//find the VoidBreacher in your deck
								for (int i=0; i < handManager.deck.Count; i++)
								{
									if (handManager.deck[i].cardType == Card.CardType.VoidBreacher)
									{
										myVoidBreacherCard = handManager.deck[i];
										voidBreacherCardNumber =  i;
									}
								}
								if (myVoidBreacherCard.cardState == Card.CardState.Hand)
								{
									SummonVoidBreacher(voidBreacherCardNumber, hit.collider.transform.position);
									//this.networkView.RPC("SummonVoidBreacher", RPCMode.All, voidBreacherCardNumber, hit.collider.transform.position);

								}
							}
						}
						//What to do during the Summoning Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
						{
							
						}
						//What to do during the Movement Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
						{
							if (selectedCreature)
							{
								//Telling your creature that you have selected to move from one space to another
								if (selectedCreature.canMove && hit.collider.GetComponent<BoardSquare>().inMovementRange)
								{
									SetCreatureMovePath(mClickedSquare, selectedCreature);
									selectedCreature.MoveToSpace();
									selectedCreature.mCanAttackMonument = false;
									selectedCreature.mAttackableMonument = null;
									selectedCreature.canMove = false;
									selectedCreature.BeUnclicked();
									//If the creature you just moved had the Elusive property, unlock it from combat with other creatures
									if (selectedCreature.mElusive)
									{
										foreach(Creature combatRival in selectedCreature.mCombatLockTargets)
										{
											combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, selectedCreature.GetComponent<NetworkView>().viewID);
										}
										selectedCreature.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
									}
								}
							}
							//Tell a creature that you're getting ready to summon to appear on the board and move out of its summoner's space
							if (gameObjectToSummon)
							{
								if (hit.collider.GetComponent<BoardSquare>().validSummonSpot)
								{
									SummonLimboCreature(hit.collider.transform.position);
								}
							}
						}

						//What to do during the Combat Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
						{
							//for moving creatures with the victory rush effect (for right now just the Minotaur), which lets them move a space after defeating another creature
							if (selectedCreature) 
							{
								if (selectedCreature.canMove && hit.collider.GetComponent<BoardSquare>().inMovementRange)
								{
									SetCreatureMovePath(mClickedSquare, selectedCreature);
									selectedCreature.MoveToSpace();
									selectedCreature.canMove = false;
									selectedCreature.BeUnclicked();
								}
							}

						}
						//What to do during the Conclusion Phase
						if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
						{
							//For cards that let you move during the Conclusion Phase
							if (selectedCreature)
							{
								if (selectedCreature.canMove && hit.collider.GetComponent<BoardSquare>().inMovementRange)
								{
									selectedCreature.transform.position = hit.collider.transform.position + new Vector3(0,0.5f,0);
									selectedCreature.mCanAttackMonument = false;
									selectedCreature.mAttackableMonument = null;
									selectedCreature.canMove = false;
									selectedCreature.BeUnclicked();
									if (selectedCreature.mElusive)
									{
										foreach(Creature combatRival in selectedCreature.mCombatLockTargets)
										{
											combatRival.GetComponent<NetworkView>().RPC("RemoveFromCombatLock", RPCMode.All, selectedCreature.GetComponent<NetworkView>().viewID);
										}
										selectedCreature.GetComponent<NetworkView>().RPC("BreakFromCombatLock", RPCMode.All);
									}
								}
							}
						}
					}
				}
			}



		}
	}//End of Update()

	void OnGUI()
	{
		#region Segement of code for ending the game and going back to the title screen was written by Sean Lambdin
		if (mGameOver && GetComponent<NetworkView>().isMine) //Game over stuff
        {
			if (mLosingPlayer == playerNumber)
            {
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 50, 50), "You Lose");
            }
			else if (mLosingPlayer != playerNumber)
            {
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 50, 50), "You Win");
            }

            if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.75f, Screen.width * 0.10f, Screen.height * 0.10f), new GUIContent("Return Players to Lobby")))
            {
                Debug.Log("This should be hitting");
                //mNetworkManager.GetComponent<NetworkView>().RPC("ReturnToLobby", RPCMode.All);
				mNetworkManager.ReturnToLobby();
				//Destroy(this.gameObject);
            }
        }//end of game over stuff
		#endregion
		//only draw the GUI on my turn and when nothing is animating
		if (turnManager.playerTurn == playerNumber && this.GetComponent<NetworkView>().isMine && !mAnimationPlaying)
		{
			//Have a button to change to next phase if you're not in the middle of playing an action card
			if (!handManager.playingActionCard && !handManager.mTargetting  &&!handManager.mActivatingActionCard)
			{
				//Button to go to the next phase if not on the Conclusion Phase
				if (turnManager.currentPhase != TurnPhaseManager.TurnPhase.Conclusion)
				{
					if (hasSummonedVoidBreacher == true)
					{
						if (GUI.Button(new Rect(Screen.width * 0.9f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "Next Phase"))
						{
							//AdvancePhase
							turnManager.GetComponent<NetworkView>().RPC("AdvancePhase", RPCMode.All);
							if(turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
							{
								FindObjectOfType<DeckTrayToggle>().mDeckDisplayed = true;
							}
							if (!mBoardCreator.squaresDeleted)
							{
									//delete any unused board squares at the beginning of the match after tiles have been randomized
									BoardSquare[] boardSquares = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
									foreach (BoardSquare square in boardSquares)
									{
										square.DeleteIfUnused();
									}
									//delete the initilization tiles
									GameObject[] tilesToDelete = GameObject.FindGameObjectsWithTag("BoardTile");
									foreach (GameObject tile in tilesToDelete)
									{
										Destroy(tile);
									}
									mBoardCreator.squaresDeleted = true;

							}
							//Unhighlight spaces
							Creature[] creatures = FindObjectsOfType<Creature>();
							foreach (Creature creature in creatures)
							{
								if (creature.controllingPlayer == this.playerNumber)
								{
									creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
								}
							}
						}
					}
				}
				//Button to end Turn if on the Conclusion Phase
				else
				{
					if (GUI.Button(new Rect(Screen.width * 0.9f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "End Turn"))
					{
						//AdvancePhase
						turnManager.GetComponent<NetworkView>().RPC("AdvancePhase", RPCMode.All);
						//Unhighlight spaces
						Creature[] creatures = FindObjectsOfType<Creature>();
						foreach (Creature creature in creatures)
						{
							if (creature.controllingPlayer == this.playerNumber)
							{
								creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
							}
						}
					}
				}
			}
			//Display GUI asking to resolve your Action Card before continuing to the next phase
			else
			{
				GUI.Box(new Rect(Screen.width * 0.9f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "Please Finish Your Action");
			}

			//draw the GUI to select which creature in a shared space to move
			{
				if(showLimboSelectGUI)
				{
					if (selectedCreature.canMove)
					{
						//The button for moving the ceature already on the board
						if(GUI.Button(new Rect(Screen.width*0.5f, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent(selectedCreature.creatureName, selectedCreature.creatureName)))
						{
							selectedCreature.HighlightMovableSquares();
							showLimboSelectGUI = false;
						}
					}
					//The buttons for the creatures that the selected creature is carrying
					for(int i = 0; i < selectedCreature.carriedLimboCreatures.Count; i++)
					{
						if(GUI.Button(new Rect(Screen.width*0.61f+Screen.width*0.11f*i, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f),  new GUIContent(selectedCreature.carriedLimboCreatures[i].GetComponent<Creature>().creatureName, selectedCreature.carriedLimboCreatures[i].GetComponent<Creature>().creatureName)))
						{
							gameObjectToSummon = selectedCreature.carriedLimboCreatures[i];
							selectedCreature.limboCreatureLookingAt = i;
							selectedCreature.HighlightSummonSquares(gameObjectToSummon.GetComponent<Creature>());
							showLimboSelectGUI = false;
						}
					}
				}
			}

		}
		//making it so mouse doesn't click the board when over a button
		if (Event.current.type == EventType.Repaint)
		{
			//Mouse over a button
			if (GUI.tooltip != "")
			{
				mouseOverGUI = true;

				foreach(Card cardImage in handManager.deck)
				{
					if(GUI.tooltip == cardImage.cardName)
					{
						handManager.mMouseOverInfoManager.mCardImage = cardImage.mCardArt;
						handManager.mMouseOverInfoManager.mStrengthStatus = 0;
						handManager.mMouseOverInfoManager.mFortitudeStatus = 0;
						handManager.mMouseOverInfoManager.mRuneCardImages.Clear();
					}
				}

			}
			//Mouse not over a button
			else
			{
				mouseOverGUI = false;
			}//END of making it so mouse doesn't click the board when over a button

		}
	}//end of OnGUIO()


	void DeselectCreature()
	{
		if(selectedCreature != null)
		{
			selectedCreature.BeUnclicked();
			//selectedCreature = null;
		}
	}//End of DeselectCreature()

	public void RefillSourceStones()
	{
		sourceStoneCount = 12;
	}//End of RefillSourceStones()
	

	#region Functions for summoning Familiars/Minions/Traps.  Written by me, but extensive enough to deserve a region
	public void ReadyGameObjectToSummon(GameObject summonedObject, int handCardNumber)
	{
		gameObjectToSummon = summonedObject;
		selectedCardInHand = handCardNumber;
	}//end of ReadyGameObjectToSummon()



	//RPC call to summon Voidbreacher
	[RPC] void SummonVoidBreacher(int vbCardNumber, Vector3 vbSummonSquare)
	{
		//summon the VoidBreacher
		GameObject summonedVoidBreacher = (GameObject)Network.Instantiate(handManager.deck[vbCardNumber].summonedGameObject, vbSummonSquare + new Vector3(0,0.5f,0), Quaternion.identity,0);
		summonedVoidBreacher.GetComponent<NetworkView>().RPC("SummonPlayerInfo", RPCMode.All, playerNumber, vbCardNumber, handManager.deck[vbCardNumber].cardName, handManager.deck[vbCardNumber].mCardDescription);
		handManager.deck[vbCardNumber].cardState = Card.CardState.Table;
		//unhighlight the voidbreacher summon squares
		this.GetComponent<NetworkView>().RPC("ClearVoidBreacherSummonSquares", RPCMode.All);
		hasSummonedVoidBreacher = true;
		if (GetComponent<NetworkView>().isMine)
		{
			mVoidBreacher = summonedVoidBreacher.GetComponent<Creature>();
		}
		Debug.Log("Clearing summon squares");
	}
	//Unhighlight squares specially associated with VoidBreacher summoning
	[RPC] void ClearVoidBreacherSummonSquares ()
	{
		BoardSquare[] summonSquares  = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare summonSquareToUnhighlight in summonSquares)
		{
			if (summonSquareToUnhighlight.playerStartSpot == turnManager.playerTurn)
			{
				summonSquareToUnhighlight.playerStartSpot=0;
//				Debug.Log("Summon Square cleared");
			}
		}
	}
	//Summon a creature into the space of the creature that's carrying it, then move it to the space you told it to go to
	[RPC] void SummonLimboCreature (Vector3 lcSummonSquarePos)
	{
		GameObject newSummon = (GameObject)Network.Instantiate(gameObjectToSummon, selectedCreature.transform.position, Quaternion.identity,0);
		//Assign the newlyy summoned creature all it's referenced information, such as the card it's associated with and who owns it
		newSummon.GetComponent<NetworkView>().RPC("SummonPlayerInfo", RPCMode.All, playerNumber, selectedCreature.limboCreatureHandCardNumber[selectedCreature.limboCreatureLookingAt], 
		                          handManager.deck[selectedCreature.limboCreatureHandCardNumber[selectedCreature.limboCreatureLookingAt]].cardName, 
		                          handManager.deck[selectedCreature.limboCreatureHandCardNumber[selectedCreature.limboCreatureLookingAt]].mCardDescription);
		SetCreatureMovePath(mClickedSquare, newSummon.GetComponent<Creature>());
		//Let the creature that was already on the board know that it's no longer carrying the creature you just summoned
		selectedCreature.summonFromLimbo();
		selectedCreature.BeUnclicked();

		gameObjectToSummon = null;
	}//END of SumonLimboCreature ()
	#endregion

	#region functions involved in ending the game, written by Sean
	[RPC]
	void UpdateMonumentCount(string destroyedMonumentName)
	{
		Monument monumentToDestroy = GameObject.Find(destroyedMonumentName).GetComponent<Monument>();
		if (monumentToDestroy.mOwner == playerNumber)
		{
			mRemainingMonuments -= 1;
		}
		//This line that is commented out would allow creatures to enter the spaces leftover by dead monuments -Adam
		//monumentToDestroy.mOccupiedSquare.occupier = null;
		//Destroy(GameObject.Find(destroyedMonumentName).gameObject);

	}//END of UpdateMonumentCount()
	#endregion

	[RPC]
	void RevealWinner(int loser)
	{
		mLosingPlayer = loser;
		mGameOver = true;
		foreach(PlayerClickController player in FindObjectsOfType<PlayerClickController>())
		{
			player.mLosingPlayer = loser;
			player.mGameOver = true;
		}
	}//END of RevealWinner()

	#region Swap creatures function not written by me
    [RPC]
    void SwapCharacters(NetworkViewID swap1, NetworkViewID swap2)
    {
        GameObject creature1 = NetworkView.Find(swap1).observed.gameObject;
        GameObject creature2 = NetworkView.Find(swap2).observed.gameObject;
        Vector3 temp1 = creature1.transform.position;
        Vector3 temp2 = creature2.transform.position;

        creature1.transform.position = temp2;
        creature2.transform.position = temp1;

        GameObject tempSquare = creature1.GetComponent<Creature>().currentSpace;
        creature1.GetComponent<Creature>().currentSpace = creature2.GetComponent<Creature>().currentSpace;
        creature2.GetComponent<Creature>().currentSpace = tempSquare;

        creature1.GetComponent<Creature>().currentSpace.GetComponent<BoardSquare>().occupier = creature1;
        creature2.GetComponent<Creature>().currentSpace.GetComponent<BoardSquare>().occupier = creature2;

		creature1.GetComponent<Creature>().BreakFromCombatLock();
		creature2.GetComponent<Creature>().BreakFromCombatLock();
	}//END of SwapCharacters
	#endregion

	//For having a creature physically move accross the board
	public void SetCreatureMovePath(BoardSquare destinationSquare, Creature creatureToMove)
	{
		creatureToMove.mPathSquares.Clear();
		//Tell the creature it's final destination
		creatureToMove.mPathSquares.Add(destinationSquare.gameObject);

		//Tell the creature all the squares it needs to move to
		for (int i = destinationSquare.mDistanceFromMover; i>0; i--)
		{
			bool hasAddedToPath = false;
			foreach(BoardSquare neighbor in creatureToMove.mPathSquares[0].GetComponent<BoardSquare>().neighborSquares)
			{
				if (neighbor.mDistanceFromMover >= creatureToMove.mPathSquares[0].GetComponent<BoardSquare>().mDistanceFromMover)
				{
					neighbor.mDistanceFromMover = 99;
				}
				else if (hasAddedToPath)
				{
					neighbor.mDistanceFromMover = 99;
				}
				else
				{
					creatureToMove.mPathSquares.Insert(0,neighbor.gameObject);
					hasAddedToPath = true;
				}
			}
		}
	}//END of SetCreatureMovePath

	//Make it so the players view the board from different angles, as if they were sitting on opposite sides of a physical board game
	public void SetCameraPos()
	{
		if (this.GetComponent<NetworkView>().isMine)
		{
			if(playerNumber == 1)
			{
				Camera.main.transform.position = mPlayerOneCameraPos;
				Camera.main.transform.rotation = Quaternion.Euler(mPlayerOneCameraRot);
				Debug.Log(this.name + GetComponent<NetworkView>().viewID + "is setting the main camera to the player 1 position.");
			}
			if(playerNumber == 2)
			{
				Camera.main.transform.position = mPlayerTwoCameraPos;
				Camera.main.transform.rotation = Quaternion.Euler(mPlayerTwoCameraRot);
				Debug.Log(this.name  + GetComponent<NetworkView>().viewID + "is setting the main camera to the player 2 position.");
			}
		}
	}//END of SetCameraPos

}
