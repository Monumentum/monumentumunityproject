﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script, along with PlayerClickController serve as the two primary components of the PlayerController prefab
//It stores references to the game objects that constitute the cards in the player's hand/deck for the purposes of cycling them between in-hand, in-play, and recharging/cooldown.

public class PlayerHandManager : MonoBehaviour 
{
	public List<Card> deck = new List<Card>();

	public TurnPhaseManager turnManager;
	PlayerClickController clickController;
	public int playerNumber;
	//Whether or not the player is currently looking at the action cards in their hand ~Adam
	public bool viewActionCards = false;

	//For the several steps that playing a card goes through ~Adamm
	public bool playingActionCard = false;
    public bool mTargetting = false;
    public bool mActivatingActionCard = false;

	//The Action card whose effects are being played out ~Adam
	public Card actionCardToPlay;
	//The card we're currently using/looking at ~Adam
    public Card mCurrentCard;

	//Whether or not an animation we don't want intterurrpted is happening ~Adam
	public bool mAnimationPlaying = false;

	//Info for mouseover tooltip ~Adam
	[HideInInspector] public MouseOverInfoManager mMouseOverInfoManager;
	
	public GUIStyle mHandCardTooltipStyle;
	[SerializeField] private GUIStyle mHandDisplayStyle;

	//For hiding/displaying the deck ~Adam
	DeckTrayToggle mDeckTray;

	// Use this for initialization
	void Start () 
	{
		//set up the objects we'll be referencing with this script
		clickController = GetComponent<PlayerClickController>();
		turnManager = GameObject.Find("TurnManager").GetComponent<TurnPhaseManager>();
		playerNumber = clickController.playerNumber;
		
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

		mDeckTray = FindObjectOfType(typeof(DeckTrayToggle)) as DeckTrayToggle;

	}//end of Start()
	
	// Update is called once per frame
	void Update () 
	{
	
	}//end of Update()


	void OnGUI ()
	{
		// only draw stuff on my turn and when nothing is animating
		if (turnManager.playerTurn == playerNumber && this.GetComponent<NetworkView>().isMine && !mAnimationPlaying) 
		{
			//Variables for setting up the UI positiong for displaying the cards in the player's hand
			float handGUIXPos;
			float handGUIYPos;
			int handGUINumber;
			Card currentCardForGUI;

			//hide other GUI things if I'm in the middle of playing an action card, but provide a cancel button
			if (playingActionCard || mTargetting || mActivatingActionCard)
			{
				if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("Cancel Action Card", "Cancel Action")))
				{
					mDeckTray.ToggleDeckDisplay();
					clickController.gameObjectToSummon = null;
					playingActionCard = false;
					mTargetting = false;
					mActivatingActionCard = false;
					actionCardToPlay.cardState = Card.CardState.Hand;
					actionCardToPlay.mTargetedCreature = null;
					clickController.sourceStoneCount += actionCardToPlay.sourceStoneCost;
					actionCardToPlay = null;
					UnHighlightSquares();

				}

			}
			//Cancel button for summoning
			else if (clickController.gameObjectToSummon != null)
			{
				if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("Cancel Summoing", "Cancel Summoning")))
				{
					mDeckTray.ToggleDeckDisplay();

					clickController.gameObjectToSummon = null;
					actionCardToPlay = null;
					UnHighlightSquares();
					
				}
				
			}
//
//			else
//			{
//				//Showing/Hiding Action Cards
//				if (viewActionCards)
//				{
//					//Button to hide Action Cards
//					if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("Hide Deck", "Hide your deck")))
//					{
//						viewActionCards = false;
//					}
//
//					//Display the Action cards in your hand to the GUI
//					handGUIXPos = Screen.width*0.005f;
//					handGUIYPos = Screen.height*0.42f;
//					handGUINumber=1;
//
//					//Putting a backdrop to the deck box
//					GUI.Box(new Rect(0f, Screen.height*0.41f, Screen.width*0.075f*6.5f, Screen.height*0.43f), "");
//
//					for (int i = 0; i < deck.Count; i++)
//					{
//						currentCardForGUI = deck[i].GetComponent<Card>();
//						if(deck[i].GetComponent<Card>().cardState == Card.CardState.Hand)
//						{
//							if (currentCardForGUI.cardType == Card.CardType.Action || currentCardForGUI.cardType == Card.CardType.Familiar || currentCardForGUI.cardType == Card.CardType.Minion)
//							{
//								//For Creature Cards
//								if(currentCardForGUI.cardType == Card.CardType.Familiar || currentCardForGUI.cardType == Card.CardType.Minion)
//								{
//									if(GUI.Button(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.075f, Screen.height*0.2f), new GUIContent(currentCardForGUI.mCardArt.texture, currentCardForGUI.cardName), mHandDisplayStyle))
//									{
//										if (clickController.sourceStoneCount >= currentCardForGUI.sourceStoneCost && turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
//										{
//											clickController.ReadyGameObjectToSummon(deck[i].summonedGameObject, i);
//											
//											Creature[] creatures = FindObjectsOfType<Creature>();
//											
//											//Highlight Spaces containing creatures that you can tell to carry the creature in preparation for summoning it
//											foreach (Creature creature in creatures)
//											{
//												if (creature.controllingPlayer == this.playerNumber)
//												{
//													if (currentCardForGUI.cardType == Card.CardType.Familiar && creature.tag == "VoidBreacher")
//													{
//														creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
//													}
//													else if (currentCardForGUI.cardType == Card.CardType.Minion && creature.tag == "Familiar")
//													{
//														creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
//													}
//													else
//													{
//														creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
//													}
//												}
//											}
//											viewActionCards = false;
//										}
//									}
//									if(turnManager.currentPhase != TurnPhaseManager.TurnPhase.Summoning)
//									{
//										GUI.Box(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.075f, Screen.height*0.2f), "");
//									}
//								}
//								//For Action Cards
//								else if (currentCardForGUI.cardType == Card.CardType.Action)
//								{
//									if(GUI.Button(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.075f, Screen.height*0.2f), new GUIContent(currentCardForGUI.mCardArt.texture, currentCardForGUI.cardName), mHandDisplayStyle))
//	//								                                                                                                       currentCardForGUI.mCardDescription)) )
//									{
//										//Play the action card this button is for and do its effects if the player has enough source stones to pay its cost
//										if (clickController.sourceStoneCount >= currentCardForGUI.sourceStoneCost)
//										{
//											clickController.sourceStoneCount -= currentCardForGUI.sourceStoneCost;
//											viewActionCards = false;
//											playingActionCard = true;
//											currentCardForGUI.CardTarget();
//											actionCardToPlay = currentCardForGUI;
//										}
//									}
//								}
//
//								//Adjust the GUI positioning for the next card's button
//								handGUIXPos += Screen.width*0.08f;
//								if ((handGUINumber)%6==0)
//								{
//									handGUIXPos = Screen.width*0.005f;
//									handGUIYPos += Screen.height*0.21f;
//								}
//								handGUINumber++;
//
//							}
//						}
//						//Display cards not in the hand
//						else if(currentCardForGUI.cardType != Card.CardType.VoidBreacher)
//						{
//							GUI.Label(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.075f, Screen.height*0.2f), new GUIContent(currentCardForGUI.mCardArt.texture, currentCardForGUI.cardName), mHandDisplayStyle);
//							GUI.Box(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.075f, Screen.height*0.2f), "On " + currentCardForGUI.cardState.ToString());
//
//							//Adjust the GUI positioning for the next card's button
//							handGUIXPos += Screen.width*0.08f;
//							if ((handGUINumber)%6==0)
//							{
//								handGUIXPos = Screen.width*0.005f;
//								handGUIYPos += Screen.height*0.21f;
//							}
//							handGUINumber++;
//						}
//					}
//
//
//
//				}
//				else
//				{
//					//Button to view Action Cards
//					if (GUI.Button(new Rect(0, Screen.height*0.3f, Screen.width*0.1f, Screen.height*0.1f), new GUIContent("Show Deck", "View your deck")))
//					{
//						viewActionCards = true;
//						UnHighlightSquares();
//					}
//				}//End of showing/hiding action cards



				//What to do on the Preparation Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Preparation)
				{

				}//End of Preparation Phase

				//What to do on the Summoning Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
				{
//					if(clickController.gameObjectToSummon != null)
//					{
//						//viewActionCards = false;
//					}
//					else
//					{
//						viewActionCards = true;
//					}
//					//display the Familiars/Minions in your hand to the GUI during the Summoning Phase
//					handGUIXPos = Screen.width*0.1f;
//					handGUIYPos = Screen.height*0.7f;
//					handGUINumber = 1;
//					for (int i = 0; i < deck.Count; i++)
//					{
//						currentCardForGUI = deck[i].GetComponent<Card>();
//						if(deck[i].GetComponent<Card>().cardState == Card.CardState.Hand)
//						{
//							if (currentCardForGUI.cardType == Card.CardType.Familiar || currentCardForGUI.cardType == Card.CardType.Minion)
//							{
////								if(GUI.Button(new Rect(handGUIXPos,handGUIYPos, Screen.width*0.1f, Screen.height*0.1f), new GUIContent(currentCardForGUI.cardName + ": " + currentCardForGUI.sourceStoneCost, currentCardForGUI.cardName)))
//////								                                                                                                       "Strength: " + currentCardForGUI.summonedGameObject.GetComponent<Creature>().strength
//////								                                                                                                       +"\nFortitude: " + currentCardForGUI.summonedGameObject.GetComponent<Creature>().fortitude
//////								                                                                                                       +"\n"+ currentCardForGUI.mCardDescription)) )
////								{
////									if (clickController.sourceStoneCount >= currentCardForGUI.sourceStoneCost)
////									{
////										clickController.ReadyGameObjectToSummon(deck[i].summonedGameObject, i);
////
////										Creature[] creatures = FindObjectsOfType<Creature>();
////
////										//Highlight Spaces containing creatures that you can tell to carry the creature in preparation for summoning it
////										foreach (Creature creature in creatures)
////										{
////											if (creature.controllingPlayer == this.playerNumber)
////											{
////												if (currentCardForGUI.cardType == Card.CardType.Familiar && creature.tag == "VoidBreacher")
////												{
////													creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
////												}
////												else if (currentCardForGUI.cardType == Card.CardType.Minion && creature.tag == "Familiar")
////												{
////													creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
////												}
////												else
////												{
////													creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
////												}
////											}
////										}
////									}
////								}
//								handGUIXPos += Screen.width*0.11f;
//								if ((handGUINumber)%6==0)
//								{
//									handGUIXPos = Screen.width*0.1f;
//									handGUIYPos += Screen.height*0.11f;
//								}
//								handGUINumber++;
//
//							}
//						}
//					}
				}//End of Summoning Phase

				//What to do on the Movement Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Movement)
				{
					
				}//End of Movement Phase
				
				//What to do on the Combat Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Combat)
				{
					
				}//End of Combat Phase
				
				//What to do on the Conclusion Phase
				if (turnManager.currentPhase == TurnPhaseManager.TurnPhase.Conclusion)
				{
					
				}//End of Conclusion Phase

		}

		if (Event.current.type == EventType.Repaint)
		{
			//Dispaly the card art/info for the card whose button is currently being moused-over
			if (GUI.tooltip != "")
			{
				clickController.mouseOverGUI = true;

				foreach(Card cardImage in deck)
				{
					if(GUI.tooltip == cardImage.cardName)
					{
						mMouseOverInfoManager.mCardImage = cardImage.mCardArt;
						mMouseOverInfoManager.mStrengthStatus = 0;
						mMouseOverInfoManager.mFortitudeStatus = 0;
						mMouseOverInfoManager.mRuneCardImages.Clear();
					}
				}

			}
			else
			{
			}
		}
	}//end of OnGUI

	//find the cards and put them in your hand at the start of the game
	public void CreateHand ()
	{
		clickController = GetComponent<PlayerClickController>();

		playerNumber = clickController.playerNumber;
		Debug.Log("Creating Hand for Player " + playerNumber);

		Card[] cardsToFind = FindObjectsOfType<Card>();
		//Because the prefabs of the card game objects do not have network view components, but get instatiated for both players, 
		//assign the proper player number to both sets of cards
		foreach (Card myCard in cardsToFind)
		{
			if (myCard.playerNumber == 0)
			{
				if(Network.isServer)
				{
					myCard.playerNumber = 2;
				}
				else if (Network.isClient)
				{
					myCard.playerNumber = 1;
				}
			}
			//Add the cards that belong to this player to this player's hand/deck
			if (myCard.playerNumber == playerNumber)
			{
				deck.Add(myCard);
				myCard.handNumber = deck.Count-1;
				myCard.handManager = this;
				myCard.handClickController = this.clickController;

			}
		}

		AssignUIDeck();
	}//End of CreateHand()


	void UnHighlightSquares ()
	{
		BoardSquare[] squaresToUnHighlight = FindObjectsOfType(typeof(BoardSquare)) as BoardSquare[];
		foreach (BoardSquare unHighlightTarget in squaresToUnHighlight)
		{
			unHighlightTarget.inMovementRange = false;
			unHighlightTarget.hasAttackableTarget = false;
			unHighlightTarget.validSummonSpot = false;
		}
	}//End of UnHighlightSquares()


	public void AssignUIDeck()
	{
		//For new UI setup ~Adam
		if(this.GetComponent<NetworkView>().isMine)
		{
			Debug.Log("Assigning UI Deck Buttons");
			for (int i = 0; i < deck.Count; i++)
			{
				DeckDisplayButton[] deckButtons = FindObjectsOfType<DeckDisplayButton>();
				foreach(DeckDisplayButton cardButton in deckButtons)
				{
					if (cardButton.mDeckNumber == i)
					{
						cardButton.mHandManager = this;
						cardButton.mAssociatedCard = deck[i];
						cardButton.AssignCard();
					}
				}
			}
		}
	}//END of AssignUIDeck()

	public void PlayCardFromUIButton(int cardNumberToPlay)
	{
		//Playing a creature
		if(deck[cardNumberToPlay].cardType == Card.CardType.Familiar || deck[cardNumberToPlay].cardType == Card.CardType.Minion)
		{
			if (clickController.sourceStoneCount >= deck[cardNumberToPlay].sourceStoneCost && turnManager.currentPhase == TurnPhaseManager.TurnPhase.Summoning)
			{
				mDeckTray.ToggleDeckDisplay();
				clickController.ReadyGameObjectToSummon(deck[cardNumberToPlay].summonedGameObject, cardNumberToPlay);
				
				Creature[] creatures = FindObjectsOfType<Creature>();
				
				//Highlight Spaces containing creatures that you can tell to carry the creature in preparation for summoning it
				foreach (Creature creature in creatures)
				{
					if (creature.controllingPlayer == this.playerNumber)
					{
					if (deck[cardNumberToPlay].cardType == Card.CardType.Familiar && creature.tag == "VoidBreacher")
						{
							creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
						}
					else if (deck[cardNumberToPlay].cardType == Card.CardType.Minion && creature.tag == "Familiar")
						{
							creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = true;
						}
					else
						{
							creature.currentSpace.GetComponent<BoardSquare>().inMovementRange = false;
						}
					}
				}
				viewActionCards = false;
			}
		}

		//For Action Cards
		else if ( deck[cardNumberToPlay].cardType == Card.CardType.Action)
		{

			//Play the action card this button is for and do its effects if the player has enough source stones to pay its cost
			if (clickController.sourceStoneCount >=  deck[cardNumberToPlay].sourceStoneCost)
			{
				mDeckTray.ToggleDeckDisplay();
				clickController.sourceStoneCount -= deck[cardNumberToPlay].sourceStoneCost;
				viewActionCards = false;
				playingActionCard = true;
				deck[cardNumberToPlay].CardTarget();
				actionCardToPlay = deck[cardNumberToPlay];
			}

		}
	}//END of PlayCardFromUIButton()
}
