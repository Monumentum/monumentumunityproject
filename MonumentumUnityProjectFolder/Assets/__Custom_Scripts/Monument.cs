﻿using UnityEngine;
using System.Collections;

public class Monument : MonoBehaviour {


    public BoardSquare mOccupiedSquare;
    public int mAttacksLeftToWithstand = 3;
	public bool mMonumentDead = false;
    //public PlayerClickController mOwner;
    public int mOwner;


	MouseOverInfoManager mMouseOverInfoManager;

	// Use this for initialization
	void Start () 
    {
		mMouseOverInfoManager = FindObjectOfType(typeof(MouseOverInfoManager)) as MouseOverInfoManager;

	}
	
	// Update is called once per frame
	void Update () 
	{
	    if (mOccupiedSquare && !mMonumentDead)
        {
            foreach (BoardSquare square in mOccupiedSquare.neighborSquares)
            {
                if (square.occupier)
                {
                    Creature attacker = square.occupier.GetComponent<Creature>();
                    if (!attacker.lockedInCombat)
                    {
                        attacker.mCanAttackMonument = true;
                        attacker.mAttackableMonument = this;
                    }
                    else
                    {
                        attacker.mCanAttackMonument = false;
                        attacker.mAttackableMonument = null;
                    }
                }
            }
        }
		else
		{
			foreach (BoardSquare square in mOccupiedSquare.neighborSquares)
			{
				if (square.occupier)
				{
					Creature attacker = square.occupier.GetComponent<Creature>();
					attacker.mCanAttackMonument = false;
					attacker.mAttackableMonument = null;
				}
			}
		}

        if (mAttacksLeftToWithstand <= 0 && !mMonumentDead)
        {
            //mOwner.mRemainingMonuments -= 1;
            //mOwner.networkView.RPC("UpdateMonumentCount", RPCMode.All, this);
            PlayerClickController aController = FindObjectsOfType<PlayerClickController>()[0];
            aController.GetComponent<NetworkView>().RPC("UpdateMonumentCount", RPCMode.All, this.name);
			mMonumentDead = true;
        }
		if(mMonumentDead)
		{
			GetComponent<Renderer>().material.color = Color.red;
			mOccupiedSquare.hasAttackableTarget =false;
		}
		else if(mOwner == 2)
		{
			GetComponent<Renderer>().material.color = Color.black;
		}
	}//END of Update()

	void OnMouseEnter()
	{
		mMouseOverInfoManager.SetCreatureToolTip(true, "Player " + mOwner + "'s monument\n" + mAttacksLeftToWithstand + " hits remaining");
	}//END of OnMouseEnter()
	
	void OnMouseExit()
	{
		mMouseOverInfoManager.SetCreatureToolTip(false, "");		
	}//END of OnMouseExit()

}
