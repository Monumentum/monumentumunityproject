﻿using UnityEngine;
using System.Collections;
using PayPalLibrary;

public class PayPalGUITest : MonoBehaviour 
{

	PayPalAPICaller PayPalAPI;
	
	void Start()
	{
		PayPalAPI = new PayPalLibrary.PayPalAPICaller();
		PayPalAPI.Sandbox = true;
		PayPalAPI.ReturnMessage = "";
	}
	
	void OnGUI() 
	{
		if (GUI.Button(new Rect(100,100,200,50),"SET CREDENTIALS"))
		{
			PayPalAPI.SetCredentials("Harvey-facilitator-1_api1.Play-Monumentum.com", "673T59RQS86FU2EU", "An5ns1Kso7MWUdW4ErQKJJJ4qi4-AHg3LReVXynexvtH3-hg4ualDvXI");
		}
		if (GUI.Button(new Rect(100,200,200,50),"EXPRESS CHECKOUT"))
		{
			PayPalAPI.ReturnMessage = "";
			bool success1 = PayPalAPI.ExpressCheckout("http://www.Success.link", "http://www.Cancel.link", "Product", "1", "USD", true);
			Debug.Log(success1 + "    " + PayPalAPI.Token + "    " + PayPalAPI.ReturnMessage);
			if (success1) Application.OpenURL(PayPalAPI.ReturnMessage + "&useraction=commit");
		}
		if (GUI.Button(new Rect(100,300,200,50),"GET DETAILS"))
		{
			PayPalAPI.ReturnMessage = "";
			bool success2 = PayPalAPI.GetShippingDetails();
			Debug.Log(success2 + "    " + PayPalAPI.PayerID + "    " + PayPalAPI.ReturnMessage + "    " + PayPalAPI.Address);
		}
		if (GUI.Button(new Rect(100,400,200,50),"CONFIRM PAYMENT"))
		{
			PayPalAPI.ReturnMessage = "";
			bool success3 = PayPalAPI.ConfirmPayment("1", "USD");
			Debug.Log(success3 + "    " + PayPalAPI.ReturnMessage);
		}
		GUI.Label(new Rect(400,200,400,400),PayPalAPI.Address + "\n" + PayPalAPI.PayerID + "\n" + PayPalAPI.ReturnMessage);
	}

}
