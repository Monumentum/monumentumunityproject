PayPal Express Checkout Plugin
by baKno Games

This Plugin works on Mac, Windows, Linux, IOS and Android, it is a managed code library so it works on non-pro Unity as well. A simple implementation of the Express Checkout API by PayPal. With this Plugin you can create a custom checkout directly from your Unity project, and after login into PayPal using the browser, confirm the final payment within Unity as well. 

It is important to get familiarized with Express Checkout. Please visit this page and read along...
https://developer.paypal.com/docs/classic/express-checkout/integration-guide/ECGettingStarted/

It will be a useful point of reference for your web integration since a couple of web scripted pages will need to be created, to receive successfull transactions and cancellations.

How to use this Plugin

The main class in the Plugin is the PayPalAPI class. It provides two attributes and four main methods:

PayPalAPI.Sandbox is a boolean to define wether you are testing your integration or selling real products
PayPalAPI.ReturnMessage is a string that is used to return messages after each method is called
PayPalAPI.SetCredentials: To set up your PayPal API credentials (Sandboxed or Live)
PayPalAPI.ExpressCheckout: To set up the payment details
PayPalAPI.GetShippingDetails: To get the shipping details from PayPal 
PayPalAPI.ConfirmPayment: To confirm the payment

A simple javascript is included with all the functionality implemented in order.

- Define the Sandbox attribute. For testing set PayPalAPI.Sandbox to true.
- Set the PayPal API credentials using the SetCredentials function.
- Invoke the ExpressCheckout method which will call PayPal API to obtain the token and start the transaction.
- Redirect the user to the provided url, which will open up the browser guiding the user through the PayPal express checkout.
- After the user is redirected back to your site/application from PayPal, process their shipping details using the method GetShippingDetails.
- Show a confirmation message to the user and allow them to confirm the payment using the ConfirmPayment function.
- Or process the payment with your own application logic.

NOTE: Make sure to use the same amount and currency in the ExpressCheckout and ConfirmPayment methods.

If you have any problems using the Plugin, feel free to contact info@bakno.com.